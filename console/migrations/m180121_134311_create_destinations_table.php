<?php

use yii\db\Migration;

/**
 * Handles the creation of table `destinations`.
 */
class m180121_134311_create_destinations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('destinations', [
            'id' => $this->primaryKey(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'tour_id' => $this->integer(),
            'cover' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('destinations');
    }
}
