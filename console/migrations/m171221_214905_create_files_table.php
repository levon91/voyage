<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files`.
 */
class m171221_214905_create_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'uniq_name' => $this->string()->notNull(),
            'name' => $this->string(),
            'type' => $this->string()->notNull(),
            'extension' => $this->string(),
            'type_id' => $this->integer(),
            'created_at' => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('files');
    }
}
