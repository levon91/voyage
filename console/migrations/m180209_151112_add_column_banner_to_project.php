<?php

use yii\db\Migration;

/**
 * Class m180209_151112_add_column_banner_to_project
 */
class m180209_151112_add_column_banner_to_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects','banner', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180209_151112_add_column_banner_to_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_151112_add_column_banner_to_project cannot be reverted.\n";

        return false;
    }
    */
}
