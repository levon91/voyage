<?php

use yii\db\Migration;

/**
 * Handles the creation of table `funny_banner`.
 */
class m180209_164816_create_funny_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('funny_banner', [
            'id' => $this->primaryKey(),
            'number' => $this->float(),
            'type' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('funny_banner');
    }
}
