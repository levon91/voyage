<?php

use yii\db\Migration;

/**
 * Handles adding active to table `{{%files}}`.
 */
class m190612_125203_add_active_column_to_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('files', 'active', $this->boolean()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('files', 'active');
    }
}
