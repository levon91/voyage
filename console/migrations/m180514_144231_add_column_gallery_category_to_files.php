<?php

use yii\db\Migration;

/**
 * Class m180514_144231_add_column_gallery_category_to_files
 */
class m180514_144231_add_column_gallery_category_to_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('files','gallery_category',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180514_144231_add_column_gallery_category_to_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180514_144231_add_column_gallery_category_to_files cannot be reverted.\n";

        return false;
    }
    */
}
