<?php

use yii\db\Migration;

/**
 * Class m180409_173856_delete_title_column_from_file
 */
class m180409_173856_delete_title_column_from_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('files','title');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180409_173856_delete_title_column_from_file cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180409_173856_delete_title_column_from_file cannot be reverted.\n";

        return false;
    }
    */
}
