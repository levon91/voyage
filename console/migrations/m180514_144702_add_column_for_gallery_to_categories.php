<?php

use yii\db\Migration;

/**
 * Class m180514_144702_add_column_for_gallery_to_categories
 */
class m180514_144702_add_column_for_gallery_to_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'for_gallery', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180514_144702_add_column_for_gallery_to_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180514_144702_add_column_for_gallery_to_categories cannot be reverted.\n";

        return false;
    }
    */
}
