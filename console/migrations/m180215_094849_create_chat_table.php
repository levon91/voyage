<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m180215_094849_create_chat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chat_sessions', [
            'id' => $this->primaryKey(),
            'creator_ip' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'session_id' => $this->integer()->notNull(),
            'message' => $this->text(),
            'answered' => $this->boolean(),
            'is_new' => $this->boolean(),
            'is_admin' => $this->boolean(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('chat');
    }
}
