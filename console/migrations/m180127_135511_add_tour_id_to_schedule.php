<?php

use yii\db\Migration;

/**
 * Class m180127_135511_add_tour_id_to_schedule
 */
class m180127_135511_add_tour_id_to_schedule extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('schedule', 'tour_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180127_135511_add_tour_id_to_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_135511_add_tour_id_to_schedule cannot be reverted.\n";

        return false;
    }
    */
}
