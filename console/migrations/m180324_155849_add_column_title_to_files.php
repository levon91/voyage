<?php

use yii\db\Migration;

/**
 * Class m180324_155849_add_column_title_to_files
 */
class m180324_155849_add_column_title_to_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('files','title',$this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180324_155849_add_column_title_to_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_155849_add_column_title_to_files cannot be reverted.\n";

        return false;
    }
    */
}
