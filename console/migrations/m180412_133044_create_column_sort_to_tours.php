<?php

use yii\db\Migration;

/**
 * Class m180412_133044_create_column_sort_to_tours
 */
class m180412_133044_create_column_sort_to_tours extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tours', 'sort', $this->integer(3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180412_133044_create_column_sort_to_tours cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_133044_create_column_sort_to_tours cannot be reverted.\n";

        return false;
    }
    */
}
