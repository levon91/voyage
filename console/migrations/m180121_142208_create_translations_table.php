<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translations`.
 */
class m180121_142208_create_translations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('translations', [
            'id' => $this->primaryKey(),
            'lang' => $this->string(),
            'text' => $this->text(),
            'type' => $this->string(),
            'type_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('translations');
    }
}
