<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visitors}}`.
 */
class m190615_001550_create_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visitors}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(),
            'link_from' => $this->string(),
            'link' => $this->string(),
            'country' => $this->string(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visitors}}');
    }
}
