<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tours`.
 */
class m171221_215335_create_tours_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('tours', [
            'id' => $this->primaryKey(),
            'title_am' => $this->string()->notNull(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'content_am' => $this->text()->notNull(),
            'content_en' => $this->string(),
            'content_ru' => $this->string(),
            'caption_am' => $this->text()->notNull(),
            'caption_en' => $this->string(),
            'caption_ru' => $this->string(),
            'category_id' => $this->integer(),
            'price' => $this->float(),
            'days' => $this->integer(),
            'age_min' => $this->integer(),
            'type' => $this->string(),
            'cover' => $this->string(),
            'banner' => $this->string(),
            'attachments' => $this->text(),
            'destination_id' => $this->integer(),
            'coordinates' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createIndex('destination_id', 'tours', 'destination_id');
        $this->addForeignKey('post_tour', 'tours', 'destination_id', 'posts', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('tours');
    }
}
