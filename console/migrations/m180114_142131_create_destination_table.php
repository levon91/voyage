<?php

use yii\db\Migration;

/**
 * Handles the creation of table `destination`.
 */
class m180114_142131_create_destination_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('destination', [
            'id' => $this->primaryKey(),
            'title_am' => $this->string()->notNull(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'content_am' => $this->text()->notNull(),
            'content_en' => $this->string(),
            'content_ru' => $this->string(),
            'caption_am' => $this->text()->notNull(),
            'caption_en' => $this->string(),
            'caption_ru' => $this->string(),
            'category_id' => $this->integer(),
            'price' => $this->float(),
            'cover' => $this->string(),
            'attachments' => $this->text(),
            'coordinates' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('destination');
    }
}
