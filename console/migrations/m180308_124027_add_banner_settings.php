<?php

use yii\db\Migration;

/**
 * Class m180308_124027_add_banner_settings
 */
class m180308_124027_add_banner_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'banner', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180308_124027_add_banner_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180308_124027_add_banner_settings cannot be reverted.\n";

        return false;
    }
    */
}
