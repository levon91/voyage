<?php

use yii\db\Migration;

/**
 * Handles adding cover to table `categories`.
 */
class m180103_153524_add_cover_column_to_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'cover', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
