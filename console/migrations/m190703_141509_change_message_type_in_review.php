<?php

use yii\db\Migration;

/**
 * Class m190703_141509_change_message_type_in_review
 */
class m190703_141509_change_message_type_in_review extends Migration
{
    /**
     * {@inheritdoc}
     */


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('reviews', 'message', 'text');
    }

    public function down()
    {
        $this->alterColumn('reviews', 'message', 'string');
    }

}
