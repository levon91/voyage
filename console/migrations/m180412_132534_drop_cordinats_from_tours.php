<?php

use yii\db\Migration;

/**
 * Class m180412_132534_drop_cordinats_from_tours
 */
class m180412_132534_drop_cordinats_from_tours extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tours', 'coordinates');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180412_132534_drop_cordinats_from_tours cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_132534_drop_cordinats_from_tours cannot be reverted.\n";

        return false;
    }
    */
}
