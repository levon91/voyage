<?php

use yii\db\Migration;

/**
 * Class m180119_164603_change_url_type
 */
class m180119_164603_change_url_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('menus', 'url',$this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180119_164603_change_url_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180119_164603_change_url_type cannot be reverted.\n";

        return false;
    }
    */
}
