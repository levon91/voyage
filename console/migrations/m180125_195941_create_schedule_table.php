<?php

use yii\db\Migration;

/**
 * Handles the creation of table `schedule`.
 */
class m180125_195941_create_schedule_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('schedule', [
            'id' => $this->primaryKey(),
            'time_from' => $this->string(),
            'time_to' => $this->string(),
            'week_day' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('schedule');
    }
}
