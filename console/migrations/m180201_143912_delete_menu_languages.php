<?php

use yii\db\Migration;

/**
 * Class m180201_143912_delete_menu_languages
 */
class m180201_143912_delete_menu_languages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('menus','title_am');
        $this->dropColumn('menus','title_ru');
        $this->dropColumn('menus','title_en');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180201_143912_delete_menu_languages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_143912_delete_menu_languages cannot be reverted.\n";

        return false;
    }
    */
}
