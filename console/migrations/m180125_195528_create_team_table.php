<?php

use yii\db\Migration;

/**
 * Handles the creation of table `team`.
 */
class m180125_195528_create_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('team', [
            'id' => $this->primaryKey(),
            'cover' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('team');
    }
}
