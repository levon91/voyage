<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meta`.
 */
class m180412_120724_create_meta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('meta', [
            'id' => $this->primaryKey(),
            'global' => $this->string(),
            'home' => $this->boolean(),
            'type' => $this->string(),
            'key' => $this->string(),
            'type_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('meta');
    }
}
