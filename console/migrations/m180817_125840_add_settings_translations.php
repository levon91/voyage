<?php

use yii\db\Migration;

/**
 * Class m180817_125840_add_settings_translations
 */
class m180817_125840_add_settings_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('settings','value','value_en');
        $this->addColumn('settings','value_fr',$this->string());
        $this->addColumn('settings','value_es',$this->string());
        $this->addColumn('settings','value_ru',$this->string());
        $this->addColumn('settings','value_it',$this->string());
        $this->addColumn('settings','value_de',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180817_125840_add_settings_translations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180817_125840_add_settings_translations cannot be reverted.\n";

        return false;
    }
    */
}
