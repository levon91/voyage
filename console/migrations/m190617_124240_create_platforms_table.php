<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%platforms}}`.
 */
class m190617_124240_create_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('platforms', [
            'id' => $this->primaryKey(),
            'logo' => $this->string()->null(),
            'link' => $this->string()->notNull(),
            'description_en' => $this->string()->null(),
            'description_de' => $this->string()->null(),
            'description_fr' => $this->string()->null(),
            'description_es' => $this->string()->null(),
            'description_it' => $this->string()->null(),
            'description_ru' => $this->string()->null(),
            'order' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%platforms}}');
    }
}
