<?php

use yii\db\Migration;

/**
 * Class m180127_133438_add_schedule
 */
class m180127_133438_add_schedule extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('tours','schedule',$this->boolean()->defaultValue(false));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180127_133438_add_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_133438_add_schedule cannot be reverted.\n";

        return false;
    }
    */
}
