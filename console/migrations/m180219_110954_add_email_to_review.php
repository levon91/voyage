<?php

use yii\db\Migration;

/**
 * Class m180219_110954_add_email_to_review
 */
class m180219_110954_add_email_to_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('reviews', 'email', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180219_110954_add_email_to_review cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180219_110954_add_email_to_review cannot be reverted.\n";

        return false;
    }
    */
}
