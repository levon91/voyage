<?php

use yii\db\Migration;

/**
 * Class m180213_123726_add_post_languages
 */
class m180213_123726_add_post_languages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('posts','title_am');
        $this->dropColumn('posts','title_en');
        $this->dropColumn('posts','title_ru');
        $this->dropColumn('posts','content_am');
        $this->dropColumn('posts','content_en');
        $this->dropColumn('posts','content_ru');
        $this->dropColumn('posts','caption_am');
        $this->dropColumn('posts','caption_en');
        $this->dropColumn('posts','caption_ru');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180213_123726_add_post_languages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_123726_add_post_languages cannot be reverted.\n";

        return false;
    }
    */
}
