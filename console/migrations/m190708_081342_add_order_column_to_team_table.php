<?php

use yii\db\Migration;

/**
 * Handles adding order to table `{{%team}}`.
 */
class m190708_081342_add_order_column_to_team_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('team', 'order', $this->integer()->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
