<?php

use yii\db\Migration;

/**
 * Handles the creation of table `form`.
 */
class m180316_105453_create_form_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('form', [
            'id' => $this->primaryKey(),
            'participants' => $this->string(100),
            'flexible_dates' => $this->boolean(),
            'travel_dates' => $this->date(),
            'occasion' => $this->string(),
            'adults' => $this->integer(2),
            'teenagers' => $this->integer(2),
            'kids' => $this->integer(2),
            'babies' => $this->integer(2),
            'private_group' => $this->boolean(),
            'date_start' => $this->date(),
            'date_length' => $this->date(),
            'age_groups' => $this->boolean(),
            'approximate_dates' => $this->boolean(),
            'date_type' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('form');
    }
}
