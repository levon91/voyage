<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m171221_212342_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title_am' => $this->string()->notNull(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'content_am' => $this->text()->notNull(),
            'content_en' => $this->string(),
            'content_ru' => $this->string(),
            'cover' => $this->string(),
            'attachments' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('pages');
    }
}
