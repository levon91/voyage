<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m171221_213819_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'title_am' => $this->string()->notNull(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'content_am' => $this->text()->notNull(),
            'content_en' => $this->string(),
            'content_ru' => $this->string(),
            'caption_am' => $this->text()->notNull(),
            'caption_en' => $this->string(),
            'caption_ru' => $this->string(),
            'category_id' => $this->integer(),
            'type' => $this->string(),
            'cover' => $this->string(),
            'banner' => $this->string(),
            'attachments' => $this->text(),
            'coordinates' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
        $this->createIndex('category_id', 'posts', 'category_id');
        $this->addForeignKey('post_category', 'posts', 'category_id', 'categories', 'id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('posts');
    }
}
