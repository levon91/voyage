<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footer_menu}}`.
 */
class m190704_140077_create_footer_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%footer_menu}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'value_en' => $this->text(),
            'value_de' => $this->text(),
            'value_fr' => $this->text(),
            'value_es' => $this->text(),
            'value_it' => $this->text(),
            'value_ru' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footer_menu}}');
    }
}
