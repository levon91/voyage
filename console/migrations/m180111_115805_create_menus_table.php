<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menus`.
 */
class m180111_115805_create_menus_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('menus', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'title_am' => $this->string()->notNull(),
            'title_en' => $this->string()->notNull(),
            'title_ru' => $this->string()->notNull(),
            'type' => $this->string(),
            'type_id' => $this->integer(),
            'location' => $this->string(),
            'sort' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'url' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('menus');
    }
}
