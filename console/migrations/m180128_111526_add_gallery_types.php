<?php

use yii\db\Migration;

/**
 * Class m180128_111526_add_gallery_types
 */
class m180128_111526_add_gallery_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('files', 'gallery_type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180128_111526_add_gallery_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_111526_add_gallery_types cannot be reverted.\n";

        return false;
    }
    */
}
