<?php

use yii\db\Migration;

/**
 * Class m180128_150505_add_projects_table
 */
class m180128_150505_add_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects',[
            'id' => $this->primaryKey(),
            'cover' => $this->string(),
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180128_150505_add_projects_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_150505_add_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
