<?php

use yii\db\Migration;

/**
 * Class m180219_092751_reviews
 */
class m180219_092751_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->notNull(),
            'client_name' => $this->string()->notNull(),
            'image' => $this->string(),
            'tour_id' => $this->integer(),
            'approved' => $this->boolean()->defaultValue(false),
            'created_date' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180219_092751_reviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180219_092751_reviews cannot be reverted.\n";

        return false;
    }
    */
}
