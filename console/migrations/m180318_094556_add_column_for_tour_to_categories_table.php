<?php

use yii\db\Migration;

/**
 * Class m180318_094556_add_column_for_tour_to_categories_table
 */
class m180318_094556_add_column_for_tour_to_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'for_tour', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180318_094556_add_column_for_tour_to_categories_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180318_094556_add_column_for_tour_to_categories_table cannot be reverted.\n";

        return false;
    }
    */
}
