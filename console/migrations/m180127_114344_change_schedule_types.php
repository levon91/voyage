<?php

use yii\db\Migration;

/**
 * Class m180127_114344_change_schedule_types
 */
class m180127_114344_change_schedule_types extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('schedule', 'time_from', $this->date());
        $this->alterColumn('schedule', 'time_to', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180127_114344_change_schedule_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_114344_change_schedule_types cannot be reverted.\n";

        return false;
    }
    */
}
