<?php

use yii\db\Migration;

/**
 * Class m180321_120542_add_gallery_type
 */
class m180321_120542_add_gallery_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('files', 'image_size', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180321_120542_add_gallery_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_120542_add_gallery_type cannot be reverted.\n";

        return false;
    }
    */
}
