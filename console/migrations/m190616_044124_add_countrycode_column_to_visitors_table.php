<?php

use yii\db\Migration;

/**
 * Handles adding countrycode to table `{{%visitors}}`.
 */
class m190616_044124_add_countrycode_column_to_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('visitors', 'country_code', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('visitors', 'country_code');
    }
}
