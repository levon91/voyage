<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m171221_213819_create_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'title_am' => $this->string()->notNull(),
            'title_en' => $this->string(),
            'title_ru' => $this->string(),
            'desc_am' => $this->text(),
            'desc_en' => $this->text(),
            'desc_ru' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }
}
