<?php

use yii\db\Migration;

/**
 * Class m180303_121540_add_post_token
 */
class m180303_121540_add_post_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('posts', 'token', $this->text()->defaultValue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180303_121540_add_post_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180303_121540_add_post_token cannot be reverted.\n";

        return false;
    }
    */
}
