<?php

use yii\db\Migration;

/**
 * Class m180318_110201_add_in_landing_page_column
 */
class m180318_110201_add_in_landing_page_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tours', 'in_landing_page', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180318_110201_add_in_landing_page_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180318_110201_add_in_landing_page_column cannot be reverted.\n";

        return false;
    }
    */
}
