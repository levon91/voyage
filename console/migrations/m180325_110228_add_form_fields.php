<?php

use yii\db\Migration;

/**
 * Class m180325_110228_add_form_fields
 */
class m180325_110228_add_form_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {


        $this->addColumn('form', 'accommodation', $this->string());
        $this->addColumn('form', 'additional', $this->text());
        $this->addColumn('form', 'budget', $this->float());
        $this->addColumn('form', 'accompaniment', $this->string());
        $this->addColumn('form', 'first_name', $this->string());
        $this->addColumn('form', 'last_name', $this->string());
        $this->addColumn('form', 'email', $this->string());
        $this->addColumn('form', 'phone', $this->string());
        $this->addColumn('form', 'birth_date', $this->date());
        $this->addColumn('form', 'residence_country', $this->string(100));
        $this->addColumn('form', 'accept', $this->boolean(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180325_110228_add_form_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180325_110228_add_form_fields cannot be reverted.\n";

        return false;
    }
    */
}
