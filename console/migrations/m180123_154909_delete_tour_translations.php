<?php

use yii\db\Migration;

/**
 * Class m180123_154909_delete_tour_translations
 */
class m180123_154909_delete_tour_translations extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('tours','title_am');
        $this->dropColumn('tours','title_en');
        $this->dropColumn('tours','title_ru');
        $this->dropColumn('tours','content_am');
        $this->dropColumn('tours','content_en');
        $this->dropColumn('tours','content_ru');
        $this->dropColumn('tours','caption_am');
        $this->dropColumn('tours','caption_en');
        $this->dropColumn('tours','caption_ru');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180123_154909_delete_tour_translations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180123_154909_delete_tour_translations cannot be reverted.\n";

        return false;
    }
    */
}
