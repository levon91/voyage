<?php

use yii\db\Migration;

/**
 * Class m171221_213059_settings
 */
class m171221_213059_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'value' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_213059_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_213059_settings cannot be reverted.\n";

        return false;
    }
    */
}
