<?php

use yii\db\Migration;

/**
 * Class m180211_204246_remove
 */
class m180211_204246_remove extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('categories', 'title_am');
        $this->dropColumn('categories', 'title_ru');
        $this->dropColumn('categories', 'title_en');

        $this->dropColumn('categories', 'desc_am');
        $this->dropColumn('categories', 'desc_ru');
        $this->dropColumn('categories', 'desc_en');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180211_204246_remove cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180211_204246_remove cannot be reverted.\n";

        return false;
    }
    */
}
