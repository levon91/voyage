<?php

use yii\db\Migration;

/**
 * Class m180123_095550_alter_ranslations_table
 */
class m180123_095550_alter_ranslations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('translations', 'text', 'text_am');
        $this->addColumn('translations', 'text_en', $this->text());
        $this->addColumn('translations', 'text_ru', $this->text());
        $this->addColumn('translations', 'text_it', $this->text());
        $this->addColumn('translations', 'text_es', $this->text());
        $this->addColumn('translations', 'text_fr', $this->text());
        $this->addColumn('translations', 'text_de', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180123_095550_alter_ranslations_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180123_095550_alter_ranslations_table cannot be reverted.\n";

        return false;
    }
    */
}
