<?php

namespace frontend\components;

use common\models\Routes;
use yii;
use yii\base\BootstrapInterface;

class Routing implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $cmsModel = Routes::find()
            ->all(); // customize the query according to your need
        $routeArray = [];

        foreach ($cmsModel as $row) {
            $routeArray[$row->url] = $row->action;
        }
      
        $routeArray[''] = 'site/index';
        $routeArray['events'] = '/page/events';
        $routeArray[ 'blog'] = '/page/categories';
        $routeArray[ 'news'] = '/page/news';
        $routeArray[ 'contact'] = '/site/contact';
        $routeArray[ 'create-tour'] = '/page/create-tour';
        $routeArray[ 'gallery'] = '/page/gallery';
        $routeArray[ 'team'] = '/page/team';
        $routeArray[ 'page/<id:\d+>'] = '/page/index';
        $routeArray[ 'pages/categories/<id:\d+>'] = '/pages/categories/<id:\d+>';
         $routeArray[ 'tours/all-tours/<id:\w+>'] = '/tours/all-tours';
        $routeArray['<controller:\w+>/<id:\d+>'] = '<controller>/index';
        $routeArray['<controller:\w+>/<action:\w+>/<id:\d+>'] = '<controller>/<action>';
        $routeArray['<controller:\w+>/<action:\w+>'] = '<controller>/<action>';
        $routeArray['/<name:>'] = 'page/post-categories';
       
        $app->urlManager->addRules($routeArray,false);
    }
}