<?php

namespace frontend\components;

/**
 * Created by PhpStorm.
 * User: Comp
 * Date: 19/01/2018
 * Time: 21:47
 */


use common\models\Chat;
use common\models\FooterMenu;
use common\models\Settings;
use common\models\Translation;
use Yii;
use yii\base\Component;
use common\models\Menus;
use common\models\Metas;

class Helper extends Component
{
    public $menu;
    public static $social = ['Address', 'facebook', 'Phone', 'Email', 'vimeo', 'linkedin', 'instagram', 'pinterest', 'google', 'twitter', 'youtube-play'];

    public function getMenuChild($menu, $sub = 1)
    {
        $this->menu .= '<ul>';
        foreach ($menu as $item) {
            if (!empty($item['items'])) {
                $this->menu .= '<li><a href="' . '/' . Yii::$app->language . '/' . $item['url'][0] . '">' . $item['label'] . '</a>';
                return $this->getMenuChild($item['items'], ++$sub);
            } else {

                $this->menu .= '<li><a href="' . '/' . Yii::$app->language . '/' . $item['url'][0] . '">' . $item['label'] . '</a></li>';
            }
        }
        if ($sub > 1) {
            $this->menu .= '</li>';
        }
        $this->menu .= '</ul>';
        return $this->menu;
    }

    public function getUserMessages()
    {
        return Chat::find()->select('chat.id,chat.session_id,chat.message,chat.is_admin,chat_sessions.creator_ip,chat.created_at')
            ->innerJoin('chat_sessions', 'chat_sessions.id = chat.session_id')->where(['chat_sessions.creator_ip' => $_SERVER['REMOTE_ADDR']])->andWhere(['>', 'chat.created_at', date("Y-m-d H:i:s", strtotime('-2 hours'))])->all();
    }


    public static function getFooterMenus()
    {
        $footer = \common\models\Settings::find()->where(['in', 'key', self::$social])->all();
        $footer_menu = FooterMenu::find()->asArray()->all();
        $footerEmail = '';
        $footerAddress = '';
        $footerPhone = '';
        $social = [];
        foreach ($footer as $key => $foot) {
            switch ($foot->key) {
                case 'Phone':
                    $footerPhone = $foot;
                    break;
                case 'Email':
                    $footerEmail = $foot;
                    break;
                case 'Address':
                    $footerAddress = $foot;
                    break;
                default:
                    $social[] = $foot;
            }
        }

        $menusObj = new Menus();
        $menus = $menusObj->getMenus('footer_1');
        $footerMenus = $menusObj->getMenus('footer_3');
        $footerMenusCenter = $menusObj->getMenus('footer_2');
        $footerMenuItems = '';
        $footerMenuItems2 = '';
        $menuItems = '<ul>';
        foreach ($menus as $menu):
            $items = empty($menu['items']);
            $href = !$items ? 'javascript:void(0);' : $menu['url'][0];
            $class = !$items ? 'submenu' : '';
            $menuItems .= '<li class="' . $class . '">
            <a href="/' .Yii::$app->language. $href . '"
               class="show-submenu">' . $menu['label'] . '
            </a>';
            if (!$items) {
                $menuItems .= Yii::$app->helper->getMenuChild($menu['items']);
            }
            $menuItems = $menuItems . '</li>';

        endforeach;
        $menuItems .= '</ul>';

        foreach ($footerMenus as $footerMenu):

            $items = empty($footerMenu['items']);
            $href = !$items ? 'javascript:void(0);' : $footerMenu['url'][0];
            $class = !$items ? 'submenu' : '';
            $footerMenuItems .= '<li class="' . $class . '">
            <a href="/' .Yii::$app->language. $href . '"
               class="show-submenu">' . $footerMenu['label'] . '
            </a>';
            if (!$items) {
                $footerMenuItems .= Yii::$app->helper->getMenuChild($footerMenu['items']);
            }
            $footerMenuItems = $footerMenuItems . '</li>';
        endforeach;
        foreach ($footerMenusCenter as $footerMenu_2):

            $items = empty($footerMenu_2['items']);
            $href = !$items ? 'javascript:void(0);' : $footerMenu_2['url'][0];
            $class = !$items ? 'submenu' : '';
            $footerMenuItems2 .= '<li class="' . $class . '">
            <a href="/' .Yii::$app->language. $href . '"
               class="show-submenu">' . $footerMenu_2['label'] . '
            </a>';
            if (!$items) {
                $footerMenuItems2 .= Yii::$app->helper->getMenuChild($footerMenu_2['items']);
            }
            $footerMenuItems2 = $footerMenuItems2 . '</li>';
        endforeach;

        return ['footer_menu' => $footer_menu,'footerEmail' => $footerEmail, 'address' => $footerAddress, 'social' => $social, 'footerPhone' => $footerPhone, 'menuItems' => $menuItems, 'footerMenuItems' => $footerMenuItems, 'footerMenuItems2' => $footerMenuItems2];
    }

    public static function getLanguage()
    {
        return Yii::$app->params['lang'][Yii::$app->language];
    }

    public static function getWorkingDais()
    {
        return Settings::find()->where(['key' => 'Working dais'])->one();
    }

    public static function getInfo()
    {
        $info = Settings::find()->where(['in', 'key', ['address', 'phone', 'email']])->all();
        $allInfo = [];
        foreach ($info as $key => $value) {
            $allInfo[$value->key] = $value->value;
        }
        return $allInfo;
    }
    
    public static function getGlobalMetas()
    {
        $metas = Metas::find()->where(['global' => 1])->all();
        return $metas;
    }
    
     public static function getHomeMetas()
    {
        $metas = Metas::find()->where(['home' => 1])->all();
        return $metas;
    }
    
        public static function getPagMetas($id)
    {
        return Metas::find()->where(['type' => 'page'])->andWhere(['type_id' => $id])->all();
    }
     public static function getPostMetas($id)
    {
        return Metas::find()->where(['type' => 'post'])->andWhere(['type_id' => $id])->all();
    }
       public static function getCategoryMetas($id)
    {
        return Metas::find()->where(['type' => 'category'])->andWhere(['type_id' => $id])->all();
    }
}
