<?php


namespace frontend\controllers;

use Yii;
use common\models\Statistics;
use common\models\Visitors;
use Faker\Provider\DateTime;
use yii\db\Expression;
use yii\helpers\Url;

class IpController extends Controller
{

    public function actionGetIp(){
        $request = yii::$app->geolocation->getInfo(Yii::$app->request->userIP);
        $visitor = Visitors::find()->where(['ip' =>$request['geoplugin_request']])->orderBy(['id' => SORT_DESC])->one();
        $crated_at = new \DateTime($visitor['created_at']);
        $crated_at = $crated_at->add(new \DateInterval('P1D'));
        $crated_at = date_format($crated_at, 'Y-m-d');

        $date = new \DateTime();
        $date = date_format($date, 'Y-m-d');
        if ($crated_at<=$date && $visitor!= null){
            $visitor = null;
        }
        if (!$visitor) {
            if (Yii::$app->request->get('referer') != null) {
                $prefer_link = Yii::$app->request->get('referer');
                $app_url = \Yii::$app->params['app_url'];
                $pos = strpos($prefer_link, $app_url);
                if ($pos === false) {
                    $link_from = $prefer_link;

                }else{
                    $link_from = null;
                }
            } else {
                $link_from = null;
            }
            if (Yii::$app->request->isAjax) {
                $actual_link = Yii::$app->request->get('url');
            }else{
                $actual_link = null;
            }
            $visitor = new Visitors();
            $visitor->ip = $request['geoplugin_request'];
            $visitor->country = $request['geoplugin_countryName'];
            $visitor->country_code = $request['geoplugin_countryCode'];
            $visitor->link_from = $link_from;
            $visitor->link = $actual_link;
            $visitor->created_at = $date;

            $visitor->save();
        }
    }

}