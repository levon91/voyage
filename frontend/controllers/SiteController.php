<?php


namespace frontend\controllers;


use common\models\Categories;


use common\models\Destinations;


use common\models\Pages;

use common\models\Platforms;
use common\models\Projects;

use common\models\Reviews;
use common\models\Services;

use common\models\ToursSearch;

use common\models\Translation;

use frontend\models\CreateYourTourForm;

use SebastianBergmann\CodeCoverage\Report\Xml\Project;

use Yii;

use yii\base\InvalidParamException;

use yii\db\Query;

use yii\web\BadRequestHttpException;

use yii\filters\VerbFilter;

use yii\filters\AccessControl;

use common\models\LoginForm;

use frontend\models\PasswordResetRequestForm;

use frontend\models\ResetPasswordForm;

use frontend\models\SignupForm;

use frontend\models\ContactForm;

use common\models\Media;

use common\models\Tours;

use common\models\Settings;

use common\models\FunnyBanner;

use common\models\ChatSession;

use common\models\Chat;


/**
 * Site controller
 */
class SiteController extends Controller

{

    /**
     * @inheritdoc
     */

    public function behaviors()

    {

        return [

            'access' => [

                'class' => AccessControl::className(),

                'only' => ['logout', 'signup'],

                'rules' => [

                    [

                        'actions' => ['signup'],

                        'allow' => true,

                        'roles' => ['?'],

                    ],

                    [

                        'actions' => ['logout'],

                        'allow' => true,

                        'roles' => ['@'],

                    ],

                ],

            ],
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'duration' => 60,
                'variations' => [
                    \Yii::$app->language,
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT COUNT(*) FROM posts',
                ],
            ],
            'verbs' => [

                'class' => VerbFilter::className(),

                'actions' => [

                    'logout' => ['post'],

                ],

            ],

        ];

    }


    /**
     * @inheritdoc
     */

    public function actions()

    {

        return [

            'error' => [

                'class' => 'yii\web\ErrorAction',

            ],

            'captcha' => [

                'class' => 'yii\captcha\CaptchaAction',

                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,

            ],

        ];

    }


    public function actionError(){
        return $this->render('error404');
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */

    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function actionIndex(){

        $translation = Translation::find()->select('type_id')->where(['text_en' => 'Top Tours'])->andWhere(['type' => 'categories-title'])->one();

        $category = Categories::find()->select('id')->where(['id' => $translation->type_id])->one();

        $topTours = $this->actionTopTours($category->id);

        $model = new Media();


        $tours = ToursSearch::getLandingPageTour($category->id);

        $allTours = Tours::find()->count();

      //  $home_gallery = $this->getGallery(['home_gallery']);

        $big = $model->getGalleryType('BIG', 1);

        $small = $model->getGalleryType('small', 4);

        $projects = Projects::find()->select(['id','cover'])->limit(3)->all();

        $banners = FunnyBanner::find()->limit(4)->all();

        $services = Services::find()->limit(4)->all();

        $review = Settings::find()->where(['key' => 'review'])->one();

        $topTitle = Settings::find()->where(['key' => 'Top Title'])->one();

        $ourAgency = Settings::find()->where(['key' => 'Our agency'])->one();



        $serv = [];

        foreach ($services as $service) {

            $serv[$service->title_en] = $service;

        }

        $services = $serv;

//
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            $videoSlider = Media::find()
            ->where(['type' => 'video-mobile','active' => true])
            ->one();
        }else{
            $videoSlider = Media::find()
            ->where(['type' => 'video-desktop','active' => true])
            ->one();
        }
        if (!$videoSlider) {

            $slider = $this->getPhotos();

            $slide_video = false;

        } else {

            $slider = false;

            $slide_video = $videoSlider;


        }

        $video = isset($vid['Video']) ? $vid['Video'] : '';
        $reviews = Reviews::find()->where(['tour_id' => 0])->andWhere(['approved' => 1])->orderBy(['id' => SORT_DESC])->limit(15)->all();
        $platforms = Platforms::find()->orderBy('order')->all();
        return $this->render('index', [

            'slider' => $slider,

            'tours' => $tours,

            'count' => $allTours,

         //   'home_gallery' => $home_gallery,

            'topTours' => $topTours,

            'big' => $big,

            'small' => $small,

            'projects' => $projects,

            'banners' => $banners,

            'video' => $video,
            'platforms' => $platforms,

            'slide_video' => $slide_video,

            'services' => $services,

            'topTitle' => $topTitle,

            'ourAgency' => $ourAgency,

            'review'   => $review,

            'reviews' => $reviews

        ]);

    }





    /**
     * Logs in a user.
     *
     * @return mixed
     */

    /*public function actionLogin()

    {

        if (!Yii::$app->user->isGuest) {

            return $this->goHome();

        }



        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();

        } else {

            $model->password = '';



            return $this->render('login', [

                'model' => $model,

            ]);

        }

    }*/


    /**
     * Logs out the current user.
     *
     * @return mixed
     */

    public function actionLogout()

    {

        Yii::$app->user->logout();


        return $this->goHome();

    }


    /**
     * Displays contact page.
     *
     * @return mixed
     */

    public function actionContact()

    {

        $model = new ContactForm();

        $contactBanner = Settings::find()->where(['key' => 'Contact Us'])->one();


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {

                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');

            } else {

                Yii::$app->session->setFlash('error', 'There was an error sending your message.');

            }


            return $this->refresh();

        } else {

            return $this->render('contactUs', [

                'model' => $model,

                'contactBanner' => $contactBanner,

            ]);

        }

    }


    /**
     * Displays about page.
     *
     * @return mixed
     */

    public function actionAbout()

    {

        $aboutBanner = Settings::find()->where(['key' => 'About Us'])->one();

        $about = Pages::find()->where(['id' => 1])->one();

        return $this->render('about', [

            'about' => $about,

            'aboutBanner' => $aboutBanner,

        ]);

    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */

    public function actionRequestPasswordReset()

    {

        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {

                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');


                return $this->goHome();

            } else {

                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');

            }

        }


        return $this->render('requestPasswordResetToken', [

            'model' => $model,

        ]);

    }


    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */

    public function actionResetPassword($token)

    {

        try {

            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {

            throw new BadRequestHttpException($e->getMessage());

        }


        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {

            Yii::$app->session->setFlash('success', 'New password saved.');


            return $this->goHome();

        }


        return $this->render('resetPassword', [

            'model' => $model,

        ]);

    }


    public function getPhotos()

    {

        return Media::find()->where(['type' => 'slider'])->all();

    }


    public function getGallery($gallery)

    {

        return Media::find()->where(['in', 'type', $gallery])->all();

    }


    public function actionTopTours($category)

    {

        return Tours::find()->select(['cover','id'])->where(['category_id' => $category])->andWhere(['in_landing_page' => 1])->orderBy('sort')->limit(3)->all();

    }



//    public function actionParallax()
//
//    {
//        $parallax = Settings::find()->where(['key' => 'Parallax'])->one();
//
//        return $this->render('parallax', [
//
//            'parallax' => $parallax,
//
//        ]);
//
//    }
    public function actionParallax()


    {


        $parallax = Settings::find()->where(['key' => 'Parallax'])->one();


        return $this->render('parallax', [


            'parallax' => $parallax,


        ]);


    }


    public function actionSendMessage()

    {

        if (Yii::$app->request->isAjax && Yii::$app->request->post() && trim(Yii::$app->request->post('message')) != '') {


            // create Session

            $session = Yii::$app->session->get('chat');

            if (!$session && Yii::$app->request->post('message')) {

                $chat_session = ChatSession::find()->where(['creator_ip' => $_SERVER['REMOTE_ADDR']])->one();

                if ($chat_session) {

                    Yii::$app->session->set('chat', $chat_session->id);

                } else {

                    $chatSession = new ChatSession();

                    $chatSession->creator_ip = $_SERVER['REMOTE_ADDR'];

                    $chatSession->created_at = date('Y-m-d', time());

                    $chatSession->save();

                    Yii::$app->session->set('chat', $chatSession->id);

                }

            }

            if (Yii::$app->request->post('message') && trim(Yii::$app->request->post('message')) != '') {

                $chat = new Chat();

                $chat->session_id = Yii::$app->session->get('chat');

                $chat->answered = false;

                $chat->message = Yii::$app->request->post('message');

                $chat->is_new = true;

                $chat->is_admin = false;

                $chat->created_at = date('Y-m-d H:i:s', time());

                if (!$chat->save()) {

                }

            }

        } elseif (Yii::$app->request->isAjax) {

            $session = Yii::$app->session->get('chat');

            if (!$session) {


            } else {

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                \Yii::$app->response->data = $this->getAnswer($session);

            }

        } else {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            \Yii::$app->response->data = $this->getAnswer(yii::t('app', 'Sorry we cant help you!'));

            return;

        }

    }


    private function getAnswer($session)

    {


        $answers = Chat::find()->where(['session_id' => $session])->andWhere(['is_admin' => true])->andWhere(['is_new' => true])->all();

        $message = '';

        if ($answers) {

            foreach ($answers as $answer) {

                $message = $answer->message . PHP_EOL;

                $answer->is_new = false;

                $answer->save(false);

            }

            return $message;

        }

        return $message;

    }

}


?>