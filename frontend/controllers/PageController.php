<?php
/**
 * Created by PhpStorm.
 * User: vazge
 * Date: 1/28/2018
 * Time: 2:55 PM
 */

namespace frontend\controllers;

use common\models\Categories;
use common\models\Events;
use common\models\Media;
use common\models\Menus;
use common\models\Pages;
use common\models\Posts;
use common\models\Projects;
use common\models\ProjectsSearch;
use common\models\Settings;
use common\models\Team;
use common\models\Translation;
use frontend\models\CreateYourTourForm;
use frontend\models\TravellersForm;
use frontend\models\TravellersFormDetails;
use frontend\models\TravellersFormPersonalData;
use yii;
use common\models\Routes;
use common\models\Form;
use kartik\mpdf\Pdf;


class PageController extends Controller
{

    public function actionTeam()
    {
        $team = $this->getTeam();
        $team_title = Settings::find()->where(['key' => 'team-title'])->one();
        $team_description = Settings::find()->where(['key' => 'team-description'])->one();

        return $this->render('team', [
            'team' => $team,
            'team_title' => $team_title,
            'team_description' => $team_description,
        ]);
    }

    public function getTeam()
    {
        return Team::find()
            ->orderBy(['order' => SORT_ASC])
            ->all();
    }

    public function actionIndex($id=false)
    {
        if (!$id) {
              $routes = Routes::find()->where(['url' => Yii::$app->request->pathInfo])->one();
            if($routes){
                $type = explode('_',$routes->type);
                $id = end($type);
            }else{
                  return $this->redirect('http://voyagearmenia.com/');
            }
        }

        $page = Pages::find()->where(['id' => $id])->one();

        if (!$page) {
            throw new \yii\web\NotFoundHttpException();
        } else {
            return $this->render('view', ['page' => $page]);
        }
    }

    public function actionProjects($id)
    {
        $project = Projects::find()->where(['id' => $id])->one();
        return $this->render('projectDetails', [
            'project' => $project,
        ]);
    }

    public function actionAllProjects()
    {
        $projectBanner = Settings::find()->where(['key' => 'Project Banner'])->one();
        $projects = ProjectsSearch::getAllProjects();
        return $this->render('all_projects', [
            'projects' => $projects,
            'projectBanner' => $projectBanner,
        ]);
    }

    public function actionCategories($id = false)
    {
$post = false;
        if (!$id && Yii::$app->request->pathInfo != 'blog') {
            return $this->redirect('http://dev.voyagearmenia.com');
        } elseif (Yii::$app->request->pathInfo == 'blog') {
            $blog = Translation::find()->select('text_en,type_id,type')->where(['type' => 'categories-title'])->andWhere(['text_en' => 'Blog'])->one();
        
            $id = $blog->type_id;
            $post = true;
        } elseif (Yii::$app->request->pathInfo == 'news') {
            $blog = Translation::find()->select('text_en,type_id,type')->where(['type' => 'categories-title'])->andWhere(['text_en' => 'News'])->one();
            $id = $blog->type_id;
            $post = true;
        }
        $post = new Posts();
        $category = Categories::find()->where(['id' => $id])->one();
        $posts = $post->getByCategory($id,$post);
        
        return $this->render('//posts/list_view', ['posts' => $posts, 'category' => $category]);
    }

    public function actionNews()
    {

        $blog = Translation::find()->select('text_en,type_id,type')->where(['type' => 'categories-title'])->andWhere(['text_en' => 'News'])->one();
        $id = $blog->type_id;

        $post = new Posts();
        $category = Categories::find()->where(['id' => $id])->one();
        $posts = $post->getByCategory($id,true);
        return $this->render('//posts/list_view', ['posts' => $posts, 'category' => $category]);
    }

    public function actionEvents()
    {
        $events = Events::getEvents();
        $eventImage = Settings::find()->where(['key' => 'Event Banner'])->one();
        $image = $eventImage->value;
        return $this->render('events', [
            'dataProvider' => $events,
            'eventImage' => $image
        ]);
    }

    public function actionGallery()
    {
        $images = Media::find()->all();
        $cats = Media::find()->select('image_size')->groupBy('image_size')->all();
        $category = new Media();
        $categories = $category->getCategories();

        return $this->render('gallery', ['images' => $images, 'cats' => $cats, 'categories' => $categories]);
    }


    /* public function actionCreateTour()
     {
         $model = new CreateYourTourForm();
         $banner = Settings::find()->where(['key' => 'Create tour banner'])->one();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
             $model->sendEmail($model->email);
             Yii::$app->session->setFlash('success', Yii::t('app', 'Thanks for Request,we will connect with you in 24 hours'));
         }

         return $this->render('create_your_tour', [
             'model' => $model,
             'banner' => $banner
         ]);
     }*/


    public function actionTravellersPersonalDataHandleRequest()
    {
        $model = new TravellersFormPersonalData();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $form = Form::saveData();
            if ($form) {
                $this->actionReport($form);
                $form[0]->sendEmail();
            }

            return $this->goHome();
        } else {
            return $this->renderPartial('details-form2', ['model' => $model]);
        }
    }

    public function actionTravellersDetailsHandleRequest()
    {
        $model = new TravellersFormDetails();
        $personalData = new TravellersFormPersonalData();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->set('form_second_details', Yii::$app->request->post());
            return $this->renderPartial('details-form2', ['model' => $personalData]);
        } else {
            return $this->renderPartial('details-form', ['model' => $model]);
        }
    }

    public function actionTravellersHandleRequest()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new TravellersForm();
        $details = new TravellersFormDetails();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->set('form_details', Yii::$app->request->post());
            return ($this->renderAjax('details-form', ['model' => $details]));
        } else {
            return $this->renderAjax('form', ['model' => $model]);
        }
    }

    public function actionCreateTour()
    {
        $model = new TravellersForm();
        $title = Settings::find()->where(['key' => 'Create Tour Title'])->one();
        $sub = Settings::find()->where(['key' => 'Create Tour Subtitle'])->one();
        return $this->render('travellers-form', ['model' => $model, 'title' => $title, 'subtitle' => $sub]);
    }

    public function actionReport($data)
    {
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('report', ['data' => $data[1]]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_FILE,
            'filename' => 'report.pdf',
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Voyage Armenia'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Request'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        return $pdf->render();
    }
}