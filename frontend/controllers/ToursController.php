<?php
/**
 * Created by PhpStorm.
 * User: vazge
 * Date: 1/21/2018
 * Time: 6:45 PM
 */

namespace frontend\controllers;

use common\models\Reviews;
use common\models\Settings;
use yii\filters\VerbFilter;
use frontend\controllers\Controller;
use frontend\controllers\SiteController;
use common\models\ToursSearch;
use common\models\Tours;
use common\models\Media;
use common\models\Routes;
use common\models\Destinations;
use yii;

class ToursController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionAllTours($id = null)
    {
        if ($id) {
            if (Yii::$app->request->get('sort'))
                $tours = ToursSearch::getTours(false, $id, true, Yii::$app->request->get('sort'));
            else {
                $tours = ToursSearch::getTours(false, $id, true);
            }
        } else {
            if (Yii::$app->request->get('sort'))
                $tours = ToursSearch::getTours(false, 0, false, Yii::$app->request->get('sort'));
            else {
                $tours = ToursSearch::getTours();
            }
        }

        $category = ToursSearch::getAllCategories();
        $toursBanner = Settings::find()->where(['key' => 'Tour Banner'])->one();
        $allTourTitle = Settings::find()->where(['key' => 'All Tours Title'])->one();
        $allTourDesc = Settings::find()->where(['key' => 'All Tours Description'])->one();
        $destinations = Destinations::find()->all();
        return $this->render('all_tours', [
            'tours' => $tours,
            'toursBanner' => $toursBanner,
            'destinations' => $destinations,
            'category' => $category,
            'allTourTitle' => $allTourTitle,
            'allTourDesc' => $allTourDesc
        ]);
    }

    public function actionLeaveReview()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $model = new Reviews();
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->setFlash('message', 'Thank you for review it\'s important for us ');

                $this->redirect(Yii::$app->request->getReferrer());
            } else {
                Yii::$app->session->setFlash('message', 'There was an error sending your message.');
            }
        }
        $this->redirect(Yii::$app->request->getReferrer().'#reviews');
    }

    public function actionDetails($id=false)
    {
        
         if (!$id) {
                     return $this->redirect('http://voyagearmenia.com/');
                  }
        $tour = Tours::find()->with(['destination', 'schedules'])->where(['id' => $id])->one();
        $ids = explode(',', $tour->attachments);
        $files = Media::find()->where(['in', 'id', $ids])->all();
        $reviews = Reviews::find()->where(['tour_id' => $id])->andWhere(['approved' => 1])->all();
        return $this->render('tour_details', ['tour' => $tour, 'attachments' => $files, 'reviews' => $reviews]);
    }

    public function actionIndex($id=false)
    {
        
         if (!$id) {
              $routes = Routes::find()->where(['url' => Yii::$app->request->pathInfo])->one();
            
            if($routes){
                $type = explode('_',$routes->type);
                $id = end($type);
            }else{
                  return $this->redirect('http://voyagearmenia.com/');
            }
        }
        $tour = Tours::find()->with(['destination', 'schedules'])->where(['id' => $id])->one();
        $ids = explode(',', $tour->attachments);
        $files = Media::find()->where(['in', 'id', $ids])->all();
        $reviews = Reviews::find()->where(['tour_id' => $id])->andWhere(['approved' => 1])->all();
        return $this->render('tour_details', ['tour' => $tour, 'attachments' => $files, 'reviews' => $reviews]);
    }

}