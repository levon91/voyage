<?php
/**
 * Created by PhpStorm.
 * User: Comp
 * Date: 15/02/2018
 * Time: 14:10
 */

namespace frontend\controllers;

class ChatController extends Controller
{
    public function actionSendChat()
    {
        if (!empty($_POST)) {
            echo \sintret\chat\ChatRoom::sendChat($_POST);
        }
    }
}