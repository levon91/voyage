<?php
/**
 * Created by PhpStorm.
 * User: vazge
 * Date: 1/28/2018
 * Time: 2:55 PM
 */

namespace frontend\controllers;

use common\models\Categories;
use common\models\Pages;
use common\models\Posts;
use common\models\Projects;
use common\models\Team;
use common\models\Routes;
use frontend\models\CreateYourTourForm;
use Yii;

class PostsController extends Controller
{


    public function actionIndex($id=false)
    {
        if (!$id) {
            
            $routes = Routes::find()->where(['url' => Yii::$app->request->pathInfo])->one();
            if($routes){
                $type = explode('_',$routes->type);
                $id = end($type);
            }else{
                  return $this->redirect('http://voyagearmenia.com/');
            }
            
        }
        $page = Posts::find()->where(['id' => $id])->one();
        if (!$page) {
            throw new \yii\web\NotFoundHttpException();
        } else {
            if ($page->token != null) {
                if (Yii::$app->request->get('token') && Yii::$app->request->get('token') == $page->token) {
                    return $this->render('view', ['post' => $page]);
                } else {
                    throw new \yii\web\NotFoundHttpException();
                }
            } else {
                return $this->render('view', ['post' => $page]);
            }
        }
    }


}