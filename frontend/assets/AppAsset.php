<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      //  '/css/site.css',
        '/css/bootstrap.min.css',
        '/css/fontello/css/all-fontello.min.css',
        '/css/style.css',
       // '/css/ion.rangeSlider.skinFlat.css',
      //  '/css/admin.css',
        '/css/animate.css',

       // '/css/animate.css',
        '/css/base.css',
       // '/css/blog.css',
        //'/css/blog.css',
        //'/css/color-aqua.css',
        //'/css/color-green.css',
        '/css/color-orange.css',
        '/css/custom.css',
//        '/css/date_time_picker.css',
        //'/css/datedropper.css',
        //'/css/extralayers.css',
        '/css/finaltilesgallery.css',
//        '/css/flavr.min.css',
        '/css/ion.rangeSlider.css',
//        '/css/skins/square/grey.css',
        '/css/menu.css',
        //'/rev-slider-files/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
//        '/rev-slider-files/fonts/font-awesome/css/font-awesome.css',
        //'/rev-slider-files/css/settings.css',
        '/css/mobile.css',

    ];
    public $js = [
//        'js/jquery-2.2.4.min.js"',
        '/js/common_scripts_min.js',
        '/js/functions.js',
//        '/rev-slider-files/js/jquery.themepunch.tools.min.js',
//        '/rev-slider-files/js/jquery.themepunch.revolution.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.actions.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.carousel.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.kenburn.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.layeranimation.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.migration.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.navigation.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.parallax.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.slideanims.min.js',
//        '/rev-slider-files/js/extensions/revolution.extension.video.min.js',
//        '/js/function.js',
//        '/js/notify_func.js',
        '/js/cat_nav_mobile.js',
        '/js/icheck.js',
        '/js/map.js',
//        '/js/infobox.js',
        '/js/getinfo.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
