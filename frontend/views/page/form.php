<?php
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;

//use kartik\form\ActiveField as KartikField;


$participants = [
    'just_me' => 'Just me', 'couple' => 'Couple', 'family' => 'Family', 'friends' => 'Friends', 'private_group' => 'Private group'
];
$flexible_dates = ['yes' => 'Yes', 'no' => 'No'];
$occasion = ['holidays' => 'Just for leisure', 'honneymoon' => 'Honneymoon', 'other' => 'Other'];
$private_group = ['club_association' => 'Club/association', 'company' => 'Company trip', 'conference_meeting' => 'Conference/meeting', 'other' => 'Other'];
$date_approximate_length = ['-1' => 'Advise me', 1 => 'Less than a week', 7 => '1 week', 14 => '2 weeks', 21 => '3 weeks', 22 => 'More than 3 weeks'];
$age_groups = ['adults', 'teenagers', 'kids', 'babies'];
$date_approximate_start = [];
$date_approximate_start['-1'] = 'Advise me';
$currentYear = date('Y');

$nextYear = date('Y-m-d', strtotime(date('Y-m-d', time()) . '+ 2 year'));

$start = (new DateTime(date('Y-m-d', time())))->modify('first day of this month');
$end = (new DateTime($nextYear))->modify('first day of next month');
$interval = DateInterval::createFromDateString('1 month');
$period = new DatePeriod($start, $interval, $end);

foreach ($period as $dt) {
    $date_approximate_start[$dt->format("Y-m-d")] = $dt->format("Y-F");
}


?>
<?php $form = ActiveForm::begin(
    ['id' => 'travellers-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => false,
        'options' => ['class' => 'form-horizontal']]);
?>
    <p class="form-description">
        <?= $subtitle ? $subtitle->value : '' ?>
    </p>
    <h2 class="form-label"><?= Yii::t('app', 'To begin with:') ?></h2>

<?=
$form->field($model, 'participants', ['options' => ['class' => 'form-group radio-label']])
    ->radioList($participants,
        ['itemOptions' => ['class' => 'participants_item']])->label(Yii::t('app', 'Who will be travelling?'));
?>
<?=

$form->field($model, 'occasion', ['options' => ['class' => 'form-group radio-label participants-subfield ' . ($model->participants == 'couple' ? '' : 'hidden')]])
    ->radioList($occasion, ['itemOptions' => ['class' => 'occasion_item']])->label(Yii::t('app', 'Who will be travelling?'));
?>

<?=
$form->field($model, 'private_group', ['options' => ['class' => 'form-group radio-label participants-subfield ' . ($model->participants == 'private_group' ? '' : 'hidden')]])
    ->radioList($private_group, ['itemOptions' => ['class' => 'private-group-item']])->label('Please specify');
?>


    <div
        class="form-group age-groups participants-subfield row <?= $model->participants == 'family' || $model->participants == 'friends' || $model->participants == 'private_group' ? '' : 'hidden' ?>">
        <?php foreach ($age_groups as $group): ?>
            <?php
            $min = 0;
            ?>
            <?= $form->field($model, $group,
                [
                    'options' => ['class' => 'col-sm-3'],
                    'template' => '
                <div class="age-group-wrapper">{label}
                    <div class="age-group">
                        {input}
                        <div class="qty-btns">
                            <div class="qty-btn btn-decrement">&minus;</div>
                            <div class="qty-btn btn-increment">&plus;</div>
                        </div>
                        {error}{hint}
                    </div>
                    <div class="clear"></div>
                </div>'
                ]
            )->textInput(['type' => 'number', 'min' => $min]); ?>
        <?php endforeach; ?>
        <div class="clear"></div>
    </div>

<?= $form->field($model, 'age_groups')->hiddenInput()->label(false); ?>

<?=
$form->field($model, 'flexible_dates', ['options' => ['class' => 'form-group radio-label flexible_dates']])
    ->radioList($flexible_dates, ['itemOptions' => ['class' => 'flexible_dates_item']]);
?>

<?=
$form->field($model, 'travel_dates',
    [
        'options' => ['class' => 'form-group dates-group ' . ($model->flexible_dates != 'yes' ? 'hidden' : '')]
    ])
    ->widget(DateRangePicker::classname()); ?>

    <div class="form-group dropdowns-block dates-group <?= $model->flexible_dates != 'no' ? 'hidden' : ''; ?>">
        <div class="col-sm-2">
            <?= $form->field($model, 'date_approximate_start', ['options' => ['class' => 'form-group date-box-item']])
                ->dropDownList(
                    $date_approximate_start
                ); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'date_approximate_length', ['options' => ['class' => 'date-box-item']])
                ->dropDownList(
                    $date_approximate_length
                ); ?>
        </div>
    </div>

<?= $form->field($model, 'approximate_dates')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']); ?>
    </div>

<?php ActiveForm::end(); ?>