<?php setlocale(LC_TIME, Yii::$app->language.'_'.strtoupper(Yii::$app->language).'.UTF-8');?>
<div class="strip_booking">
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <div class="date">
                <span class="month"><?= strftime("%b", strtotime($model->date)); ?></span>
                <span
                    class="day"><strong><?= strftime("%d", strtotime($model->date)); ?></strong>
                    <?= strftime("%a", strtotime($model->date)); ?></span>
            </div>
        </div>
        <div class="col-md-6 col-sm-5">
            <h3 class="text-left no-padd"><?= $model->title; ?></h3>
            <p><?= $model->content; ?></p>
        </div>
    </div>
</div>