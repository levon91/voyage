<section class="parallax-window" data-parallax="scroll" data-image-src="/uploads/projects/<?= $project->banner; ?>"
         data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1><?= $project->title ?></h1>
                    <span><?= $project->caption ?></span>

                </div>
            </div>
        </div>
</section>
<!-- End section -->
<div class="container">
    <div class="row project_row">
        <div class="col-md-12 col-xs-12 col-sm-12 project_content">
           <?= $project->content ?>
        </div>
    </div>
</div>