<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Html;

?>

    <section class="parallax-window" data-parallax="scroll"
             data-image-src="/uploads/media/<?= $eventImage; ?>"
             data-natural-width="1400" data-natural-height="470">
    </section>
    <div class="container">
        </section>
        <section id="section-1">
            <div id="tools">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="styled-select-filters">
                            <select name="sort_date" id="sort_date">
                                <option value="" selected><?= Yii::t('app', 'Sort by date'); ?></option>
                                <option value="latest"><?= Yii::t('app', 'Latest'); ?></option>
                                <option value="oldest"><?= Yii::t('app', 'Oldest'); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php Pjax::begin();
            $url = Yii::$app->request->getPathInfo();
            ?>
            <?= Html::a("sort", ['/' . $url . '?sort=asc'], ['class' => 'hidden', 'id' => 'asc']) ?>
            <?= Html::a("sort", ['/' . $url . '?sort=desc'], ['class' => 'hidden', 'id' => 'desc']) ?>
            <!--/tools -->

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'event',
                'summary' => false
            ]) ?>
            <?php Pjax::end(); ?>

        </section>
        <!-- End section 1 -->
    </div>
<?php
$lang = Yii::$app->language;
$script = <<< JS
    $(document).on('change','#sort_date',function(e) {  
        if($(this).val() == 'oldest')
            $("#asc").click();
        if($(this).val() == 'latest')
            $("#desc").click();
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>