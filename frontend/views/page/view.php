<?php
/**
 * Created by PhpStorm.
 * User: Levon
 * Date: 22/02/2018
 * Time: 21:34
 */
/* @var $this yii\web\View */

use yii\helpers\Html;

$pageMeta = \frontend\components\Helper::getPagMetas($page->id);

foreach ($pageMeta as $meta) {
    $this->registerMetaTag([
        'name' => $meta->key,
        'content' => $meta->value
    ]);
}

$this->params['breadcrumbs'][] = $this->title;
?>

<section class="parallax-window" data-parallax="scroll"
         data-image-src="/uploads/pages/<?= is_object($page) ? $page->cover : ''; ?>"
         data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1><?= $page->title; ?></h1>
        </div>
    </div>
</section>
<!-- End Section -->

<div class="container">
    <h1><?= Html::encode($page->title) ?></h1>

    <div class="about">
        <?= $page->content ?>
    </div>

</div>
