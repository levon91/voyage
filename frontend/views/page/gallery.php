<div class="container margin_160">
    <div class="main_title gallery_title">
        <h1><?= Yii::t('app', 'Gallery'); ?></h1>

    </div>
    <hr>

    <div id="gallery" class="final-tiles-gallery  effect-dezoom effect-fade-out caption-top social-icons-right">
        <div class="ftg-filters">
            <a href="#ftg-set-ftgall">All</a>
            <?php foreach ($categories as $cat): ?>
                <a href="#ftg-set-<?= $cat->title == '' ? Yii::t('app', 'Uncategorized') : str_replace(' ','-',$cat->title); ?>"><?= $cat->title == '' ? Yii::t('app', 'Uncategorized') : $cat->title; ?></a>
            <?php endforeach; ?>
        </div>
        <div class="ftg-items">
            <?php foreach ($images as $image): ?>
                <div
                        class="tile ftg-set-<?= isset($image->gallery) == '' ? Yii::t('app', 'Uncategorized') : str_replace(' ','-', $image->gallery->title); ?>">
                    <a class="tile-inner" data-lightbox="gallery"
                       href="/uploads/media/<?= $image->uniq_name ?>">
                        <img class="item"
                             src=""
                             data-src="/uploads/media/<?= $image->uniq_name ?>">
                    </a>
                </div>
                <!-- End image -->
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('/css/finaltilesgallery.css');
$this->registerCssFile('/css/lightbox2.css');

$this->registerJsFile('js/jquery.finaltilesgallery.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('js/lightbox2.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJs("  $(function () {
        'use strict';
        //we call Final Tiles Grid Gallery without parameters,
        //see reference for customisations: http://final-tiles-gallery.com/index.html#get-started
        $(\".final-tiles-gallery\").finalTilesGallery();
    });", yii\web\View::POS_END);
?>