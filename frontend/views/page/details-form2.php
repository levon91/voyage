<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$currentYear = date('Y');
?>

<?php $form=ActiveForm::begin(['id' => 'personal-data-form',
    'enableAjaxValidation' => false, 'enableClientValidation' => false,
    'options' => ['class' => 'form-horizontal']
    ]);
?>
<h2 class="form-label">
    Final step: contact details
</h2>
<?=$form->field($model, 'first_name')->textInput();?>
<?=$form->field($model, 'last_name')->textInput();?>
<?=$form->field($model, 'email')->textInput(['type' => 'email']);?>
<?=$form->field($model, 'phone')->textInput();?>
<div class="flex birth-date">
    <?=$form->field($model, 'birth_day')->textInput(['type' => 'number', 'min' => 1, 'max' => 31]);?>
    <?=$form->field($model, 'birth_month')->textInput(['type' => 'number', 'min' => 1, 'max' => 12]);?>
    <?=$form->field($model, 'birth_year')->textInput(['type' => 'number', 'min' => 1870, 'max' => $currentYear]);?>
</div>
<?=$form->field($model, 'residence_country')->dropDownList($model->getCountryList());?>
<?=$form->field($model, 'accept')->checkbox(['checked' => false]);?>
<?=Html::submitButton('Submit', ['class' => 'btn btn-success'])?>
<?php ActiveForm::end(); ?>