<?php use toriphes\lazyload\LazyLoad;?>
<?php foreach ($tours as $tour) : ?>
    <div class="col-md-4 col-sm-6 wow zoomIn tour_padding" data-wow-delay="0.1s">
        <div class="tour_container">
            <div class="img_container">
                <a href="/<?= $tour->route ? Yii::$app->language.'/'.$tour->route->url : Yii::$app->language.'/tours/details/'.$tour->id; ?>">
                    <?php echo LazyLoad::widget(['src' => "/uploads/tours/".$tour->cover,'options' => ['class' =>  'img-responsive']]);
                    ?>
                    <div class="short_info">
                        <h3 class="price"><?php if($tour->price){
                                echo $tour->price."<sup>AMD</sup>";
                            }else{echo '';} ?></h3>
                        <h3><strong><?= $tour->title ?></strong></h3>
                    </div>
                </a>
            </div>

        </div>
        <!-- End box tour -->
    </div>
<?php endforeach; ?>