<div class="white_bg">
    <div class="container team_page ">
        <div class="main_title team-title-text">
            <h2><span><?= is_object($team_title) ?  $team_title -> value : '' ?></span></h2>
        </div>

        <div class="team-description">
            <?= is_object($team_description) ?  $team_description -> value : '' ?>
        </div>
        <div class="row">
            <?php foreach ($team as $people): ?>
            <div class="col-md-4 col-sm-6 text-center">
                <img src="/uploads/team/<?= $people->cover ?>" width="100%" alt="Pic" class="img-responsive">
                <h4><span><?= $people->name . ' ' . $people->surname ?></span></h4>
                <div class="position"><?= $people->position ?></div>
                <div class="scrollbar" id="style-15">
                    <div class="force-overflow"> <p><?= $people->info ?></p></div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

