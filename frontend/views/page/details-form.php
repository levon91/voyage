<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin(
    ['id' => 'details-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => false,
//        'action' => '/custom/travellers-handle-request',
        'options' => ['class' => 'form-horizontal']]);
?>
<p class="form-description">
    Don't worry if you're not sure about the specifics yet, everything in this form can be modified by your local agent during the
    trip-planning process.
</p>
<h2 class="form-label">
    Describe your ideal trip for us:
</h2>

<?= $form->field($model, 'accommodation')->checkboxList($model->getAccommodation()); ?>

<?= $form->field($model, 'additional')->textInput(); ?>

<?= $form->field($model, 'budget')->textInput(['type' => 'number']); ?>

<?= $form->field($model, 'accompaniment')->radioList($model->getAccompaniment()); ?>

<?= $form->field($model, 'plan')->radioList($model->getPlan()); ?>

<?= Html::submitButton('Submit', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>