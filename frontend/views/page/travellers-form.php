
<div class="create_your_tour container">
    <h2 class="text-center"><?= $title ? $title->value : '' ?></h2>
    <div class="main-form col-md-8">
        <?= $this->render('form', ['model' => $model,'subtitle' =>$subtitle ]) ?>
    </div>
</div>
<?php
$this->registerJsFile("@web/js/form-handling.js", [
    'depends' => [
        \yii\web\JqueryAsset::className()
    ]
]);
?>
<?php
$this->registerJs("$('.dropdown.languages.lang').on('click',function(){
$(this).toggleClass('open');
})", yii\web\View::POS_READY);
?>
