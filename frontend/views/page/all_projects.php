<?php
use yii\helpers\Html;

?>

<section class="parallax-window" data-parallax="scroll"
         data-image-src="/uploads/media/<?php echo is_object($projectBanner) ? $projectBanner->value : ''; ?>"
         data-natural-width="1400"
         data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1><?= Yii::t('app','All Projects')?></h1>
        </div>
    </div>
</section>


<main>

    <div class="container margin_60">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <?php echo $this->render('//posts/projects', [
                        'projects' => $projects
                    ]); ?>
                </div>
                <hr>


            </div>
            <!-- End col lg 9 -->
        </div>
        <!-- End row -->
    </div>
    <!-- End container -->
</main>
<!-- End main -->


<?php $this->registerJsFile(
    "/js/infobox.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>
