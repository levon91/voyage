<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use common\models\Categories;

$this->title = 'Create Your Tour';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="parallax-window" data-parallax="scroll"
         data-image-src="/uploads/media/<?= is_object($banner) ? $banner->value : ''; ?>"
         data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-2">
    </div>
</section>
<div class="container">
    <h1 class="create_tour_title"><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>

    <?php endif; ?>
    <p class="create_tour_subtitle">
        <?= $subtitle ? $subtitle->value : '' ?>
    </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-6">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'phoneNumber') ?>
                <?= $form->field($model, 'destination')->dropDownList(ArrayHelper::map(\common\models\Destinations::find()->all(), 'title', 'title')) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'tour_category')->dropDownList(ArrayHelper::map(Categories::find()->where(['for_tour' => 1])->all(), 'title', 'title')) ?>
                <?= $form->field($model, 'food')->dropDownList(['Not Chosen' => Yii::t('app', 'Choose'), 'Lunch' => Yii::t('app', 'Lunch'), 'Dinner' => Yii::t('app', 'Dinner'), 'all' => Yii::t('app', 'All'), 'Other' => Yii::t('app', 'Other')]) ?>
                <?= $form->field($model, 'transportation')->dropDownList(['0' => Yii::t('app', 'Choose'), 'Yes' => Yii::t('app', 'Yes'), 'No' => Yii::t('app', 'No')]) ?>
                <?= $form->field($model, 'persons')->dropDownList(array_combine(array_values(range(1, 10)), range(1, 10))) ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 4]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary submit_btn', 'name' => 'contact-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
