<?php
use yii\widgets\Pjax;
use yii\helpers\Html;

$dais = \frontend\components\Helper::getWorkingDais();
$info = \frontend\components\Helper::getInfo();
?>

    <section class="parallax-window fill" data-parallax="scroll"
             data-src="/uploads/media/<?= is_object($toursBanner) ? $toursBanner->value : ''; ?>"
             data-natural-width="1400"
             data-natural-height="470"
    >
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1><?= is_object($allTourTitle) ?  $allTourTitle -> value : '' ?></h1>
                <p><?= is_object($allTourDesc) ? $allTourDesc -> value : '' ?></p>
            </div>
        </div>
    </section>
<main>
    <!--    <div id="position">-->
    <!--        <div class="container">-->
    <!--            <ul>-->
    <!--                <li><a href="#">Home</a>-->
    <!--                </li>-->
    <!--                <li><a href="#">Category</a>-->
    <!--                </li>-->
    <!--                <li>Page active</li>-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </div>-->
    <!-- Position -->

        <div class="collapse" id="collapseMap">
            <div id="map" class="map"></div>
        </div>
        <!-- End Map -->

        <div class="container margin_60">
            <div class="row">
                <?php Pjax::begin();

                $url = Yii::$app->request->getPathInfo();
                ?>
                <aside class="col-lg-3 col-md-3">
                    <p>
                        <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false"
                           aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View
                            on
                            map</a>
                    </p>
                    <?= Html::a("update", ['/' . $url . '?sort=desc'], ['class' => 'hidden', 'id' => 'asc']) ?>
                    <?= Html::a("update", ['/' . $url . '?sort=asc'], ['class' => 'hidden', 'id' => 'desc']) ?>
                    <div class="box_style_cat">
                        <ul id="cat_nav">
                            <?php
                            foreach ($category as $categories) : ?>

                                    <?php
                                    if ($categories->categories == null) {
                                        continue;
                                    }
                                    echo '<li>';
                                    $title = $categories->categories->title; ?>
                                    <?php $id = $categories->categories != null ? $categories->categories->id : ''; ?>
                                    <?= Html::a("<i
                                            class=\"icon_set_1_icon-51\"></i>
                                        $title
                                        <span><?= $categories->cnt ?></span>", ['tours/all-tours/' . $id]); ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>


                    <div class="box_style_4">
                        <i class="icon_set_1_icon-90"></i>
                        <h4><span><?= Yii::t('app', 'Book') ?></span> <?= Yii::t('app', 'by email or phone'); ?></h4>
                        <a href="tel://<?= $info['Email'] ?>"
                           class="phone phone_all_tours"> <?= $info['Email'] ?></a>
                        <a href="tel://<?= $info['Phone'] ?>"
                           class="email"> <?= $info['Phone'] ?></a>
                       <br>
                        <small><?= Yii::t('app', 'Monday to Friday') ?> <?= $dais->value;?></small>
                    </div>
                </aside>
                <!--End aside -->

                <div class="col-lg-9 col-md-8" style="margin-bottom: 15px">


                        <div class="row" id="tools">
                            <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 10px">
                                <div class="styled-select-filters">
                                    <select name="sort_price" id="sort_price">
                                        <option value="" selected>Sort by price</option>
                                        <option value="asc">Lowest price</option>
                                        <option value="desc">Highest price</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    <!--End tools -->


                    <div class="row">
                        <?php echo $this->render('//page/tours', [
                            'tours' => $tours
                        ]); ?>
                    </div>
                    <?php Pjax::end(); ?>


                    <hr>


                </div>
                <!-- End col lg 9 -->
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
    </main>
    <!-- End main -->
<?php $this->registerJs(
    "$('#cat_nav').mobileMenu()", yii\web\View::POS_READY
)
?>

<?php $this->registerJsFile(
    "/js/map.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php
$markers = "";
foreach ($destinations as $destination) {
    $markers .= "'$destination->title': [
			{
				name: '" . $destination->title . "',
				location_latitude: " . $destination->latitude . ", 
				location_longitude: " . $destination->longitude . ",
				map_image_url: '/uploads/destinations/" . $destination->cover . "',
				name_point: '" . $destination->title . "',
				description_point: '" . $destination->description . "',
				get_directions_start_address: ''
			//	phone: '+3934245255',
			//	url_point: 'single_hotel.html'
			}
			],";
}
?>

<?php $this->registerJs(
    "function initMap(){
    window.destinations = { 
    " . $markers . "
			}
    }", yii\web\View::POS_HEAD
)
?>

<?php $this->registerJsFile(
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyB4qW1Bs0GS8BpufPkYby0x2Em5UbL8CUM&callback=initMap", ['depends' => \yii\web\JqueryAsset::className()]
)
?>



<?php $this->registerJsFile(
    "/js/infobox.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>
<?php
$lang = Yii::$app->language;
$script = <<< JS
    $(document).on('change','#sort_price',function(e) {  
        if($(this).val() == 'desc')
            $("#asc").click();
        if($(this).val() == 'asc')
            $("#desc").click();
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<?php $this->registerJsFile('@web/js/jquery.lazy.min.js',['depends' => 'yii\web\YiiAsset']) ?>
<?php
$js = <<<JS
        $(function() {
        $('.fill').lazy();
    });
JS;
$this->registerJs($js);
//?>
