<?php
/**
 * Created by PhpStorm.
 * User: Levon
 * Date: 19/02/2018
 * Time: 13:53
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


$model = new \common\models\Reviews();
?>
<div class="row">
    <div class="col-md-3">
        <h3><?= Yii::t('app', 'Reviews'); ?> </h3>
        <a href="#" class="btn_1 add_bottom_30" data-toggle="modal"
           data-target="#myReview"><?= Yii::t('app', 'Leave a review'); ?></a>
    </div>
    <div class="col-md-9">
        <div id="general_rating"><?= count($reviews); ?> <?= Yii::t('app', 'Reviews'); ?>

        </div>
        <hr>
        <?php foreach ($reviews as $review): ?>
            <div class="review_strip_single">
                <small><?= $review->created_date; ?></small>
                <h4><?= $review->client_name; ?></h4>
                <p>
                    <?= $review->message ?>
                </p>
            </div>
        <?php endforeach; ?>

        <!-- End review strip -->
    </div>
</div>
<!-- Modal Review -->
<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myReviewLabel"><?= Yii::t('app', 'Write your review') ?></h4>
            </div>
            <div class="modal-body">
                <div id="message-review">
                </div>
                <?php $form = ActiveForm::begin(['action' => ['/tours/leave-review'],]); ?>
                    <?= $form->field($model, 'tour_id')->hiddenInput(['value' => ($tour->id)])->label(false);
                    ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= $form->field($model, 'client_name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Full Name')]) ?>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Your email')]) ?>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="form-group">

                    <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', 'Write your review')]) ?>
                </div>

                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn_1', 'id' => 'submit-review']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- End modal review -->
