<?php
$dais = \frontend\components\Helper::getWorkingDais();
$info = \frontend\components\Helper::getInfo();

use toriphes\lazyload\LazyLoad;
use yii\helpers\Url;
?>

<section class="parallax-window fill" data-parallax="scroll" data-src="/uploads/tours/<?= $tour->banner; ?>"
         data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1><?= $tour->title ?></h1>
                    <span><?= $tour->caption ?></span>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div id="price_single_main">
                        from/per person
                        <span> <?= $tour->price != null ? $tour->price . "<sup>AMD</sup>" : ''; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section -->

<main>

    <?php if (Yii::$app->session->hasFlash('message')): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('message'); ?>
        </div>

    <?php endif; ?>
    <div class="collapse" id="collapseMap">
        <div id="map" class="map"></div>
    </div>
    <!-- End Map -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-8" id="single_tour_desc">

                <p class="visible-sm visible-xs"><a class="btn_map" data-toggle="collapse" href="#collapseMap"
                                                    aria-expanded="false" aria-controls="collapseMap"
                                                    data-text-swap="Hide map" data-text-original="View on map">View on
                        map</a>
                </p>
                <!-- Map button for tablets/mobiles -->

                <div id="Img_carousel" class="slider-pro">

                    <div class="sp-slides">
                        <?php foreach ($attachments as $attach) : ?>
                            <div class="sp-slide">
                                <?php echo LazyLoad::widget(['src' => "/uploads/media/".$attach->uniq_name,'options' =>
                                    [
                                        'class' =>  'sp-image',
                                        'data-src' =>  '/uploads/media/'.$attach->uniq_name,
                                        'data-small' =>  '/uploads/media/'.$attach->uniq_name,
                                        'data-medium' =>  '/uploads/media/'.$attach->uniq_name,
                                        'data-large' =>  '/uploads/media/'.$attach->uniq_name,
                                        'data-retina' =>  '/uploads/media/'.$attach->uniq_name,
                                    ]
                                ]);?>

                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="sp-thumbnails">
                        <?php foreach ($attachments as $thumbs) : ?>
                            <img alt="Image" class="sp-thumbnail" src="/uploads/media/<?= $thumbs->uniq_name ?>">
                        <?php endforeach; ?>
                    </div>

                </div>

                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <h3><?= Yii::t('app', 'Description') ?></h3>
                    </div>
                    <div class="col-md-9">
                        <h4><?= $tour->title ?></h4>
                        <?= $tour->content; ?>
                        <!-- End row  -->
                    </div>
                </div>
                <hr>
                <?php if ($tour->schedule): ?>
                    <div class="row">
                        <div class="col-md-3">
                            <h3>Schedule</h3>
                        </div>
                        <div class="col-md-9">
                            <div class=" table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                    <?php
                                    if ($tour->schedule):
                                        foreach ($tour->schedules as $item):?>
                                            <th colspan="2">
                                                <p>From: <?= $item->time_from ?> -
                                                    To: <?= $item->time_to ?></p>
                                            </th>
                                        <?php endforeach;
                                    endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                <?php endif; ?>

                <?php echo $this->render('reviews', ['reviews' => $reviews, 'tour' => $tour]); ?>
            </div>
            <!--End  single_tour_desc-->

            <aside class="col-md-4">
                <p class="hidden-sm hidden-xs">
                    <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false"
                       aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on
                        map</a>
                </p>

                <div class="box_style_4">
                    <i class="icon_set_1_icon-90"></i>
                    <h4><span><?= Yii::t('app', 'Book') ?></span> <?= Yii::t('app', 'by email or phone'); ?></h4>
                    <a href="tel://<?= $info['Email'] ?>"
                       class="phone"> <?= $info['Email'] ?></a>
                    <a href="tel://<?= $info['Phone'] ?>"
                       class="email tour_email"> <?= $info['Phone'] ?></a><br>
                    <small><?= Yii::t('app', 'Monday to Friday') ?> <?= isset($dais->value) ? $dais->value : '' ?></small>
                </div>

            </aside>
<!--            <div class="fb-like" data-href="--><?php //echo Url::base(true).Url::current() ?><!--" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>-->
<!--            <div id="fb-root"></div>-->
        </div>
        <!--End row -->
    </div>
    <!--End container -->

    <div id="overlay"></div>
    <!-- Mask on input focus -->

</main>
<!-- End main -->

<?php $this->registerJsFile(
    "https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.3&appId=315881398941390&autoLogAppEvents=1", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerJsFile(
    "/js/icheck.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>
<?php $this->registerJs(
    "
		
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	
	", yii\web\View::POS_READY
)
?>

<?php $this->registerJsFile(
    "/js/jquery.sliderPro.min.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerJs(
    "
		$(document).ready(function ($) {
			$('#Img_carousel').sliderPro({
				width: 960,
				height: 500,
				fade: true,
				arrows: true,
				buttons: false,
				fullScreen: false,
				smallSize: 500,
				startSlide: 0,
				mediumSize: 1000,
				largeSize: 3000,
				thumbnailArrows: true,
				autoplay: false
			});
		});
	", yii\web\View::POS_READY
)
?>

<?php $this->registerJsFile(
    "/js/bootstrap-datepicker.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>


<?php $this->registerJsFile(
    "/js/bootstrap-timepicker.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerJs(
    "
		
		$('input.date-pick').datepicker('setDate', 'today');
		$('input.time-pick').timepicker({
			minuteStep: 15,
			showInpunts: false
		})
	
	", yii\web\View::POS_READY
)
?>

<?php $this->registerJsFile(
    "/js/assets/validate.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerCssFile(
    "/css/slider-pro.min.css"
) ?>

<?php $this->registerJsFile(
    "/js/map.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>
<?php
$markers = "";
foreach ($tour->destination as $destination) {
    $markers .= "'$destination->title': [
			{
				name: '" . $destination->title . "',
				location_latitude: " . $destination->latitude . ", 
				location_longitude: " . $destination->longitude . ",
				map_image_url: '/uploads/destinations/" . $destination->cover . "',
				name_point: '" . $destination->title . "',
				description_point: '" . $destination->description . "',
				get_directions_start_address: ''
				phone: '" . Yii::$app->session->get('tel') . "',
			//	url_point: 'single_hotel.html'
			}
			],";
}
?>
<?php $this->registerJs(
    "function initMap(){
    window.destinations = { 
    " . $markers . "
			}
    }", yii\web\View::POS_HEAD
)
?>
<?php $this->registerJsFile(
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyB4qW1Bs0GS8BpufPkYby0x2Em5UbL8CUM&callback=initMap", ['depends' => \yii\web\JqueryAsset::className()]
)
?>



<?php $this->registerJsFile(
    "/js/infobox.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>
<?php
$this->registerJs('$("#review").on("submit",function(e){
    e.preventDefault();
    console.log($("form#review").serialize())
})', yii\web\View::POS_END);
?>
<?php $this->registerJsFile('@web/js/jquery.lazy.min.js',['depends' => 'yii\web\YiiAsset']) ?>
<?php
$js = <<<JS
        $(function() {
        $('.fill').lazy();
    });
JS;
$this->registerJs($js);
//?>