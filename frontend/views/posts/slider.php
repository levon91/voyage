<div class="main-menu">
    <div id="header_menu">
        <img src="img/logo_sticky.png" width="160" height="34" alt="City tours" data-retina="true">
    </div>
    <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
    <ul>
        <li class="submenu">
            <a href="javascript:void(0);" class="show-submenu">Home <i class="icon-down-open-mini"></i></a>
            <ul>
                <li><a href="javascript:void(0);">Revolution slider</a>
                    <ul>
                        <li><a href="index.html">Default slider</a></li>
                        <li><a href="index_20.html">Advanced slider</a></li>
                        <li><a href="index_14.html">Youtube Hero</a></li>
                        <li><a href="index_15.html">Vimeo Hero</a></li>
                        <li><a href="index_17.html">Youtube 4K</a></li>
                        <li><a href="index_16.html">Carousel</a></li>
                        <li><a href="index_19.html">Mailchimp Newsletter</a></li>
                        <li><a href="index_18.html">Fixed Caption</a></li>
                    </ul>
                </li>
                <li><a href="index_12.html">Layer slider</a></li>
                <li><a href="index_2.html">With Only tours</a></li>
                <li><a href="index_3.html">Single image</a></li>
                <li><a href="index_4.html">Header video</a></li>
                <li><a href="index_7.html">With search panel</a></li>
                <li><a href="index_13.html">With tabs</a></li>
                <li><a href="index_5.html">With map</a></li>
                <li><a href="index_6.html">With search bar</a></li>
                <li><a href="index_8.html">Search bar + Video</a></li>
                <li><a href="index_9.html">With Text Rotator</a></li>
                <li><a href="index_10.html">With Cookie Bar (EU law)</a></li>
                <li><a href="index_11.html">Popup Advertising</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a href="javascript:void(0);" class="show-submenu">Tours <i class="icon-down-open-mini"></i></a>
            <ul>
                <li><a href="all_tours_list.html">All tours list</a></li>
                <li><a href="all_tours_grid.html">All tours grid</a></li>
                <li><a href="all_tours_map_listing.html">All tours map listing</a></li>
                <li><a href="single_tour.html">Single tour page</a></li>
                <li><a href="single_tour_with_gallery.html">Single tour with gallery</a></li>
                <li><a href="javascript:void(0);">Single tour fixed sidebar</a>
                    <ul>
                        <li><a href="single_tour_fixed_sidebar.html">Single tour fixed sidebar</a></li>
                        <li><a href="single_tour_with_gallery_fixed_sidebar.html">Single tour 2 Fixed Sidebar</a></li>
                        <li><a href="cart_fixed_sidebar.html">Cart Fixed Sidebar</a></li>
                        <li><a href="payment_fixed_sidebar.html">Payment Fixed Sidebar</a></li>
                        <li><a href="confirmation_fixed_sidebar.html">Confirmation Fixed Sidebar</a></li>
                    </ul>
                </li>
                <li><a href="single_tour_working_booking.php">Single tour working booking</a></li>
                <li><a href="single_tour_datepicker_v2.html">Date and time picker V2</a></li>
                <li><a href="cart.html">Single tour cart</a></li>
                <li><a href="payment.html">Single tour booking</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a href="javascript:void(0);" class="show-submenu">Hotels <i class="icon-down-open-mini"></i></a>
            <ul>
                <li><a href="all_hotels_list.html">All hotels list</a></li>
                <li><a href="all_hotels_grid.html">All hotels grid</a></li>
                <li><a href="all_hotels_map_listing.html">All hotels map listing</a></li>
                <li><a href="single_hotel.html">Single hotel page</a></li>
                <li><a href="single_hotel_datepicker_adv.html">Single hotel datepicker adv</a></li>
                <li><a href="single_hotel_datepicker_v2.html">Date and time picker V2</a></li>
                <li><a href="single_hotel_working_booking.php">Single hotel working booking</a></li>
                <li><a href="single_hotel_contact.php">Single hotel contact working</a></li>
                <li><a href="cart_hotel.html">Cart hotel</a></li>
                <li><a href="payment_hotel.html">Booking hotel</a></li>
                <li><a href="confirmation_hotel.html">Confirmation hotel</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a href="javascript:void(0);" class="show-submenu">Transfers <i class="icon-down-open-mini"></i></a>
            <ul>
                <li><a href="all_transfer_list.html">All transfers list</a></li>
                <li><a href="all_transfer_grid.html">All transfers grid</a></li>
                <li><a href="single_transfer.html">Single transfer page</a></li>
                <li><a href="single_transfer_datepicker_v2.html">Date and time picker V2</a></li>
                <li><a href="cart_transfer.html">Cart transfers</a></li>
                <li><a href="payment_transfer.html">Booking transfers</a></li>
                <li><a href="confirmation_transfer.html">Confirmation transfers</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a href="javascript:void(0);" class="show-submenu">Restaurants <i class="icon-down-open-mini"></i></a>
            <ul>
                <li><a href="all_restaurants_list.html">All restaurants list</a></li>
                <li><a href="all_restaurants_grid.html">All restaurants grid</a></li>
                <li><a href="all_restaurants_map_listing.html">All restaurants map listing</a></li>
                <li><a href="single_restaurant.html">Single restaurant page</a></li>
                <li><a href="single_restaurant_datepicker_v2.html">Date and time picker V2</a></li>
                <li><a href="payment_restaurant.html">Booking restaurant</a></li>
                <li><a href="confirmation_restaurant.html">Confirmation transfers</a></li>
            </ul>
        </li>
        <li class="megamenu submenu">
            <a href="javascript:void(0);" class="show-submenu-mega">Bonus<i class="icon-down-open-mini"></i></a>
            <div class="menu-wrapper">
                <div class="col-md-4">
                    <h3>Header styles</h3>
                    <ul>
                        <li><a href="index.html">Default transparent</a></li>
                        <li><a href="header_2.html">Plain color</a></li>
                        <li><a href="header_3.html">Plain color on scroll</a></li>
                        <li><a href="header_4.html">With socials on top</a></li>
                        <li><a href="header_5.html">With language selection</a></li>
                        <li><a href="header_6.html">With lang and conversion</a></li>
                        <li><a href="header_7.html">With full horizontal menu</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Footer styles</h3>
                    <ul>
                        <li><a href="index.html">Footer default</a></li>
                        <li><a href="footer_2.html">Footer style 2</a></li>
                        <li><a href="footer_3.html">Footer style 3</a></li>
                        <li><a href="footer_4.html">Footer style 4</a></li>
                        <li><a href="footer_5.html">Footer style 5</a></li>
                        <li><a href="footer_6.html">Footer style 6</a></li>
                        <li><a href="footer_7.html">Footer style 7</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Shop &amp; colors</h3>
                    <ul>
                        <li><a href="shop.html">Shop</a></li>
                        <li><a href="shop-single.html">Shop single</a></li>
                        <li><a href="shopping-cart.html">Shop cart</a></li>
                        <li><a href="checkout.html">Shop Checkout</a></li>
                    </ul>
                </div>
            </div><!-- End menu-wrapper -->
        </li>
        <li class="megamenu submenu">
            <a href="javascript:void(0);" class="show-submenu-mega">Pages<i class="icon-down-open-mini"></i></a>
            <div class="menu-wrapper">
                <div class="col-md-4">
                    <h3>Pages</h3>
                    <ul>
                        <li><a href="about.html">About us</a></li>
                        <li><a href="general_page.html">General page</a></li>
                        <li><a href="tourist_guide.html">Tourist guide</a></li>
                        <li><a href="wishlist.html">Wishlist page</a></li>
                        <li><a href="faq.html">Faq</a></li>
                        <li><a href="faq_2.html">Faq smooth scroll</a></li>
                        <li><a href="pricing_tables.html">Pricing tables</a></li>
                        <li><a href="gallery_3_columns.html">Gallery 3 columns</a></li>
                        <li><a href="gallery_4_columns.html">Gallery 4 columns</a></li>
                        <li><a href="grid_gallery_1.html">Grid gallery</a></li>
                        <li><a href="grid_gallery_2.html">Grid gallery with filters</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Pages</h3>
                    <ul>
                        <li><a href="contact_us_1.html">Contact us 1</a></li>
                        <li><a href="contact_us_2.html">Contact us 2</a></li>
                        <li><a href="blog_right_sidebar.html">Blog</a></li>
                        <li><a href="blog.html">Blog left sidebar</a></li>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="invoice.html" target="_blank">Invoice</a></li>
                        <li><a href="404.html">404 Error page</a></li>
                        <li><a href="site_launch/index.html">Site launch / Coming soon</a></li>
                        <li><a href="timeline.html">Tour timeline</a></li>
                        <li><a href="page_with_map.html"><i class="icon-map"></i> Full screen map</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Elements</h3>
                    <ul>
                        <li><a href="footer_2.html"><i class="icon-columns"></i> Footer with working newsletter</a></li>
                        <li><a href="footer_5.html"><i class="icon-columns"></i> Footer with Twitter feed</a></li>
                        <li><a href="icon_pack_1.html"><i class="icon-inbox-alt"></i> Icon pack 1 (1900)</a></li>
                        <li><a href="icon_pack_2.html"><i class="icon-inbox-alt"></i> Icon pack 2 (100)</a></li>
                        <li><a href="icon_pack_3.html"><i class="icon-inbox-alt"></i> Icon pack 3 (30)</a></li>
                        <li><a href="icon_pack_4.html"><i class="icon-inbox-alt"></i> Icon pack 4 (200)</a></li>
                        <li><a href="icon_pack_5.html"><i class="icon-inbox-alt"></i> Icon pack 5 (360)</a></li>
                        <li><a href="shortcodes.html"><i class="icon-tools"></i> Shortcodes</a></li>
                        <li><a href="newsletter_template/newsletter.html" target="blank"><i class=" icon-mail"></i>
                                Responsive email template</a></li>
                        <li><a href="admin.html"><i class="icon-cog-1"></i> Admin area</a></li>
                        <li><a href="general_page.html"><i class="icon-light-up"></i> Weather Forecast</a></li>
                    </ul>
                </div>
            </div><!-- End menu-wrapper -->
        </li>
    </ul>
</div><!-- End main-menu -->
<ul id="top_tools">
    <li>
        <div class="dropdown dropdown-search">
            <a href="#" class="search-overlay-menu-btn" data-toggle="dropdown"><i class="icon-search"></i></a>
        </div>
    </li>
    <li>
        <div class="dropdown dropdown-cart">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class=" icon-basket-1"></i>Cart (0) </a>
            <ul class="dropdown-menu" id="cart_items">
                <li>
                    <div class="image"><img src="img/thumb_cart_1.jpg" alt="image"></div>
                    <strong>
                        <a href="#">Louvre museum</a>1x $36.00 </strong>
                    <a href="#" class="action"><i class="icon-trash"></i></a>
                </li>
                <li>
                    <div class="image"><img src="img/thumb_cart_2.jpg" alt="image"></div>
                    <strong>
                        <a href="#">Versailles tour</a>2x $36.00 </strong>
                    <a href="#" class="action"><i class="icon-trash"></i></a>
                </li>
                <li>
                    <div class="image"><img src="img/thumb_cart_3.jpg" alt="image"></div>
                    <strong>
                        <a href="#">Versailles tour</a>1x $36.00 </strong>
                    <a href="#" class="action"><i class="icon-trash"></i></a>
                </li>
                <li>
                    <div>Total: <span>$120.00</span></div>
                    <a href="cart.html" class="button_drop">Go to cart</a>
                    <a href="payment.html" class="button_drop outline">Check out</a>
                </li>
            </ul>
        </div><!-- End dropdown-cart-->
    </li>
</ul>
</nav>
</div>
</div><!-- container -->
</header><!-- End Header -->

<main>
    <div id="rev_slider_54_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="notgeneric1"
         data-source="gallery"
         style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
        <div id="rev_slider_54_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-140" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                    data-masterspeed="2000" data-thumb="rev-slider-files/assets/100x50_notgeneric_bg1.jpg"
                    data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                    data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3=""
                    data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                    data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="rev-slider-files/assets/notgeneric_bg1.jpg" alt="" data-bgposition="center center"
                         data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                         data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-140-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','46','36','28']" data-lineheight="['46','46','36','28']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 46px; font-weight: 700;font-family:Montserrat;">
                        WELCOME TO CITYTOURS
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption NotGeneric-SubTitle   tp-resizeme" id="slide-140-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; white-space: nowrap; font-weight: 400;font-family:Montserrat;">TOURS HOTELS
                        RESTAURANTS
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme" id="slide-140-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; white-space: nowrap;cursor:default;"><i class="pe-7s-compass"></i></div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption NotGeneric-Button rev-btn " id="slide-140-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['124','124','124','123']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="button"
                         data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                         data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1);bc:rgba(255, 255, 255, 1);bw:1 1 1 1;"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[30,30,30,30]"
                         style="z-index: 8; white-space: nowrap; font-weight: 400;font-family:Montserrat;border-color:rgba(255,255,255,0.50);border-width:1px 1px 1px 1px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        NEXT GOODIE
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption rev-scroll-btn " id="slide-140-layer-9"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                         data-width="35" data-height="55" data-whitespace="nowrap"
                         data-visibility="['on','on','on','off']" data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                         data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 100; color: transparent;border-color:rgba(255, 255, 255, 0.5);border-style:solid;border-width:1px 1px 1px 1px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        <span></span>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-141" data-transition="fadetotopfadefrombottom" data-slotamount="default"
                    data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut"
                    data-easeout="Power3.easeInOut" data-masterspeed="1500"
                    data-thumb="rev-slider-files/assets/100x50_notgeneric_bg5.jpg" data-rotate="0"
                    data-saveperformance="off" data-title="Chill" data-param1="" data-param2="" data-param3=""
                    data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                    data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="rev-slider-files/assets/notgeneric_bg5.jpg" alt="" data-bgposition="center center"
                         data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                         data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-3" id="slide-141-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','46','36','28']" data-lineheight="['46','46','36','28']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 46px; font-weight: 700;font-family:Montserrat;">
                        DISCOVER NICE PLACES
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-2" id="slide-141-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; white-space: nowrap; font-weight: 400;font-family:Montserrat;">TOURS HOTELS
                        RESTAURANTS
                    </div>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-1" id="slide-141-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; white-space: nowrap;cursor:default;"><i class="pe-7s-mouse"></i></div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption NotGeneric-Button rev-btn " id="slide-141-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['124','124','124','123']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="button"
                         data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                         data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1);bc:rgba(255, 255, 255, 1);bw:1 1 1 1;"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[30,30,30,30]"
                         style="z-index: 8; white-space: nowrap; font-weight: 400;font-family:Montserrat;border-color:rgba(255,255,255,0.50);border-width:1px 1px 1px 1px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        NEXT GOODIE
                    </div>

                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption rev-scroll-btn " id="slide-141-layer-9"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                         data-width="35" data-height="55" data-whitespace="nowrap"
                         data-visibility="['on','on','on','off']" data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                         data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 100; color: transparent;border-color:rgba(255, 255, 255, 0.5);border-style:solid;border-width:1px 1px 1px 1px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        <span></span>
                    </div>

                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-8" id="slide-141-layer-10"
                         data-x="['left','left','left','left']" data-hoffset="['680','680','680','680']"
                         data-y="['top','top','top','top']" data-voffset="['632','632','632','632']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 10;">
                        <div class="rs-looped rs-pendulum" data-easing="linearEaseNone" data-startdeg="-20"
                             data-enddeg="360" data-speed="35" data-origin="50% 50%"><img
                                    src="rev-slider-files/assets/blurflake4.png" alt=""
                                    data-ww="['240px','240px','240px','240px']"
                                    data-hh="['240px','240px','240px','240px']" data-no-retina></div>
                    </div>

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-7" id="slide-141-layer-11"
                         data-x="['left','left','left','left']" data-hoffset="['948','948','948','948']"
                         data-y="['top','top','top','top']" data-voffset="['487','487','487','487']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 11;">
                        <div class="rs-looped rs-wave" data-speed="20" data-angle="0" data-radius="50px"
                             data-origin="50% 50%"><img src="rev-slider-files/assets/blurflake3.png" alt=""
                                                        data-ww="['170px','170px','170px','170px']"
                                                        data-hh="['170px','170px','170px','170px']" data-no-retina>
                        </div>
                    </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-141-layer-12"
                         data-x="['left','left','left','left']" data-hoffset="['719','719','719','719']"
                         data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 12;">
                        <div class="rs-looped rs-rotate" data-easing="Power2.easeInOut" data-startdeg="-20"
                             data-enddeg="360" data-speed="20" data-origin="50% 50%"><img
                                    src="rev-slider-files/assets/blurflake2.png" alt=""
                                    data-ww="['50px','50px','50px','50px']" data-hh="['51px','51px','51px','51px']"
                                    data-no-retina></div>
                    </div>

                    <!-- LAYER NR. 14 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-6" id="slide-141-layer-13"
                         data-x="['left','left','left','left']" data-hoffset="['187','187','187','187']"
                         data-y="['top','top','top','top']" data-voffset="['216','216','216','216']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 13;">
                        <div class="rs-looped rs-wave" data-speed="4" data-angle="0" data-radius="10"
                             data-origin="50% 50%"><img src="rev-slider-files/assets/blurflake1.png" alt=""
                                                        data-ww="['120px','120px','120px','120px']"
                                                        data-hh="['120px','120px','120px','120px']" data-no-retina>
                        </div>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-142" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                    data-masterspeed="2000" data-thumb="rev-slider-files/assets/100x50_notgeneric_bg2.jpg"
                    data-rotate="0" data-saveperformance="off" data-title="Enjoy Nature" data-param1="" data-param2=""
                    data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                    data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="rev-slider-files/assets/notgeneric_bg2.jpg" alt="" data-bgposition="center center"
                         data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100"
                         data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0"
                         data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg"
                         data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 15 -->
                    <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-142-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['1','0','0','0']"
                         data-fontsize="['50','48','36','28']" data-lineheight="['46','48','36','28']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 46px; font-weight: 700;font-family:Montserrat;">
                        GREAT ADVENTURES
                    </div>

                    <!-- LAYER NR. 16 -->
                    <div class="tp-caption NotGeneric-SubTitle   tp-resizeme" id="slide-142-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; white-space: nowrap; font-weight: 400;font-family:Montserrat;">TOURS HOTELS
                        RESTAURANTS
                    </div>

                    <!-- LAYER NR. 17 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme" id="slide-142-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; white-space: nowrap;cursor:default;"><i class="pe-7s-expand1"></i></div>

                    <!-- LAYER NR. 18 -->
                    <div class="tp-caption NotGeneric-Button rev-btn " id="slide-142-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['124','124','124','123']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="button"
                         data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                         data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1);bc:rgba(255, 255, 255, 1);bw:1 1 1 1;"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[30,30,30,30]"
                         style="z-index: 8; white-space: nowrap; font-weight: 400;font-family:Montserrat;border-color:rgba(255,255,255,0.50);border-width:1px 1px 1px 1px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        NEXT GOODIE
                    </div>

                    <!-- LAYER NR. 19 -->
                    <div class="tp-caption rev-scroll-btn " id="slide-142-layer-9"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                         data-width="35" data-height="55" data-whitespace="nowrap"
                         data-visibility="['on','on','on','off']" data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                         data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 100; color: transparent;border-color:rgba(255, 255, 255, 0.5);border-style:solid;border-width:1px 1px 1px 1px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        <span></span>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-143" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                    data-masterspeed="2000" data-thumb="rev-slider-files/assets/100x50_iceberg.jpg" data-rotate="0"
                    data-saveperformance="off" data-title="Iceberg" data-param1="" data-param2="" data-param3=""
                    data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                    data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="rev-slider-files/assets/iceberg.jpg" alt="" data-bgposition="center center"
                         data-bgfit="cover" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- BACKGROUND VIDEO LAYER -->
                    <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute"
                         data-videowidth="100%" data-videoheight="100%"
                         data-videomp4="rev-slider-files/assets/iceberg.mp4" data-videopreload="auto"
                         data-videoloop="loopandnoslidestop" data-forceCover="1" data-aspectratio="16:9"
                         data-autoplay="true" data-autoplayonlyfirsttime="false"></div>
                    <!-- LAYER NR. 20 -->
                    <div class="tp-caption tp-shape tp-shapewrapper  " id="slide-143-layer-10"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape"
                         data-basealign="slide" data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);"></div>

                    <!-- LAYER NR. 21 -->
                    <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-143-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','48','36','28']" data-lineheight="['48','48','36','28']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 48px; font-weight: 700;font-family:Montserrat;">
                        ADVENTURE TOURS
                    </div>

                    <!-- LAYER NR. 22 -->
                    <div class="tp-caption NotGeneric-SubTitle   tp-resizeme" id="slide-143-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; white-space: nowrap; font-weight: 400;font-family:Montserrat;">TOURS HOTELS
                        RESTAURANTS
                    </div>

                    <!-- LAYER NR. 23 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme" id="slide-143-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 8; white-space: nowrap;cursor:default;"><i class="pe-7s-anchor"></i></div>

                    <!-- LAYER NR. 24 -->
                    <div class="tp-caption NotGeneric-Button rev-btn " id="slide-143-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['124','124','124','123']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="button"
                         data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                         data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1);bc:rgba(255, 255, 255, 1);bw:1 1 1 1;"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[30,30,30,30]"
                         style="z-index: 9; white-space: nowrap; font-weight: 400;font-family:Montserrat;border-color:rgba(255,255,255,0.50);border-width:1px 1px 1px 1px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        NEXT GOODIE
                    </div>

                    <!-- LAYER NR. 25 -->
                    <div class="tp-caption rev-scroll-btn " id="slide-143-layer-9"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                         data-width="35" data-height="55" data-whitespace="nowrap"
                         data-visibility="['on','on','on','off']" data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                         data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 100; color: transparent;border-color:rgba(255, 255, 255, 0.5);border-style:solid;border-width:1px 1px 1px 1px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        <span></span>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-144" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                    data-masterspeed="2000" data-thumb="rev-slider-files/assets/100x50_notgeneric_bg3.jpg"
                    data-rotate="0" data-saveperformance="off" data-title="Hiking" data-param1="" data-param2=""
                    data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                    data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="rev-slider-files/assets/notgeneric_bg3.jpg" alt="" data-bgposition="center center"
                         data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                         data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 26 -->
                    <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-144-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','48','36','28']" data-lineheight="['48','48','36','28']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 48px; font-weight: 700;font-family:Montserrat;">
                        WANT THIS TOO?
                    </div>

                    <!-- LAYER NR. 27 -->
                    <div class="tp-caption NotGeneric-SubTitle   tp-resizeme" id="slide-144-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-fontweight="['400','500','500','500']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; white-space: nowrap; font-weight: 400;font-family:Montserrat;">GET CITYTOURS
                        TODAY
                    </div>

                    <!-- LAYER NR. 28 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme" id="slide-144-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; white-space: nowrap;cursor:default;"><i class="pe-7s-diamond"></i></div>

                    <!-- LAYER NR. 29 -->
                    <div class="tp-caption rev-scroll-btn " id="slide-144-layer-9"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                         data-width="35" data-height="55" data-whitespace="nowrap"
                         data-visibility="['on','on','on','off']" data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"0px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                         data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 8; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 100; color: transparent;border-color:rgba(255, 255, 255, 0.5);border-style:solid;border-width:1px 1px 1px 1px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                        <span></span>
                    </div>

                    <!-- LAYER NR. 30 -->
                    <a class="tp-caption NotGeneric-Button rev-btn "
                       href="https://themeforest.net/item/citytours-city-tours-tour-tickets-and-guides/10715647?ref=ansonika"
                       target="_blank" id="slide-144-layer-10" data-x="['center','center','center','center']"
                       data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                       data-voffset="['124','125','124','104']" data-fontweight="['400','500','500','500']"
                       data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions=''
                       data-responsive_offset="on" data-responsive="off"
                       data-frames='[{"delay":2000,"speed":300,"frame":"0","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1);bc:rgba(255, 255, 255, 1);bw:1 1 1 1;"}]'
                       data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]"
                       data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]"
                       data-paddingleft="[30,30,30,30]"
                       style="z-index: 9; white-space: nowrap; font-weight: 400;font-family:Montserrat;border-color:rgba(255,255,255,0.50);border-width:1px 1px 1px 1px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">BUY
                        ON ENVATO </a>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</main>