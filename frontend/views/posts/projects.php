<?php use toriphes\lazyload\LazyLoad;?>
<div class="container-fluid">
    <div class="row">
        <?php foreach ($projects as $project): ?>
            <div class="col-md-4 col-sm-12 col-xs-12 wow zoomIn colprojects" data-wow-delay="0.1s">
                <div class="projectbox">
                    <div class="img_container project">
                        <a href="<?= Yii::$app->language;?>/page/projects/<?= $project->id; ?>">
                        <?php echo LazyLoad::widget(['src' => '/uploads/projects/'.$project->cover,'options' => ['class' =>  'img-responsive project_img']]) ?>
                        </a>
                    </div>
                    <h3 class="project_name"><?= $project->title; ?></h3>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>