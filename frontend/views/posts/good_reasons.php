
<div class="container-fluid">
    <div class="row">
        <?php foreach ($projects as $project): ?>
            <div class="col-md-4 col-sm-12 col-xs-12 wow zoomIn colprojects" data-wow-delay="0.1s">
                <div class="tour_container projectbox">
                    <div class="img_container project">
                        <a href="#">
                            <h1 class="toptourname"><?= $project->title;?></h1>
                            <h3 class="toptourdesc"><?= $project->caption;?></h3>
                            <img src="/uploads/projects/<?=$project->cover?>" class="img-responsive project_img" alt="image">
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>