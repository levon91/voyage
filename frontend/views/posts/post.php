<?php setlocale(LC_TIME, Yii::$app->language . "_" . strtoupper(Yii::$app->language)); ?>
<div class="post">
    <?php if ($model->banner): ?>
        <a href="/posts/<?= $model->id ?>"><img src="/uploads/posts/<?= $model->banner ?>"
                                                    alt="<?= $model->title ?>"
                                                    class="img-responsive">
        </a>
    <?php endif; ?>
    <div class="post_info clearfix">
        <div class="post-left">
            <ul>
                <li><i class="icon-calendar-empty"></i> <?= Yii::t('app', 'On') ?>
                    <span><?= date('Y-m-d', strtotime($model->created_at)); ?></span>
                </li>
                <li><i class="icon-inbox-alt"></i> <?= Yii::t('app', 'In') ?> <a
                        href="/page/categories/<?= $model->category_id ?>"><?= $model->category->title ?></a>
                </li>
            </ul>
        </div>

    </div>
    <h2><?= $model->title ?></h2>
    <p>
        <?= $model->caption ?>
    </p>
    <a href="/<?= $model->route ? Yii::$app->language.'/'.$model->route->url : Yii::$app->language.'/posts/'.$model->id; ?>" class="btn_1"><?= Yii::t('app', 'Read more'); ?></a>
</div>
<hr>