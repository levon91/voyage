<?php
use yii\bootstrap\Carousel;

$this->title = $post->title;
$this->params['breadcrumbs'][] = $this->title;

$postMeta = \frontend\components\Helper::getPostMetas($post->id);

foreach ($postMeta as $meta) {
    $this->registerMetaTag([
        'name' => $meta->key,
        'content' => $meta->value
    ]);
}
?>

<section class="parallax-window" data-parallax="scroll" data-image-src="/uploads/posts/<?= $post->banner; ?>"
         data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1><?= $post->title ?></h1>
                    <span><?= $post->caption ?></span>
                    <!--                    <span class="rating"><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i-->
                    <!--                            class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small></span>-->
                </div>
            </div>
        </div>
    </div>
</section>

<?php

$this->registerMetaTag([
    'property' => 'description',
    'content' => strip_tags($post->caption),
]);

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Yii::$app->request->absoluteUrl,
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title,
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => strip_tags($post->caption)
]);
if ($post->cover) {
    $image = 'uploads/posts/' . $post->id . "/" . $post->cover;

    $this->registerMetaTag([
        'property' => 'og:image',
        'content' => $image,
    ]);
}

?>


<div id="fb-root"></div>

<?php

$this->registerJs("(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = \"//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8&appId=585250881526994\";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));", yii\web\View::POS_HEAD)

?>
<div id="fb-root"></div>
<div class="container">
    <div class="content col-md-12 col-sm-12 col-xs-12 post-info">
        <h1><?= $post->title; ?></h1>


        <?= $post->content; ?>

    </div>

    <div class="fb-share-button" data-href="http://new.logosngo.org/post/<?= $post->id ?>" data-layout="button"
         data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank"
                                                        href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Поделиться</a>
    </div>

    <?php
    $this->registerJs("(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = \"//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10&appId=585250881526994\";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
", yii\web\View::POS_END)

    ?>
</div>
