<div class="col-md-8 col-sm-6 hidden-xs">
    <img src="img/laptop.png" alt="Laptop" class="img-responsive laptop">
</div>
<div class="col-md-4 col-sm-6">
    <h3><span>Get started</span> with CityTours</h3>
    <p>
        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
    </p>
    <ul class="list_order">
        <li><span>1</span>Select your preferred tours</li>
        <li><span>2</span>Purchase tickets and options</li>
        <li><span>3</span>Pick them directly from your office</li>
    </ul>
    <a href="all_tour_list.html" class="btn_1">Start now</a>
</div>