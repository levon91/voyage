<?php
/**
 * Created by PhpStorm.
 * User: Comp
 * Date: 22/02/2018
 * Time: 22:33
 */
 
 
$categoryMeta = \frontend\components\Helper::getCategoryMetas($category->id);

foreach ($categoryMeta as $meta) {
    $this->registerMetaTag([
        'name' => $meta->key,
        'content' => $meta->value
    ]);
}

?>
<?php if ($category != null): ?>
    <section class="parallax-window" data-parallax="scroll"
             data-image-src="/uploads/categories/<?= $category->cover; ?>"
             data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1><?= $category->title; ?></h1>
            </div>
        </div>
    </section>
<?php endif; ?>
<main>
    <div class="container margin_60">
        <div class="box_style_1">
            <?php

            echo \yii\widgets\ListView::widget([
                'summary' => false,
                'dataProvider' => $posts,
                'itemView' => 'post'
            ]);
            ?>
        </div>
    </div>
    <!-- End container -->
</main>
