<?php
use common\models\Menus;
use \frontend\components\Helper;

$menusObj = new Menus();
$menus = $menusObj->getMenus('top');
$menuItems = '';
$menuItems = '<ul>';

$languages = Yii::$app->params['lang'];
unset($languages[Yii::$app->language]);
?>
<?php

foreach ($menus as $menu): ?>
    <?php
    $items = empty($menu['items']);

    $href = !$items ? 'javascript:void(0);' : $menu['url'][0];
    $class = !$items ? 'submenu' : '';
    $menuItems .= '<li class="' . $class . '">
            <a href="'.'/'  . Yii::$app->language .'/'.$href . '"
               class="show-submenu">' . $menu['label'] . '
                
            ';
    if (!$items) {
        $menuItems .= '<span class="caret"></span></a>'.Yii::$app->helper->getMenuChild($menu['items']);
    }else{
        $menuItems .= '</a>';
    }
    $menuItems . '</li>';
    ?>
<?php endforeach; ?>
<?php $menuItems .= '</ul>'; ?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                        <h1><a href="/<?= Yii::$app->language ?>"></a></h1>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="/img/logo.png" width="160" height="34" alt="City tours" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <?= $menuItems; ?>
                    </div>
                    <ul id="top_tools">
                        <div class="dropdown languages lang">
                            <button class="btn btn-primary dropdown-toggle lang_btn lang_<?= Helper::getLanguage(); ?>" type="button"
                                    data-toggle="dropdown"><?= Helper::getLanguage(); ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu langs">
                                <?php foreach ($languages as $key => $lang): ?>
                                    <li><a href="/" data-lang="<?= $key ?>"><?= $lang ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </ul>
                </nav>
            </div>
        </div><!-- container -->
    </header><!-- End Header -->


    <!--menu icon style-->
    <!--    <i class="icon-down-open-mini"></i>-->


<?php

$url = Yii::$app->request->getUrl();
$url = explode('/', $url);
unset($url[1]);
$url = implode('/', $url);

$query = Yii::$app->request->queryParams != '' ? Yii::$app->request->queryString : '';

$this->registerJs('
    $(".langs li a").on("click",function(e){
        e.preventDefault();
        var lang = $(this).data("lang");
        var url = "/"+lang+ "' . $url . $query . '";
        window.location.href = url;        
    })
');
?>