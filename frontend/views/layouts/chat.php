<?php $messages = Yii::$app->helper->getUserMessages(); ?>

<div class="container text-center chat_position">
    <div class="row">
        <div class="round hollow text-center">
            <div href="" id="addClass"><span class="glyphicon glyphicon-comment"></span>Chat</div>
        </div>
    </div>
</div>


<div class="popup-box chat-popup" id="qnimate">
    <div class="popup-head">
        <div class="popup-head-left pull-left">
            <i class="icon_set_1_icon-57" style="font-size:23px;background: white;border-radius: 34px;"></i> <?= Yii::t('app', 'Online Agent') ?>
        </div>
        <div class="popup-head-right pull-right">
            <div class="btn-group">
                <!--                <button class="chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false">-->
                <!--                    <i class="glyphicon glyphicon-cog"></i></button>-->
                <ul role="menu" class="dropdown-menu pull-right">
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Clear Chat</a></li>
                    <li><a href="#">Email Chat</a></li>
                </ul>
            </div>

            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i
                    class="glyphicon glyphicon-off icon_off"></i></button>
        </div>
    </div>
    <div class="popup-messages">


        <div id="direct-chat-messages" class="direct-chat-messages">


            <div class="chat-box-single-line">
                <abbr class="timestamp"></abbr>
            </div>
            <?php foreach ($messages as $message): ?>
                <div class="direct-chat-msg doted-border">
                    <?php if($message->is_admin == 1):?>
                        <i class="icon_set_1_icon-57" style="font-size:25px;background: white;border-radius: 34px;"></i>
                    <?php else: ?>
                    <i class="pe-7s-user" style="font-size:35px;background: white;border-radius: 34px;"></i>
                    <?php endif;?>
                    <div class="direct-chat-text"><?= $message->message; ?></div>
                </div>
                <div class="direct-chat-info clearfix">
                    <span
                        class="direct-chat-timestamp pull-right"><?= date('H:i', strtotime($message->created_at)); ?></span>
                </div>
            <?php endforeach; ?>
            <div class="hidden unavailable">
                <div class="direct-chat-msg doted-border">
                    <i class="icon_set_1_icon-57" style="font-size:35px;background: white;border-radius: 34px;"></i>
                    <div
                        class="direct-chat-text"><?= Yii::t('app', 'We are available from 10 to 20 on GMT+4 timezone') ?></div>
                </div>
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-timestamp pull-right"></span>
                </div>
            </div>

            <!-- Message. Default to the left -->

            <!-- /.direct-chat-text -->
        </div>
        <!-- /.direct-chat-msg -->


    </div>
    <div class="popup-messages-footer">
        <textarea id="status_message" placeholder="Type a message..." rows="10" cols="40" name="message"></textarea>
    </div>
</div>

<?php
$this->registerJsFile(
    "/js/chat.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerCssFile(
    "/css/chat.css"
) ?>
<?php
date_default_timezone_set('Asia/Yerevan');
$hour = date('H', time());
if (intval($hour) > 19 || intval($hour) < 10)
    $this->registerJs(
        '$(".unavailable").removeClass("hidden")'
    ) ?>

