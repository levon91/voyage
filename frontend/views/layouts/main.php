<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\components\Helper;

AppAsset::register($this);


$data = Helper::getFooterMenus();
Yii::$app->session->set('tel', $data['footerPhone']);
Yii::$app->session->set('address', $data['address']);
$globalMetas = Helper::getGlobalMetas();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href=“https://fonts.googleapis.com/css?family=IBM+Plex+Serif” rel=“stylesheet”>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
      <?php foreach ($globalMetas as $meta): ?>
              <meta name="<?= $meta->key ?>" content="<?= $meta->value ?>">
    <?php endforeach; ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127321089-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127321089-1');
</script>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('header'); ?>


<div id="home_page">
    <?= $content ?>
    <?= $this->render('chat') ?>
</div>


<?= $this->render('footer', ['data' => $data]); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
