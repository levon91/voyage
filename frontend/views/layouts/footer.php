<?php
$footerPhone = $data['footerPhone'];
$footerEmail = $data['footerEmail'];
$footerAddress = $data['address'];
$menuItems = $data['menuItems'];
$footerMenuItems = $data['footerMenuItems'];
$footerMenuItems2 = $data['footerMenuItems2'];
$social = $data['social'];
$footer_menu = $data['footer_menu']
?>
<footer class="revealed">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <h3><?= Yii::t('app', 'Need help?') ?></h3>
                <ul class="footer-ul">
                    <li>
                        <div class="info_div">
                            <i class="icon-address-1 footer_address footer_icon"></i>
                            <?php if(isset( $data['footer_menu'][0]['value_'.Yii::$app->language])):?>
                                <?= $data['footer_menu'][0]['value_'.Yii::$app->language] ?>
                            <?php endif?>

                        </div>
                    </li>
                    <li>
                        <div class="info_div">
                            <i class="icon_set_1_icon-89 footer_icon"></i>
                            <?php if(isset( $data['footer_menu'][1]['value_'.Yii::$app->language])):?>
                            <?= $data['footer_menu'][1]['value_'.Yii::$app->language] ?>
                            <?php endif?>
                        </div>
                    </li>
                    <li>
                        <div class="info_div">
                            <i class="icon-mail footer_icon"></i>
                            <?php if(isset( $data['footer_menu'][2]['value_'.Yii::$app->language])):?>
                            <?= $data['footer_menu'][2]['value_'.Yii::$app->language] ?>
                            <?php endif?>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-3 menu-footer">
                <h3><?= Yii::t('app', 'About'); ?></h3>
                <ul>
                    <?= $footerMenuItems2; ?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-3 menu-footer">
                <h3><?= Yii::t('app', 'Discover'); ?></h3>
                <ul>
                    <?= $footerMenuItems; ?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 fb_iframe">
                <div id="fb-root"></div>
                <div class="fb-page" data-height="150" data-width="100"  data-href="https://www.facebook.com/VoyageArmenia1/"
                     data-tabs="timeline" data-small-header="false" data-adapt-container-width="true"
                     data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/VoyageArmenia1/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/VoyageArmenia1/"><?= Yii::t('app', 'Voyage Armenia'); ?></a>
                    </blockquote>
                </div>
            </div>

        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <?php foreach ($social as $item): ?>
                            <li><a href="<?= $item->value ?>"><i class="icon-<?= $item->key ?>"></i></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
</footer><!-- End footer -->

<div id="toTop"></div><!-- Back to top button -->

<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.12&appId=122302048448600&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>