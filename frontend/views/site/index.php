<?php

/* @var $this yii\web\View */

$this->title = 'voyage';
$homeMeta = \frontend\components\Helper::getHomeMetas();


foreach ($homeMeta as $meta) {
    $this->registerMetaTag([
        'name' => $meta->key,
        'content' => $meta->value
    ]);
}
?>

<!--<div id="preloader">-->
<!--    <div class="sk-spinner sk-spinner-wave">-->
<!--        <div class="sk-rect1"></div>-->
<!--        <div class="sk-rect2"></div>-->
<!--        <div class="sk-rect3"></div>-->
<!--        <div class="sk-rect4"></div>-->
<!--        <div class="sk-rect5"></div>-->
<!--    </div>-->
<!--</div>-->
<!-- End Preload -->
<main>
    <?php

    if (!$slide_video)
        echo $this->render('slider', ['slider' => $slider]);
    else
        echo $this->render('video-slider', ['slide_video' => $slide_video]);
    ?>
    <div id="top_title">
        <h2><?= is_object($topTitle) ?  $topTitle -> value : '' ?></h2>
    </div>
    <div class="toptours">
        <?php echo $this->render('topTours', [
            'topTours' => $topTours
        ]); ?>
    </div>
    <div class="clearfix"></div>
    <div class="container action_margin">
        <div class="row">
            <?php echo $this->render('actions', ['services' => $services]); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="container-fluid gal-container small_tours">
        <div class="row gal_row">
            <?php echo $this->render('//page/tours', [
                'tours' => $tours
            ]); ?>
        </div>
        <p class="text-center add_bottom_30">
            <a href="<?= Yii::$app->language;?>/tours/all-tours" class="btn_1 medium"><i class="icon-eye-7"></i>
                <?= Yii::t('app', 'View all tours')?> (<?= $count ?>)
            </a>
        </p>
            <?php echo $this->render('reviews', ['reviews' => $reviews, 'tour' => 0, 'review' => $review]); ?>
    </div>
    <div class="clearfix"></div>
    <div>
        <?php
        echo $this->render('gallery', [
           // 'home_gallery' => $home_gallery,
            'big' => $big,
            'small' => $small
        ]); ?>
    </div>
    <div class="clearfix"></div>

    <?=
    $this->render('funy_banner', [
        'banners' => $banners
    ]);
    ?>

    <!-- End section -->
    <div class="clearfix"></div>

    <div class="project_section">
        <?php
        echo $this->render('//posts/projects', ['projects' => $projects]);
        ?>
    </div>
    <div class="clearfix"></div>

    <?=
    $this->render('platforms',['platforms' => $platforms,'ourAgency' =>$ourAgency]);
    ?>
    <!-- End row -->
    <div class="clearfix"></div>
</main>
<!-- End main -->
