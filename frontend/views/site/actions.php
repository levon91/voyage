<div class="col-md-3 col-sm-6 text-center col-xs-12">
    <p>
        <a href="<?= Yii::$app->language?><?= $services['Create Your Own Tour']->link ?>"><span
                class="glyphicon glyphicon-pencil create_tour"></span></a>
    </p>
    <h4><span><?= $services['Create Your Own Tour']->title; ?></span></h4>
    <p>
        <?= $services['Create Your Own Tour']->content; ?>
    </p>
</div>
<div class="col-md-3 col-sm-6 text-center col-xs-12">
    <p>
        <a href="<?= Yii::$app->language?><?= $services['News']->link ?>"><span class="glyphicon glyphicon-star-empty create_tour"></span></a>
    </p>
    <h4><span><?= $services['News']->title; ?></span></h4>
    <p>
        <?= $services['News']->content; ?>
    </p>
</div>
<div class="col-md-3 col-sm-6 text-center col-xs-12">
    <p>
        <a href="<?= Yii::$app->language?><?= $services['Events']->link ?>"><span class="glyphicon glyphicon-bookmark create_tour "></span></a>
    </p>
    <h4><span><?= $services['Events']->title; ?></span></h4>
    <p>
        <?= $services['Events']->content; ?>
    </p>
</div>
<div class="col-md-3 col-sm-6 text-center col-xs-12">
    <p>
        <a href="<?= Yii::$app->language?><?= $services['Blog']->link ?>"><span class="glyphicon glyphicon-globe create_tour"></span></a>
    </p>
    <h4><span><?= $services['Blog']->title; ?></span></h4>
    <p>
        <?= $services['Blog']->content; ?>
    </p>
</div>