<section>
    <div class="container-fluid gal-container">
        <div class="row gal_row">
            <?php use toriphes\lazyload\LazyLoad;

            foreach ($big as $bigs) : ?>

                <div class="col-md-6 col-sm-12 co-xs-12 gal-item big_gal">
                    <div class="box_big">
                        <a href="#" data-toggle="modal" data-target="#1">
                            <?php echo LazyLoad::widget(['src' => '/uploads/media/'.$bigs->uniq_name,
                                'options' => ['class' =>  'big_img']]) ?>

                        </a>
                        <div class="modal fade" id="1" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">×</span></button>
                                    <div class="modal-body">
                                        <img src="/uploads/media/<?=$bigs->uniq_name?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php foreach ($small as $smalls) : ?>
                <div class="col-md-3 col-sm-6 co-xs-12 gal-item">
                    <div class="box">
                        <a href="#" data-toggle="modal" data-target="#<?= $smalls->id?>">
                            <?php echo LazyLoad::widget(['src' => '/uploads/media/'.$smalls->uniq_name]) ?>
                        </a>
                        <div class="modal fade" id="<?= $smalls->id?>" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">×</span></button>
                                    <div class="modal-body">
                                        <img src="/uploads/media/<?= $smalls->uniq_name ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
    </div>
</section>

<?php
$this->registerCssFile(
    '/css/gallery.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]
);
?>