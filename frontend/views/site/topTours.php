<div class="container-fluid">
    <div class="row">
        <?php use toriphes\lazyload\LazyLoad;

        foreach ($topTours as $tour): ?>
            <div class="col-md-4 col-sm-12 col-xs-12 wow zoomIn coltoptour" data-wow-delay="0.1s">
                <div class="tour_container toptourbox">
                    <div class="img_container toptourbox">
                        <a href="<?= $tour->route ? Yii::$app->language.'/'.$tour->route->url : Yii::$app->language.'/tours/details/'.$tour->id; ?>">
                            <h1 class="toptourname"><?= $tour->title; ?></h1>
                            <h3 class="toptourdesc"><?= $tour->caption; ?></h3>
                            <?php echo LazyLoad::widget(['src' => '/uploads/tours/'.$tour->cover,'options' => ['class' =>  'img-responsive']]) ?>

                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
