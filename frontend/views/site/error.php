<main>
    <section class="text-center"
             style="margin-top: 15%;margin-bottom: 15%;">
        <p style="font-size: 80px">404</p>
        <h1 style=" font-size: 50px"><?= Yii::t('app', 'Page Not Found') ?></h1>
        <a class="btn btn-danger" href="<?= yii::$app->homeUrl ?>"><?= Yii::t('app', 'Go Home') ?></a>
    </section>
</main>
