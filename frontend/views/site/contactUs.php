<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$dais = \frontend\components\Helper::getWorkingDais();
$info = \frontend\components\Helper::getInfo();

?>
<main>
    <div id="map" class="contact_2"></div>

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <div class="form_title">
                    <h3><strong><i class="icon-pencil"></i></strong>Fill the form below</h3>
                </div>
                <div class="step">

                    <div id="message-contact"></div>
                    <?php $form = ActiveForm::begin(['id' => 'contactform', 'action' => '/' . Yii::$app->language . "/site/contact"]); ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label><?= Yii::t('app', 'First Name') ?></label>
                                <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Enter Name')])->label(false); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label><?= Yii::t('app', 'Last Name') ?></label>
                                <?= $form->field($model, 'last_name')->textInput(['placeholder' => Yii::t('app', 'Last Name')])->label(false); ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label><?= Yii::t('app', 'Email') ?></label>
                                <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Enter Email')])->label(false); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label><?= Yii::t('app', 'Phone') ?></label>
                                <?= $form->field($model, 'phone')->textInput(['placeholder' => Yii::t('app', 'Enter Phone Number')])->label(false); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?= Yii::t('app', 'Message') ?></label>
                                <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => Yii::t('app', 'Write Your Message...')])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn_1', 'name' => 'contact-button']) ?>
                            </div>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
            <!-- End col-md-8 -->

            <div class="col-md-4 col-sm-5">
                <div class="box_style_1">
                    <span class="tape"></span>
                    <h4><?= Yii::t('app', 'Address') ?> <span><i class="icon-pin pull-right"></i></span></h4>
                    <p>
                        <?= $info['Address'] ?>
                    </p>
                    <hr>
                    <h4><?= Yii::t('app', 'Help center') ?> <span><i class="icon-help pull-right"></i></span></h4>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-57"></i>
                        <h4><?= Yii::t('app', 'Need Help?') ?></h4>
                        <a href="tel://<?= $info['Email'] ?>"
                           class="phone phone_all_tours"> <?= $info['Email'] ?></a>
                        <a href="tel://<?= $info['Phone'] ?>"
                           class="email"> <?= $info['Phone'] ?></a><br>

                        <small><?= Yii::t('app', 'We are Available from'); ?> <?= $dais->value ?></small>
                    </div>
                </div>
                <!-- End col-md-4 -->
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
</main>
<!-- End main -->
<script>
    function initMap() {
        var options = {
            zoom: 15,
            center: {lat: 40.177200, lng: 44.503490}
        };
        var map = new google.maps.Map(document.getElementById('map'), options);
        var marker = new google.maps.Marker({
            position: {lat: 40.180155, lng: 44.515218},
            map: map
        });
    }
</script>
<?php $this->registerJsFile(
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyB4qW1Bs0GS8BpufPkYby0x2Em5UbL8CUM&callback=initMap", ['depends' => \yii\web\JqueryAsset::className()]
)
?>

<?php $this->registerJsFile(
    "/js/infobox.js", ['depends' => \yii\web\JqueryAsset::className()]
)
?>



