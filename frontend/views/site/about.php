<?php

/* @var $this yii\web\View */

use yii\helpers\Html;


$this->params['breadcrumbs'][] = $this->title;
?>

<section class="parallax-window" data-parallax="scroll" data-image-src="/uploads/media/<?= is_object($aboutBanner) ? $aboutBanner->value:''; ?>"
         data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <h1>About Us</h1>
            <p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
        </div>
    </div>
</section>
<!-- End Section -->

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container about">
        <?= $about->content ?>
    </div>

</div>
