<?php

use toriphes\lazyload\LazyLoad;
use yii\web\View;

?>

<div id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($slider as $key => $slid ) : ?>
            <li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : "" ?>"></li>
        <?php endforeach; ?>

    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <?php foreach ($slider as $key => $slid ) : ?>
            <div class="item <?= $key == 0 ? 'active' : "" ?>">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" data-src="/uploads/media/<?=$slid->uniq_name?>"></div>
                <?php echo LazyLoad::widget(['src' => "/uploads/media/".$slid->uniq_name,'options' => ['class' =>  'img-responsive']]);?>

                <div class="carousel-caption">
                    <h2 class="slider_text"><?= $slid->title ?></h2>
                    <img src="/img/untitled-1.png" alt="" class="slider_line" >
                </div>
            </div>
        <?php endforeach; ?>
    </div>


    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>

</div>

<?php $this->registerJs("
   $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
", View::POS_READY) ?>
<?php $this->registerCssFile("/css/bootstrap.min.css") ?>
<?php $this->registerCssFile("/css/full-slider.css") ?>
<?php $this->registerJsFile('@web/js/jquery.lazy.min.js',['depends' => 'yii\web\YiiAsset']) ?>
<?php
$js = <<<JS
        $(function() {
        $('.fill').lazy();
    });
JS;
$this->registerJs($js);
//?>

