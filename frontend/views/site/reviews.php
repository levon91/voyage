<?php
/**
 * Created by PhpStorm.
 * User: Levon
 * Date: 19/02/2018
 * Time: 13:53
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$model = new \common\models\Reviews();
?>
<?php $this->registerCssFile('/css/owl.theme.default.min.css') ?>
<?php $this->registerCssFile('/css/owl.carousel.css') ?>
<?php $this->registerCssFile('/css/reviews.css') ?>
    <div class="container">
        <div class="container-fluid">
            <div class="section-reviews">
                <p class="reviews"><?= is_object($review) ? $review->value : '' ?></p>
                <?php if (!empty($reviews)): ?>
                <div class="owl-carousel owl-one">
                    <?php foreach ($reviews as $review): ?>
                        <div class="item">
                            <h4 class="name"><?= $review->client_name; ?></h4>
                            <p class="review_text"><?= $review->message ?></p>
                            <button type="button" class="my-modal btn-modal" data-toggle="modal" data-target="#myModal">read more</button>
                            <div><?= $review->created_date; ?></div>
                        </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <div class="owl-one" style="display: none;"></div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>

    <div class="">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="content"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="col-md-12">
            <p class="text-center add_bottom_30">
                <a href="#" class="btn_1" data-toggle="modal"
                   data-target="#myReview"><?= Yii::t('app', 'Leave a review'); ?></a>
            </p>
        </div>
    </div>
<?php if (Yii::$app->session->hasFlash('message')): ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="flash">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('message'); ?>
    </div>
<?php endif; ?>
    <!-- Modal Review -->
    <div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myReviewLabel"><?= Yii::t('app', 'Write your review') ?></h4>
                </div>
                <div class="modal-body">
                    <div id="message-review">
                    </div>
                    <?php $form = ActiveForm::begin(['action' => ['/tours/leave-review'],]); ?>
                    <?= $form->field($model, 'tour_id')->hiddenInput(['value' => ($tour)])->label(false);
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= $form->field($model, 'client_name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Full Name')]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Your email')]) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="form-group">

                        <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', 'Write your review')]) ?>
                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn_1', 'id' => 'submit-review']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->registerJsFile('/js/owl.carousel.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php $this->registerJs("
$('.owl-one').owlCarousel({
        margin:10,
        nav:true,
        loop: true,
        rewind: true,
        responsive:{
            0:{
                items:1,
                loop:( $('.item').length > 1 )
            },
            551:{
                items:2,
                loop:( $('.item').length > 2 )
            },
             767:{
                items:3,
                 loop:( $('.item').length > 3 )
            },
             1200:{
                items:4,
                loop:( $('.item').length > 4 )
            }
        }
    });", \yii\web\View::POS_READY);
?>
<?php $this->registerJs("$('.my-modal').on ('click',function(){
        var message=$(this).prev().text();
        var name=$(this).parent().children().closest('h4').text();
        $('.title').text(name);
        $('.content').text(message);      
    });", \yii\web\View::POS_READY);
?>