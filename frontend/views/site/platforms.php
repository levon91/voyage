<?php $this->registerCssFile('/css/owl.theme.default.min.css') ?>
<?php $this->registerCssFile('/css/owl.carousel.css') ?>
<?php $this->registerCssFile('/css/platforms.css') ?>

<?php if (!empty($platforms)):?>
<div class="container">
    <div class="container-fluid platforms-carousel ">
        <div class="carusel">
            <p class="platforms"><?= is_object($ourAgency) ?  $ourAgency -> value : '' ?></p>
            <div class="owl-carousel owl-two">
                <?php foreach ($platforms as $k => $platform ) : ?>
                    <div class="item">
                        <a href="<?= $platform->link;?>" target="_blank">
                            <img src="/uploads/platforms/<?=$platform->logo?>">
                            <p><?= $platform->description_en; ?></p>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
    <div class="carusel" style="display: none;"></div>
<?php endif?>

<?php $this->registerJsFile('/js/owl.carousel.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php $this->registerJs("
     $('.owl-two').owlCarousel({
        margin:10,
        nav:true,
        loop: true,
        rewind: true,
        responsive:{
            0:{
                items:1,
                 loop:( $('.item').length > 1 )
            },
           551:{
                items:3,
                 loop:( $('.item').length > 3 )
            },
             767:{
                items:4,
                 loop:( $('.item').length > 4 )
            },
             992:{
                items:5,
                 loop:( $('.item').length > 5 )
            },
            1200:{
                items:6,
                loop:( $('.item').length > 6 )
            }
        }
});", \yii\web\View::POS_READY);
?>