<section>
    <div class="container-fluid funy_banner">
        <div class="row">
            <?php foreach ($banners as $banner): ?>
                <div class="col-md-3 col-xs-6 col-sm-3">
                    <span class="counter" data-count="<?= $banner->number; ?>"></span><?= $banner->type; ?>
                    <p><?= $banner->description; ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

</section>
<?php $this->registerJs(
    "$('.counter').each(function() {
  var _this = $(this),
      countTo = _this.attr('data-count');
  
  $({ countNum: _this.text()}).animate({
    countNum: countTo
  },

  {
    duration: 8000,
    easing:'linear',
    step: function() {
      _this.text(Math.floor(this.countNum));
    },
    complete: function() {
      _this.text(this.countNum);
      //alert('finished');
    }

  });  
});"
);