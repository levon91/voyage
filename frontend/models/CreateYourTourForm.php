<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CreateYourTourForm extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $body;
    public $verifyCode;
    public $phoneNumber;
    public $tour_category;
    public $food;
    public $transportation;
    public $persons;
    public $destination;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['first_name', 'email', 'last_name', 'phoneNumber', 'tour_category', 'food', 'transportation', 'persons'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['persons', 'integer', 'max' => 10, 'min' => 1],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            [['phoneNumber'], 'integer'],
            [['tour_category', 'food','transportation','persons','destination', 'body'], 'string'],
        ];
    }

    public function attributeLabels()
    {


        return [
            'verifyCode' => 'Verification Code',
            'first_name' => Yii::t('app', 'First Name'),
            'list_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'body' => Yii::t('app', 'Comments'),
            'phoneNumber' => Yii::t('app', 'Phone Number'),
            'tour_category' => Yii::t('app', 'Tour Category'),
            'food' => Yii::t('app', 'Meals'),
            'transportation' => Yii::t('app', 'Transportation'),
            'persons' => Yii::t('app', 'Number of participants'),
            'destination' => Yii::t('app', 'Destination'),
        ];
    }

    public function sendEmail()
    {
        $mess = 'First Name: ' . $this->first_name .
            '<br /> Last Name: ' . $this->last_name .
            '<br /> Tour Category: ' . $this->tour_category .
            '<br /> Phone: ' . $this->phoneNumber .
            '<br /> Persons: ' . $this->persons .
            '<br /> Transportation: ' . $this->transportation.
            '<br /> Food: ' . $this->food .
            '<br /> Destination: ' . $this->destination . '<br /> ' .
            $this->body;
        $mail = Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom($this->email)
            ->setSubject('Make your own tour')
            ->setHtmlBody($mess)
            ->send();

    }
}