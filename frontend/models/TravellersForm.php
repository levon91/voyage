<?php

namespace frontend\models;

use Faker\Provider\DateTime;
use Yii;
use yii\base\Model;

/**
 * This is the model class for table "categories".
 *
 * @property int $participants
 *
 * @property Posts[] $posts
 */
class TravellersForm extends Model
{

    public $trial;
    public $participants;
    public $flexible_dates;
    public $travel_dates;
    public $occasion;
    public $adults;
    public $teenagers;
    public $kids;
    public $babies;
    public $private_group;
    public $date_approximate_start;
    public $date_approximate_length;
    public $age_groups;
    public $approximate_dates;

    public $errorMessage = 'Please provide a response to this question.';
    public $participants_group = ['family', 'friends', 'private_group'];
    public $private_group_types = ['club_association', 'company', 'conference_meeting', 'other'];

    public function rules()
    {
        return [
            [['participants', 'flexible_dates', 'travel_dates', 'occasion', 'adults', 'teenagers', 'kids', 'babies',
                'private_group', 'date_approximate_start', 'date_approximate_length', 'age_groups', 'approximate_dates'], 'safe'],
            ['occasion', 'validateOccasion', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['participants', 'flexible_dates'], 'required', 'message' => $this->errorMessage],
            ['date_approximate_start', 'validateDateApproximateStart', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['date_approximate_length', 'validateApproximateDateLength', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['adults', 'validteAdults', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['teenagers', 'validateAge', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['travel_dates', 'required', 'when' => function ($model) {
                return $model->flexible_dates == 'yes';
            }],
            ['private_group', 'validatePrivateGroup', 'skipOnEmpty' => false, 'skipOnError' => false]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form';
    }

    public function validateAge()
    {
        if (in_array($this->participants, $this->participants_group)) {
            if ($this->teenagers < 0 || $this->kids < 0 || $this->babies < 0) {
                $this->addError('age_groups', $this->errorMessage);
            }
        }
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'participants' => Yii::t('app', 'participants')
        ];

    }

    public function validteAdults()
    {
        if (in_array($this->participants, $this->participants_group)) {
            if ($this->adults == '' || $this->adults <= '0') {
                $this->addError('age_groups', $this->errorMessage);
            }
        }
    }

    public function validateDateApproximateStart()
    {
        if ($this->flexible_dates == 'no') {
            $a = $this->date_approximate_start;
            if ($a != '-1') {
                $b = date('Y-m-d', strtotime($a));
                if ($a != $b) {
                    $this->addError('approximate_dates', $this->errorMessage);
                }
            }
        }
    }

    public function validateApproximateDateLength()
    {
        if ($this->flexible_dates == 'no') {
            $dates = ['-1', 1, 7, 14, 21, 22];
            if ($this->date_approximate_length = '' || !in_array($this->date_approximate_length, $dates)) {
                $this->addError('approximate_dates', $this->errorMessage);
            }
        }
    }

    public function validateOccasion($attribute, $params)
    {
        if ($this->participants == 'couple') {
            $occasion = ['holidays', 'honneymoon', 'other'];
            if ($this->occasion == '' || !in_array($this->occasion, $occasion)) {
                $this->addError($attribute, $this->errorMessage);
            }
        }
    }

    public function validateDateRange($attribute)
    {
        if ($this->travel_dates) {
            $pattern = "/[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/";
            $dateRange = $this->travel_dates;
            if (preg_match_all($pattern, $dateRange, $matches)) {
                if ($matches) {
                    if (count($matches[0]) != 2) {
                        $this->addError($attribute, $this->errorMessage);
                    }
                } else {
                    $this->addError($attribute, $this->errorMessage);
                }
            } else {
                $this->addError($attribute, $this->errorMessage);
            }
        } else {
            $this->addError($attribute, $this->errorMessage);
        }
    }



    public function validatePrivateGroup($attribute)
    {
        if ($this->participants == 'private_group') {
            if (!in_array($this->private_group, $this->private_group_types)) {
                $this->addError($attribute, $this->errorMessage);
            }
        }
    }
}

?>