<?php

namespace frontend\models;

use common\models\Countries;
use Faker\Provider\DateTime;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class TravellersFormPersonalData extends Model{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $birth_day;
    public $birth_month;
    public $birth_year;
    public $residence_country;
    public $accept;

    public $error = 'Please provide response to this question';

    public function rules(){
        return [
            [['first_name', 'last_name', 'email', 'phone', 'residence_country', 'accept', 'birth_day',
                'birth_month', 'birth_year'], 'required', 'message' => $this->error],
            ['email', 'email'],
            ['accept', 'compare', 'compareValue' => 1, 'operator' => '==', 'message' => $this->error],
            [['birth_day'], 'number', 'min' => 1, 'max' => 31],
            [['birth_month'], 'number', 'min' => 1, 'max' => 12],
            [['birth_year'], 'number', 'min' => 1870, 'max' => date('Y')],

        ];
    }

    public function getCountryList(){
        return ArrayHelper::map(Countries::find()->all(), 'country_name', 'country_name');
    }


}
?>