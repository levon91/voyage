<?php

namespace frontend\models;

use Faker\Provider\DateTime;
use Yii;
use yii\base\Model;


class TravellersFormDetails extends Model
{
    public $accommodation;
    public $additional;
    public $accompaniment;
    public $budget;
    public $plan;
    public $accept;

    public $accomodationRange = ['authentic', 'simple', 'comfortable', 'luxury'];
    public $accompanimentRange = ['driver', 'guide', 'none', 'join'];
    public $planRange = ['info', 'ready', 'book'];
    public $error = 'Please provide response to thi squestion';

    public function rules()
    {
        return [
            [['accommodation', 'accompaniment', 'plan'], 'required', 'message' => $this->error],
            ['accommodation', 'validateAccommodation', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['accompaniment', 'in', 'range' =>  $this->accompanimentRange, 'message' => $this->error],
            ['plan', 'in', 'range' => $this->planRange, 'message' => $this->error],
            [['budget'], 'double', 'message' => $this->error],
        ];
    }

    public function getAccompaniment()
    {
        return ['driver' => 'Driver & guide', 'guide' => 'Guide', 'none' => 'None', 'join' => 'Join a small group of travellers'];

    }

    public function  getAccommodation(){
        return ['authentic' => 'Authentic (homestay/guesthouse)', 'simple' => 'Simple (equivalent of 2* hotels)', 'comfortable' => 'Comfortable (equivalent of 3* hotels)', 'luxury' => 'Luxury (equivalent of 4* and 5* hotels)'];

    }
    public function getPlan()
    {
        return ['info' => Yii::t('app', 'I need more information to choose my destination'), 'ready' => Yii::t('app', 'I’ve chosen my destination and I\'m ready to start trip-planning'), 'book' => Yii::t('app', 'I\'ve chosen my destination and I\'m ready to book my trip')];

    }

    public function validateAccommodation($attribute){
        if(count($this->accommodation)){
            foreach($this->accommodation as $value){
                if(!in_array($value, $this->accomodationRange)){
                    $this->addError($attribute, $this->error);
                }
            }
        }
    }

//    public function validateAccept($attribute){
//        if(count($this->accept)){
//            if($this->accept[0] != )
//        }
//    }
}

?>