$(document).ready(function(){

    $('.main-form input[type="radio"]').prev('label').css({
        'display': 'flex',
        'align-items': 'center',
        'margin': '10px 0',
        'position': 'relative',
        'padding-left': '25px'
    });

    $(document).on('submit','#travellers-form', function(e){
        e.preventDefault();

        var drDownBox = $('.dropdowns-block').css('display');

        var form = $(this).serialize();

        $.ajax({
            url: '/en/page/travellers-handle-request',
            type: 'post',
            dataType: 'json',
            data: form,
            success: function(response){
                $(document).find('.main-form').html(response);
                var a = $('input[type="radio"]:checked').parent().addClass('checked-radio');
                console.log(a);
            },
            error: function(e,message,xhr){
                console.log(e);
            }
        });
    });

    $(document).on('submit', '#details-form', function(e){
        e.preventDefault();

        var form = $(this).serialize();
        $.ajax({
            url: '/en/page/travellers-details-handle-request',
            type: 'post',
            // dataType: 'json',
            data: form,
            success: function(response){
                $(document).find('.main-form').html(response);
            },
            error: function(e,message,xhr){
                console.log(e);
            }
        });
    });

    $(document).on('submit', '#personal-data-form', function(e){
        e.preventDefault();

        var form = $(this).serialize();
        $.ajax({
            url: '/en/page/travellers-personal-data-handle-request',
            type: 'post',
            data: form,
            success: function(response){
                $(document).find('.main-form').html(response);
                console.log(response);
            },
            error: function(e, xhr, message){
                console.log(e);
            }
        });
    });


    $(document).on('change', '#travellers-form input', function(){
        var inputValue = $(this).val();

        // var a = $('input[type="radio"]:checked').val();
        // console.log(a);

        if(inputValue == 'couple'){
            $('.participants-subfield').addClass('hidden');
            $('.field-travellersform-occasion').removeClass('hidden');
        }else if(inputValue == 'family' || inputValue == 'friends'){
            $('.participants-subfield').addClass('hidden');
            $('.age-groups').removeClass('hidden');
        }else if(inputValue == 'private_group'){
            $('.participants-subfield').addClass('hidden');
            $('.field-travellersform-private_group').removeClass('hidden');
            $('.age-groups').removeClass('hidden');
        }

        if(inputValue == 'yes'){
            $('.dates-group').addClass('hidden');
            $('.field-travellersform-travel_dates').removeClass('hidden');
        }else if(inputValue == 'no'){
            $('.dates-group').addClass('hidden');
            $('.dropdowns-block').removeClass('hidden');
        }
    });


    $(document).on('click', '.radio-label label', function(e){
        $(this).closest('.radio-label').find('label').removeClass('checked-radio');
        $(this).addClass('checked-radio');
    });



    $(document).on('click', '.qty-btn', function(){
        var value = $(this).parent().siblings('input[type="number"]').val();

        if(!value){
            value = 0;
        }else{
            value = parseInt(value);
        }
        var min = parseInt($(this).parent().siblings('input[type="number"]').attr('min'));

        if($(this).hasClass('btn-decrement')){
            value = value - 1;
            if(value  < min){
                value = min;
            }
            $(this).parent().siblings('input[type="number"]').val(value);
        }else if($(this).hasClass('btn-increment')){
            value++;
            $(this).parent().siblings('input[type="number"]').val(value);
        }
    });
});