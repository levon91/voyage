$(function () {
    $("#addClass").click(function () {
        $('#qnimate').addClass('popup-box-on');
    });

    $("#removeClass").click(function () {
        $('#qnimate').removeClass('popup-box-on');
    });


    var id = window.setInterval(function (e) {
        sendRequest(true);

    }, 4000);

    $('#status_message').on('focus', function (e) {
        window.clearInterval(id);
    })

    $('#status_message').blur(function (e) {
        id = window.setInterval(function (e) {
            sendRequest(true);

        }, 4000);
    })


    $('#status_message').keypress(function (e) {

        if (e.which == 13 && $(this).val().trim().length > 0) {
            $(this).blur();
            sendRequest(false);
            var html = '<div class="direct-chat-msg doted-border">'
                + '<i class="pe-7s-user" style="font-size:35px;background: white;border-radius: 34px;"></i>'
                + '<div class="direct-chat-text">' + $(this).val() + '</div>'
                + '</div>'
                + '<div class="direct-chat-info clearfix">'
                + '<span class="direct-chat-timestamp pull-right">3.36 PM</span>'
                + '</div>'
            $(this).val('');

            $('.direct-chat-messages').append(html)
        }
    });
    function sendRequest(get) {
        $.ajax({
            url: "/en/site/send-message/",
            method: 'post',
            data: {'message': $('#status_message').val()},
            success: function (message) {
                if (get && message != '') {
                    var html = '<div class="direct-chat-msg doted-border">'
                        + '<i class="icon_set_1_icon-57" style="font-size:25px;background: white;border-radius: 34px;"></i>'
                        + '<div class="direct-chat-text">' +message + '</div>'
                        + '</div>'
                        + '<div class="direct-chat-info clearfix">'
                        + '<span class="direct-chat-timestamp pull-right"></span>'
                        + '</div>';
                    $('.direct-chat-messages').append(html)

                }
            }
        })
    }
})