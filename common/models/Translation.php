<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property string $lang
 * @property string $text_am
 * @property string $text_en
 * @property string $text_ru
 * @property string $text_es
 * @property string $text_it
 * @property string $text_fr
 * @property string $text_de
 * @property string $type
 * @property int $type_id
 */
class Translation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_en'], 'safe'],
            [['text_en','text_ru','text_am','text_es','text_it','text_de','text_fr'], 'string'],
            [['type_id'], 'integer'],
            [['lang', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang' => Yii::t('app', 'Lang'),
            'text' => Yii::t('app', 'Text'),
            'type' => Yii::t('app', 'Type'),
            'type_id' => Yii::t('app', 'Type ID'),
        ];
    }
    
    public static function saveFromParts($data = [],$model,$type){
        foreach ($data as $key => $value){
            
            if (is_array($value)) {
                $trans = Translation::find()->where(['type_id' => $model->id])->andWhere(['type' => $type.'-'.$key])->one();
                $translate = new Translation();
                if ($trans) {
                    $translate = $trans;
                }
                $translate->lang = 'en';
                $translate->type = $type.'-'.$key;
                $translate->type_id = $model->id;
                $translate->text_en = $model->$key['en'];
                //$translate->text_am = $this->title['am'];
                $translate->text_ru = $model->$key['ru'];
                $translate->text_es = $model->$key['es'];
                $translate->text_it = $model->$key['it'];
                $translate->text_fr = $model->$key['fr'];
                $translate->text_de = $model->$key['de'];
                $translate->save();
            }
        }
    }
}
