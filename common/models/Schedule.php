<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property string $time_from
 * @property string $time_to
 * @property integer $tour_id
 * @property string $week_day
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time_from', 'time_to', 'tour_id'], 'safe'],
            [['time_from', 'time_to', 'week_day'], 'string', 'max' => 255],
        ];
    }

    public function getTourId()
    {
        return $this->tour_id;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'time_from' => Yii::t('app', 'Time From'),
            'time_to' => Yii::t('app', 'Time To'),
            'tour_id' => Yii::t('app', 'Tour'),
            'week_day' => Yii::t('app', 'Week Day'),
        ];
    }
    public function getDaysList()
    {
        return [
            'Monday' => Yii::t('app','Monday'),
            'Tuesday' => Yii::t('app','Tuesday'),
            'Wednesday' => Yii::t('app','Wednesday'),
            'Thursday' => Yii::t('app','Thursday'),
            'Friday' => Yii::t('app','Friday'),
            'Saturday' => Yii::t('app','Saturday'),
            'Sunday' => Yii::t('app','Sunday'),
        ];
    }

}
