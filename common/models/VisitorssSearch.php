<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Visitors;

/**
 * VisitorssSearch represents the model behind the search form of `common\models\Visitors`.
 */
class VisitorssSearch extends Visitors
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ip', 'link_from', 'link', 'country', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$filterDate = null)
    {
        if (!$filterDate) {
            $query = Visitors::find()->orderBy(['id' => SORT_DESC])->groupBy('link');
        }else{
            $query = Visitors::find()
                ->orderBy(['id' => SORT_DESC])
                ->groupBy('link')
                ->where(['created_at' => $filterDate]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'link_from', $this->link_from])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'country', $this->country]);

        return $dataProvider;
    }
}
