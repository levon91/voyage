<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "visitors".
 *
 * @property int $id
 * @property string $ip
 * @property string $link_from
 * @property string $link
 * @property string $country
 * @property string $created_at
 */
class Visitors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['ip', 'link_from', 'link', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'link_from' => 'Link From',
            'link' => 'Link',
            'country' => 'Country',
            'created_at' => 'Created At',
        ];
    }
}
