<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "platforms".
 *
 * @property int $id
 * @property string $logo
 * @property string $link
 * @property string $description
 * @property int $order
 * @property string $created_at
 * @property string $updated_at
 */
class Platforms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public static function tableName()
    {
        return 'platforms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'order'], 'required'],
            [['order'], 'integer'],
            [['logo', 'link'], 'string', 'max' => 255],
            [['description_en','description_de','description_fr','description_es','description_it','description_ru'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Logo',
            'link' => 'Link',
            'order' => 'Order',
            'description_en' => 'Description',
            'description_de' => 'Description',
            'description_fr' => 'Description',
            'description_es' => 'Description',
            'description_it' => 'Description',
            'description_ru' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
