<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property int $id
 * @property int $session_id
 * @property string $message
 * @property int $answered
 * @property int $is_new
 * @property int $is_admin
 * @property string $created_at
 */
class Chat extends \yii\db\ActiveRecord
{
    public $cnt;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['session_id'], 'required'],
            [['session_id'], 'integer'],
            [['message'], 'string'],
            ['message', 'filter', 'filter' => function ($value) {
                return \yii\helpers\HtmlPurifier::process($value);
            }],
            [['created_at', 'cnt'], 'safe'],
            [['answered', 'is_new', 'is_admin'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'message' => Yii::t('app', 'Message'),
            'answered' => Yii::t('app', 'Answered'),
            'is_new' => Yii::t('app', 'Is New'),
            'is_admin' => Yii::t('app', 'Is Admin'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
