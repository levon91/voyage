<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tours;
use common\models\Categories;

/**
 * ToursSearch represents the model behind the search form of `common\models\Tours`.
 */
class ToursSearch extends Tours
{
    public $cnt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'days', 'age_min', 'destination_id', 'sort'], 'integer'],
            [['title', 'caption', 'content', 'type', 'cover', 'banner', 'attachments', 'coordinates', 'created_at', 'updated_at'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tours::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'days' => $this->days,
            'age_min' => $this->age_min,
            'destination_id' => $this->destination_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);


        return $dataProvider;
    }

    public static function getTours($limit = false, $id = 0, $in = false, $order = false)
    {
        if ($in) {
            $tour = ToursSearch::find()->where(['category_id' => $id]);
        } else {
            $tour = ToursSearch::find()->where(['!=', 'category_id', $id]);
        }
        if ($order && in_array($order, ['asc', 'desc'])) {
            return $tour->orderBy('price ' . $order)->all();
        }
        if ($limit) {
            return $tour->limit($limit)->all();
        } else {
            return $tour->all();
        }
    }

    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])  ->andOnCondition(['for_tour' => true]);;
    }

    public static function getLandingPageTour($category)
    {
        return ToursSearch::find()->select(['id','price','cover'])->where(['<>','category_id', $category])->andWhere(['in_landing_page' => 1])->orderBy('sort')->limit(6)->all();
    }

    public static function getAllCategories()
    {
        return ToursSearch::find()->select('count(*) as cnt, id, category_id')->with('categories')->groupBy('category_id')->all();
    }
}
