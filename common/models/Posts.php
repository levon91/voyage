<?php

namespace common\models;

use Codeception\Step\Meta;
use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $caption
 * @property int $category_id
 * @property string $type
 * @property string $cover
 * @property string $banner
 * @property string $attachments
 * @property string $coordinates
 * @property bool $have_token
 * @property string $token
 * @property string $key
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 * @property Categories $category
 * @property Categories $url
 */
class Posts extends \yii\db\ActiveRecord
{
    public $title;
    public $caption;
    public $content;
    public $have_token;
    public $meta;
    public $url;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    Yii::$app->formatter->defaultTimeZone = 'Asia/Yerevan';
                    return Yii::$app->formatter->asDate('now', 'yyyy-MM-dd H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required', 'when' => function ($model) {
                return $model->title['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#posts-title-en').val() == '';
            }"],
            ['caption', 'required', 'when' => function ($model) {
                return $model->caption['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#posts-caption-en').val() == '';
            }"],
            ['content', 'required', 'when' => function ($model) {
                return $model->content['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#posts-content-en').val() == '';
            }"],

            [['category_id'], 'integer'],
            [['cover'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['banner'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['created_at', 'updated_at', 'title', 'content', 'caption', 'have_token', 'meta','url'], 'safe'],
             [['url'], 'validateUrl'],
            [['type', 'coordinates'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }


 public function validateUrl()
    {
        $route = Routes::find()->where(['url' => $this->url])->andWhere(['<>','type', 'post_'.$this->id])->one();
                if($route){
                    $this->addError('url',Yii::t('app','url already in use'));
                    return false;
                }
      }
      public function beforeDelete()
{
   
    
    if($this->route)
    {
        $this->route->delete();
    }

    return parent::beforeDelete();
}
      
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'caption' => Yii::t('app', 'Caption'),
            'category_id' => Yii::t('app', 'Category'),
            'type' => Yii::t('app', 'Type'),
            'cover' => Yii::t('app', 'Cover'),
            'banner' => Yii::t('app', 'Banner'),
            'attachments' => Yii::t('app', 'Attachments'),
            'coordinates' => Yii::t('app', 'Coordinates'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return Routes::find()->where(['type' => 'post_'.$this->id])->one();
    }


    public function afterSave($insert, $changedAttributes)
    {
         if($this->url){
           $route = Routes::find()->where(['type' => 'post_'.$this->id])->one();
                if(!$route){
                    $route = new Routes();
                }
               $route->url = $this->url;
                    $route->action = '/posts';
                    $route->type = 'post_'.$this->id;
                    $route->save();
         }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        Translation::saveFromParts(['title' => $this->title, 'caption' => $this->caption, 'content' => $this->content], $this, 'post');
        Metas::saveMeta($this->meta, 'post', $this->id);
    }
    public function getMeta()
    {
        return Metas::find()->where(['type' => 'post'])->andWhere(['type_id' => $this->id])->with('translationValue')->asArray()->all();
    }
    
    public function getTitle()
    {
        return ArrayHelper::getValue($this, 'translationTitle.title_en');
    }


    public function getTranslationTitle()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'post-title']);
    }

    public function getTranslationCaption()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'post-caption']);
    }

    public function getTranslationContent()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'post-content']);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->title = ArrayHelper::getValue($this, 'translationTitle.text_' . Yii::$app->language);
        $this->content = ArrayHelper::getValue($this, 'translationContent.text_' . Yii::$app->language);
        $this->caption = ArrayHelper::getValue($this, 'translationCaption.text_' . Yii::$app->language);

    }


    public function upload()
    {
        $time = time();
        if ($this->validate()) {
            $updated = false;
            if (is_object($this->cover)) {
                $updated = true;
                $this->cover->saveAs(Yii::getAlias('@frontend') . '/web/uploads/posts/' . $time . $this->cover->baseName . '.' . $this->cover->extension);
                $this->cover = $time . $this->cover->baseName . '.' . $this->cover->extension;
            }
            if (is_object($this->banner)) {
                $updated = true;
                $this->banner->saveAs(Yii::getAlias('@frontend') . '/web/uploads/posts/' . $time . $this->banner->baseName . '.' . $this->banner->extension);
                $this->banner = $time . $this->banner->baseName . '.' . $this->banner->extension;
            }
            
                $this->save(false);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function getByCategory($id,$byId=false)
    {
       
        if($byId){
            $query = Posts::find()->where(['category_id'=> $id]);
        }else{
             $query = Posts::find()->where(['like','category_id', '"'.$id.'"']);
        }
        
       

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}
