<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chat_sessions".
 *
 * @property int $id
 * @property string $creator_ip
 * @property string $created_at
 * @property string $updated_at
 */
class ChatSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat_sessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creator_ip', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'creator_ip' => Yii::t('app', 'Creator Ip'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
