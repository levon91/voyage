<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key
 * @property string $banner
 * @property string $value_en
 * @property string $value_ru
 * @property string $value_de
 * @property string $value_fr
 * @property string $value_it
 * @property string $value_es
 * @property string $description
 * @property string $title
 */
class Settings extends \yii\db\ActiveRecord
{

    public $title;
    public $description;
    public $value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [

            [['key', 'value_en','value_ru','value_fr','value_it','value_es','value_de'], 'string', 'max' => 255],
            [['description', 'title'], 'safe'],
            [['banner'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg,JPEG'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'value_en' => Yii::t('app', 'Value'),
            'value_ru' => Yii::t('app', 'Value'),
            'value_de' => Yii::t('app', 'Value'),
            'value_fr' => Yii::t('app', 'Value'),
            'value_it' => Yii::t('app', 'Value'),
            'value_es' => Yii::t('app', 'Value'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'banner' => Yii::t('app', 'Banner'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Translation::saveFromParts(['title' => $this->title, 'description' => $this->description], $this, 'video');
    }

    public function upload()
    {
        $time = time();
        if ($this->validate() && $this->save()) {
            if ($this->banner && !is_string($this->banner)) {
                $this->banner->saveAs(Yii::getAlias('@frontend') . '/web/uploads/video/' . $time . $this->banner->baseName . '.' . $this->banner->extension);
                $this->banner = $time . $this->banner->baseName . '.' . $this->banner->extension;
            }

            $this->save();
            return true;
        } else {
            return false;
        }
    }

    public function getTranslationTitle()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'video-title']);
    }

    public function getTranslationCaption()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'video-description']);
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->title = ArrayHelper::getValue($this, 'translationTitle.text_' . Yii::$app->language);
        $this->description = ArrayHelper::getValue($this, 'translationCaption.text_' . Yii::$app->language);
        $this->value = $this->{'value_'.Yii::$app->language} == '' ? $this->value_en : $this->{'value_'.Yii::$app->language};
    }

}
