<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $cover
 * @property string $for_tour
 * @property string $created_at
 * @property string $updated_at
 *@property string $for_gallery
 * @property Posts[] $posts
 */
class Categories extends \yii\db\ActiveRecord
{

    public $title;
    public $description;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required', 'when' => function ($model) {
                return $model->title['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#categories-title-en').val() == '';
            }"],
            ['description', 'required', 'when' => function ($model) {
                return $model->description['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#categories-description-en').val() == '';
            }"],
            // [['desc_am', 'desc_en', 'desc_ru'], 'string'],
            [['cover'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['created_at', 'updated_at', 'description', 'title', 'for_tour','for_gallery'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            /* 'title_en' => Yii::t('app', 'Title En'),
             'title_ru' => Yii::t('app', 'Title Ru'),
             'desc_am' => Yii::t('app', 'Desc Am'),
             'desc_en' => Yii::t('app', 'Desc En'),*/
            'description' => Yii::t('app', 'Description'),
            'for_tour' => Yii::t('app', 'For Tour'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'for_gallery' => Yii::t('app', 'For Gallery'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['category_id' => 'id']);
    }

    public function upload()
    {
        $time = time();
        if ($this->validate() && $this->save()) {
            if ($this->cover && !is_string($this->cover)) {
                $this->cover->saveAs(Yii::getAlias('@frontend') . '/web/uploads/categories/' . $time . $this->cover->baseName . '.' . $this->cover->extension);
                $this->cover = $time . $this->cover->baseName . '.' . $this->cover->extension;
                $this->save(false);
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (is_array($this->title)) {
            $trans = Translation::find()->where(['type_id' => $this->id])->andWhere(['type' => 'categories-title'])->one();
            $translate = new Translation();
            if ($trans) {
                $translate = $trans;
            }
            $translate->lang = 'en';
            $translate->type = 'categories-title';
            $translate->type_id = $this->id;
            $translate->text_en = $this->title['en'];
            //$translate->text_am = $this->title['am'];
            $translate->text_ru = $this->title['ru'];
            $translate->text_es = $this->title['es'];
            $translate->text_it = $this->title['it'];
            $translate->text_fr = $this->title['fr'];
            $translate->text_de = $this->title['de'];
            if (!$translate->save()) {
                var_dump($translate->getErrors());
                die;
            }
        }

        if (is_array($this->description)) {

            $translate = new Translation();
            $trans = Translation::find()->where(['type_id' => $this->id])->andWhere(['type' => 'categories-description'])->one();
            if ($trans) {
                $translate = $trans;
            }
            $translate->lang = 'en';
            $translate->type = 'categories-description';
            $translate->type_id = $this->id;
            $translate->text_en = $this->description['en'];
            // $translate->text_am = $this->description['am'];
            $translate->text_ru = $this->description['ru'];
            $translate->text_es = $this->description['es'];
            $translate->text_it = $this->description['it'];
            $translate->text_fr = $this->description['fr'];
            $translate->text_de = $this->description['de'];
            if (!$translate->save()) {
                var_dump($translate->errors);
                die;
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function getTranslationTitle()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'categories-title']);
    }

    public function getTranslationDescription()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'categories-description']);
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->title = ArrayHelper::getValue($this, 'translationTitle.text_' . Yii::$app->language);
        $this->description = ArrayHelper::getValue($this, 'translationDescription.text_' . Yii::$app->language);

    }
}
