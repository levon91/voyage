<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $date
 * @property string $title
 * @property string $content
 */
class Events extends \yii\db\ActiveRecord
{
    public $title;
    public $content;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['title', 'required', 'when' => function ($model) {
                return $model->title['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#events-title-en').val() == '';
            }"],
            ['content', 'required', 'when' => function ($model) {
                return $model->content['en'] == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#events-content-en').val() == '';
            }"],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content')
        ];
    }

    public function getTranslationTitle()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'events-title']);
    }

    public function getTranslationContent()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'events-content']);
    }

    public function getTitle()
    {
        return ArrayHelper::getValue($this, 'translationTitle.title_en');
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->title = ArrayHelper::getValue($this, 'translationTitle.text_' . Yii::$app->language);
        $this->content = ArrayHelper::getValue($this, 'translationContent.text_' . Yii::$app->language);

    }

    public function afterSave($insert, $changedAttributes)
    {
        Translation::saveFromParts(['title' => $this->title, 'content' => $this->content],$this,'events');

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public static function getEvents()
    {
        $query = Events::find();
        $order = SORT_DESC;
        // add conditions that should always apply here
        if (Yii::$app->request->get('sort')) {
            $order = Yii::$app->request->get('sort') == 'asc' ? SORT_ASC : SORT_DESC;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date'=>$order]]
        ]);
        return $dataProvider;
    }
}
