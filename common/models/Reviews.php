<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $message
 * @property string $client_name
 * @property string $image
 * @property int $tour_id
 * @property int $approved
 * @property int $email
 * @property string $created_date
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'created_date',
                'value' => date("Y-m-d h:i:sa"),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'client_name', 'email'], 'required'],
            [['tour_id'], 'integer'],
            [['created_date', 'email'], 'safe'],
            ['email', 'email'],
            [['approved'], 'default', 'value' => 0],
            [['message', 'client_name', 'image'], 'string'],
            [['approved'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'message' => Yii::t('app', 'Message'),
            'client_name' => Yii::t('app', 'Client Name'),
            'image' => Yii::t('app', 'Image'),
            'tour_id' => Yii::t('app', 'Tour ID'),
            'approved' => Yii::t('app', 'Approved'),
            'created_date' => Yii::t('app', 'Created Date'),
            'email' => Yii::t('app', 'Email'),
        ];
    }
}
