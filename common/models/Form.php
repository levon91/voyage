<?php

namespace common\models;

use Yii;
use kartik\mpdf\Pdf;

/**
 * This is the model class for table "form".
 *
 * @property int $id
 * @property string $participants
 * @property int $flexible_dates
 * @property string $travel_dates
 * @property string $occasion
 * @property string $adults
 * @property int $teenagers
 * @property int $kids
 * @property int $babies
 * @property int $private_group
 * @property string $date_start
 * @property string $date_length
 * @property string $accommodation
 * @property string $additional
 * @property double $budget
 * @property string $accompaniment
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $birth_date
 * @property string $date_end
 * @property string $residence_country
 * @property int $accept
 */
class Form extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flexible_dates', 'teenagers', 'kids', 'babies', 'private_group', 'accept'], 'integer'],
            [['travel_dates', 'date_start', 'date_length', 'birth_date', 'date_end'], 'safe'],
            [['additional'], 'string'],
            [['budget'], 'number'],
            [['participants', 'adults', 'residence_country'], 'string', 'max' => 100],
            [['occasion', 'accommodation', 'accompaniment', 'first_name', 'last_name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'participants' => Yii::t('app', 'Participants'),
            'flexible_dates' => Yii::t('app', 'Flexible Dates'),
            'travel_dates' => Yii::t('app', 'Travel Dates'),
            'occasion' => Yii::t('app', 'Occasion'),
            'adults' => Yii::t('app', 'Adults'),
            'teenagers' => Yii::t('app', 'Teenagers'),
            'kids' => Yii::t('app', 'Kids'),
            'babies' => Yii::t('app', 'Babies'),
            'private_group' => Yii::t('app', 'Private Group'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_length' => Yii::t('app', 'Date Length'),
            'accommodation' => Yii::t('app', 'Accommodation'),
            'additional' => Yii::t('app', 'Additional'),
            'budget' => Yii::t('app', 'Budget'),
            'accompaniment' => Yii::t('app', 'Accompaniment'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Lest Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'residence_country' => Yii::t('app', 'Residence Country'),
            'accept' => Yii::t('app', 'Accept'),
            'date_end' => Yii::t('app', 'End date'),
        ];
    }

    public static function saveData()
    {
        $formDetails = Yii::$app->session->get('form_details')['TravellersForm'];
        $formSecondDetails = Yii::$app->session->get('form_second_details')['TravellersFormDetails'];
        $post = Yii::$app->request->post('TravellersFormPersonalData');
        $dates = explode(' - ', $formDetails['travel_dates']);

        $form = new Form();
        $form->birth_date = date('Y-m-d', strtotime($post['birth_year'] . '-' . $post['birth_month'] . $post['birth_day']));
        $form->accommodation = implode(',', $formSecondDetails['accommodation']);
        $form->flexible_dates = intval($formDetails['flexible_dates'] == 'yes');
        if (count($dates) > 1) {
            $form->travel_dates = date('Y-m-d', strtotime(trim($dates[0])));
            $form->date_end = date('Y-m-d', strtotime(trim($dates[1])));
            unset($formDetails['travel_dates']);
        }


        unset($post['birth_year']);
        unset($post['birth_month']);
        unset($post['birth_day']);
        unset($formSecondDetails['accommodation']);
        unset($formDetails['flexible_dates']);
        unset(Yii::$app->session->get('form_details')['TravellersForm']['flexible_dates']);


        $formData['Form'] = array_merge($post, $formDetails, $formSecondDetails);
        if ($form->load($formData) && $form->save()) {
            return [$form,$formData];
        } else {
            return false;
        }
    }

    public function sendEmail()
    {


        $mail = Yii::$app
            ->mailer
            ->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setSubject('Voyage Armenia ')
            ->setHtmlBody('New Request From ' . $this->email)
            ->attach(Yii::getAlias('@frontend').'/web/report.pdf',[
                'contentType' => 'application/pdf']);
      if (!$mail->send()) {
           
        }else {
          
        }

    }
}
