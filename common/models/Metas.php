<?php

namespace common\models;

use Codeception\Step\Meta;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "metas".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property boolean $home
 * @property integer $global
 * @property string $type
 * @property integer $type_id
 *
 * @property Pages $page
 */
class Metas extends \yii\db\ActiveRecord
{
    public $value;
    public $value_en;
    public $value_de;
    public $value_es;
    public $value_fr;
    public $value_it;
    public $value_ru;
    const TYPE_POST = 'post';
    const TYPE_PAGE = 'page';
    const TYPE_CATEGORY = 'category';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['type_id', 'home', 'global'], 'integer'],
            [['type'], 'string', 'max' => 100],
            [['value', 'value_de', 'value_en', 'value_ru', 'value_fr', 'value_it', 'value_es'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'type' => Yii::t('app', 'Type'),
            'type_id' => Yii::t('app', 'Article'),
            'home' => Yii::t('app', 'Home Page'),
            'global' => Yii::t('app', 'Global'),
            'value_de' => Yii::t('app', ''),
            'value_ru' => Yii::t('app', ''),
            'value_en' => Yii::t('app', ''),
            'value_fr' => Yii::t('app', ''),
            'value_it' => Yii::t('app', ''),
            'value_es' => Yii::t('app', ''),
        ];
    }

    public function getValue()
    {
        return ArrayHelper::getValue($this, 'translationValue.text_en');
    }

    public function getTranslationValue()
    {
        return $this->hasOne(Translation::className(), ['type_id' => 'id'])->andOnCondition(['type' => 'meta-value']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        Translation::saveFromParts(['value' => $this->value], $this, 'meta');
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->value = ArrayHelper::getValue($this, 'translationValue.text_' . Yii::$app->language);
        $this->value_en = ArrayHelper::getValue($this, 'translationValue.text_en');
        $this->value_de = ArrayHelper::getValue($this, 'translationValue.text_de');
        $this->value_fr = ArrayHelper::getValue($this, 'translationValue.text_fr');
        $this->value_it = ArrayHelper::getValue($this, 'translationValue.text_it');
        $this->value_es = ArrayHelper::getValue($this, 'translationValue.text_es');
        $this->value_ru = ArrayHelper::getValue($this, 'translationValue.text_ru');

    }

    public function getPageTypes()
    {
        return [self::TYPE_POST => self::TYPE_POST, self::TYPE_PAGE => self::TYPE_PAGE, self::TYPE_CATEGORY => self::TYPE_CATEGORY];
    }

    public static function saveMeta($metas, $type, $type_id)
    {
        Metas::deleteAll('type = :type and type_id = :type_id',[':type' => $type, ':type_id' => $type_id]);
        $values = [];
        foreach ($metas as $meta) {
            $newMeta = new Metas();
            $newMeta->key = $meta['key'];
            $newMeta->type = $type;
            $newMeta->type_id = $type_id;
            unset($meta['key']);
            foreach ($meta as $key => $item) {
                $lang = explode('_', $key)[1];
                $values[$lang] = $item;
            }
            $newMeta->value = $values;
            $newMeta->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getTypes()
    {
        if ($this->type == 'page') {
            $name = Pages::className();
        } elseif ($this->type == 'post') {
            $name = Posts::className();
        } else {
            $name = Categories::className();
        }
        return $this->hasOne($name, ['id' => 'type_id']);
    }
}
