<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer_menu".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 */
class FooterMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value_en','value_de','value_fr','value_es','value_it','value_ru'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value_en' => 'Value',
            'value_de' => 'Value',
            'value_fr' => 'Value',
            'value_es' => 'Value',
            'value_it' => 'Value',
            'value_ru' => 'Value',
        ];
    }
}
