<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en_US',
                    'fileMap' => [
                        'yii' => 'yii.php',
                    ]
                ],
            ],
        ],
        'geolocation' => [
            'class' => 'rodzadra\geolocation\Geolocation',
            'config' => [
                'provider' => 'geoplugin',
                'return_formats' =>  'php',
        ],
    ],
    ],
    'params' => [
        'app_url' => 'http://voyagearmenia.com',
    ],
];
