<?php

namespace backend\controllers;

use common\models\PlatformsSearch;
use Yii;
use common\models\Visitors;
use common\models\VisitorssSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VisitorsController implements the CRUD actions for Visitors model.
 */
class VisitorsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Visitors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $filterDate = Yii::$app->request->post('check_issue_date');
        $searchModel = new VisitorssSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$filterDate);
        $visitors = Visitors::find()
        ->asArray()
        ->orderBy('id',SORT_DESC)
        ->all();
        $links = Visitors::find()
        ->asArray()
        ->orderBy('id',SORT_DESC)
        ->groupBy('link')
        ->all();
        $date = new \DateTime();
        $monthdays = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $week = $date->sub(new \DateInterval('P7D'));
        $date = new \DateTime();
        $month = $date->sub(new \DateInterval('P'.$monthdays.'D'));
        $week = date_format($week, 'Y-m-d');
        $month = date_format($month, 'Y-m-d');
        $today = new \DateTime();
        $today = date_format($today, 'Y-m-d 00:00:00');
        $todayCount = Visitors::find()
            ->asArray()
            ->where(['created_at' => $today])
            ->count();

        $weekCount = Visitors::find()
            ->asArray()
            ->where(['>','created_at',$week])
            ->count();
        $monthCount = Visitors::find()
            ->asArray()
            ->where(['>','created_at',$month])
            ->count();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
                'visitors' => $visitors,
                'links' => $links,
                'todayCount' => $todayCount,
                'weekCount' => $weekCount,
                'monthCount' => $monthCount,
                'filterDate' => $filterDate
        ]);
    }

    /**
     * Displays a single Visitors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {


        $model = $this->findModel($id);
        $filterDate = Yii::$app->getRequest()->getQueryParam('check_issue_date');
        if ($filterDate){
            $visitors = Visitors::find()
                ->asArray()
                ->where(['link' => $model->link,'created_at' => $filterDate])
                ->all();
            $countries = Visitors::find()
                ->asArray()
                ->where(['link' => $model->link,'created_at' => $filterDate])
                ->groupBy('country')
                ->orderBy(['id' => SORT_DESC])
                ->all();

        }else {
            $visitors = Visitors::find()->asArray()
                ->where(['link' => $model->link])
                ->all();
            $countries = Visitors::find()
                ->asArray()
                ->where(['link' => $model->link])
                ->groupBy('country')
                ->orderBy(['id' => SORT_DESC])
                ->all();
        }

        $countriesVisitors = [];
        $link_from= [];
        foreach ($countries as $k =>$country){
            if ($filterDate) {
                $filterVisitors = Visitors::find()
                    ->select(['link_from','COUNT(*) AS count'])
                    ->asArray()
                    ->groupBy(['link_from'])
                    ->where(['link' => $model->link, 'created_at' => $filterDate, 'country' => $country['country']])
                    ->all();
                $count = 0;
                foreach ($filterVisitors as $item){
                    $count += $item['count'];
                }
            }else{
                $filterVisitors = Visitors::find()
                    ->select(['link_from','COUNT(*) AS count'])
                    ->asArray()
                    ->groupBy(['link_from'])
                    ->where(['link' => $model->link,'country' => $country['country']])
                    ->all();
                $count = 0;
                foreach ($filterVisitors as $item){
                    $count += $item['count'];
                }
            }

            array_push($countriesVisitors,
                [
                    'country' => $country['country'],
                    'country_code' =>$country['country_code'],
                    'filterVisitors' => $filterVisitors,
                    'count' => $count
                ]);
        }

        usort($countriesVisitors, function($a, $b) {
            return $b['filterVisitors'] <=> $a['filterVisitors'];
        });
        return $this->render('view', [
            'model' => $model,
            'visitors' => $visitors,
            'countriesVisitors' => $countriesVisitors,
            'filterDate' => $filterDate
        ]);
    }


    /**
     * Finds the Visitors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visitors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visitors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
