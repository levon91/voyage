<?php

namespace backend\controllers;

use Yii;
use common\models\Platforms;
use common\models\PlatformsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PlatformsController implements the CRUD actions for Platforms model.
 */
class PlatformsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Platforms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlatformsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Platforms model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Platforms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Platforms();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'logo');
            if($image) {
                $format = $image->getExtension();
                $time = time();
                $imageName = $time . $image->baseName . '.' . $format;
                if (!is_dir(Yii::getAlias('@frontend') . '/web/uploads/platforms')) {
                    mkdir(Yii::getAlias('@frontend') . '/web/uploads/platforms');
                }
                $image->saveAs(Yii::getAlias('@frontend') . '/web/uploads/platforms/' . $imageName);
                $model->logo = $imageName;
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Platforms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $issetImageName = $model->logo;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'logo');
            if($image) {
                if ($issetImageName != null) {
                    $filname = Yii::getAlias('@frontend') . '/web/uploads/platforms/' . $issetImageName;
                    if (file_exists($filname)){
                        unlink($filname);
                    }
                }
                $format = $image->getExtension();
                $time = time();
                $imageName = $time . $image->baseName . '.' . $format;
                $image->saveAs(Yii::getAlias('@frontend') . '/web/uploads/platforms/' . $imageName);
                $model->logo = $imageName;
            }else{
                $model->logo = $issetImageName;
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Platforms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $issetImageName = $model->logo;
        if ($model) {
            if ($issetImageName != null) {
                $filname = Yii::getAlias('@frontend') . '/web/uploads/platforms/' . $issetImageName;
                if (file_exists($filname)){
                    unlink($filname);
                }
            }
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Platforms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Platforms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Platforms::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
