<?php

namespace backend\controllers;

use Yii;
use common\models\Chat;
use common\models\ChatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class ChatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChatSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $news = Chat::find()->select(['id', 'COUNT(*) as cnt', 'is_new', 'session_id'])->where(['is_admin' => 0])->andWhere(['is_new' => 1])->orderBy('is_new DESC')->groupBy('session_id')->all();
        $counts = [];

        foreach ($news as $new) {
            $counts[$new->session_id] = $new->cnt;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'news' => $counts,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chat model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chat();
        if (!Yii::$app->request->get('session')) {
            $this->redirect('/admin_v/chat/index');
        }
        $model->is_new = true;
        $model->is_admin = true;
        $model->created_at = date('Y-m-d H:i:s', time());
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

        }
        Chat::updateAll(['is_new' => false], ['and', ['is_admin' => false], ['session_id' => Yii::$app->request->get('session')]]);
        $chat = Chat::find()->where(['session_id' => Yii::$app->request->get('session')])->orderBy('created_at')->all();

        return $this->render('create', [
            'model' => $model,
            'messages' => $chat,

            'session' => Yii::$app->request->get('session'),
        ]);
    }

    /**
     * Updates an existing Chat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Chat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetMessages()
    {
        $message = Chat::find()->where(['is_new' => 1])->andWhere(['is_admin' => 0])->all();
        $url = parse_url(Yii::$app->request->post('location'));
        if (in_array('/admin_v/chat/create', $url)) {
            $id = explode('=', $url['query']);
            $id = $id[1];
            $chat = Chat::find()->where(['session_id' => $id])->orderBy('created_at')->all();
            return $this->render('_chat', [
                'messages' => $chat,
                'session' => $id,
            ]);
        } else {
            if ($message) {
                return 'success';
            } else {
                return 'field';
            }
        }
    }

    /**
     * Finds the Chat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chat::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
