<?php

namespace backend\controllers;

use common\models\Metas;
use Yii;
use common\models\Posts;
use backend\models\PostSearch;
use yii\base\Security;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Routes;

/**
 * PostsController implements the CRUD actions for Posts model.
 */
class PostsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Posts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Posts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Posts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Posts();

        if ($model->load(Yii::$app->request->post())) {
            $model->cover = UploadedFile::getInstance($model, 'cover');
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if ($model->have_token) {
                $model->token = $this->generateToken();
            }

            if ($model->upload()) {
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $cover = $model->cover;
        $banner = $model->banner;
        $model->meta = $model->getMeta();
        if ($model->load(Yii::$app->request->post())) {
            $model->cover = UploadedFile::getInstance($model, 'cover');
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if ($model->cover == null) {
                $model->cover = $cover;
            }
            if ($model->banner == null) {
                $model->banner = $banner;
            }
            if ($model->have_token) {
                $model->token = $this->generateToken();
            }
            if ($model->upload()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDeleteImage()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get('id') && Yii::$app->request->get('type')) {
            $id = Yii::$app->request->get('id');
            $type = Yii::$app->request->get('type');

            $project = Posts::find()->where(['id' => $id])->one();
            $path = Yii::getAlias('@frontend') . '/web/uploads/posts/' . $project->$type;
            unlink($path);
            $project->$type = '';
            $project->save(false);
            echo 'success';
        } else {
            throwException(404);
        }
    }

    /**
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function deleteMeta($id)
    {
        return Metas::deleteAll(['id' => $id]);
    }


    public function generateToken()
    {
        return Yii::$app->security->generateRandomString();
    }

    /**
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
