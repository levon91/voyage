<?php

namespace backend\controllers;

use Yii;
use common\models\Menus;
use common\models\MenusSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\User;

/**
 * MenusController implements the CRUD actions for Menus model.
 */
class MenusController extends Controller
{
    public $layout = 'admin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new MenusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect('/admin_l/menus/index');
    }

  

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menus();

        $request = Yii::$app->request;
        $data = $request->post();
        $post = $request->post('Menus');
        if (!empty($post)) {

            if (isset($post[$post['location']])) {
                $data['Menus']['parent_id'] = (int)$post[$post['location']];
                $data['Menus']['sort'] = Menus::getChildCount($data['Menus']['parent_id']);
            }
            if (isset($post['types']) && $post['types']) {
                $data['Menus']['type'] = $post['types'];
                $data['Menus']['type_id'] = (int)$post[$post['types']];
            }
        }

        if ($model->load($data) && $model->save()) {
            return $this->redirect('/admin_v/menus/index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;
        $data = $request->post();
        $post = $request->post('Menus');
        if (!empty($post)) {

            if (isset($post[$post['location']])) {
                $data['Menus']['parent_id'] = (int)$post[$post['location']];
//                var_dump($post['old-parent'] != $data['Menus']['parent_id']);die;


                if ($data['old-parent'] != $data['Menus']['parent_id'])
                    $data['Menus']['sort'] = Menus::getChildCount($data['Menus']['parent_id']);
            }
            if (isset($post['types']) && $post['types']) {
                $data['Menus']['type'] = $post['types'];
                $data['Menus']['type_id'] = (int)$post[$post['types']];
            }
        }
        if ($model->load($data) && $model->save()) {
            return $this->redirect('/admin_v/menus/index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Menus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSort()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        if (!empty($post)) {
            $sort = json_decode($post['sort']);

            Menus::updateSort($sort[0], 'top');
            Menus::updateSort($sort[1], 'top_2');
        }
        return $this->render('sort');
    }

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
