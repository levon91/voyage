<?php

namespace backend\controllers;

use common\models\Media;
use Yii;
use common\models\Tours;
use common\models\ToursSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use common\models\User;

/**
 * ToursController implements the CRUD actions for Tours model.
 */
class ToursController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tours models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ToursSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Tours model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tours model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tours();

        if ($model->load(Yii::$app->request->post())) {
            $model->cover = UploadedFile::getInstance($model, 'cover');
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if ($model->upload()) {
                return $this->redirect('index');
            }
        }
        $images = new ActiveDataProvider([
            'query' => Media::find(),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        return $this->render('create', [
            'model' => $model,
            'images' => $images,
        ]);
    }

    /**
     * Updates an existing Tours model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $cover = $model->cover;
        $banner = $model->banner;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->cover = UploadedFile::getInstance($model, 'cover');
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if ($model->cover == null) {
                $model->cover = $cover;
            }if ($model->banner == null) {
                $model->banner = $banner;
            }
            if ($model->upload()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $images = new ActiveDataProvider([
            'query' => Media::find(),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        $ids = explode(',', $model->attachments);
        $files = Media::find()->where(['in', 'id', $ids])->all();
        return $this->render('update', [
            'model' => $model,
            'files' => $files,
            'images' => $images,
        ]);
    }

    /**
     * Deletes an existing Tours model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tours model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tours the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tours::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDeleteImage()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get('id') && Yii::$app->request->get('type')) {
            $id = Yii::$app->request->get('id');
            $type = Yii::$app->request->get('type');
            $tour = Tours::find()->where(['id' => $id])->one();
            $path = Yii::getAlias('@frontend') . '/web/uploads/tours/' . $tour->$type;
            unlink($path);
            $tour->$type = '';
            $tour->save(false);
            echo 'success';
        } else {
            throwException(404);
        }
    }
}
