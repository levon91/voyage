<?php

namespace backend\controllers;

use Yii;
use common\models\Team;
use common\models\TeamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use common\models\User;


/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Team();

        if ($model->load(Yii::$app->request->post())) {
            $model->cover = UploadedFile::getInstance($model, 'cover');
            if ($model->upload()) {
                return $this->redirect('index');
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $issetImageName = $model->cover;

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'cover');
            if ($image != null){
                if ($issetImageName != null) {
                    $filname = Yii::getAlias('@frontend') . '/web/uploads/team/' . $issetImageName;
                    if (file_exists($filname)){
                        unlink($filname);
                    }
                }
                $model->cover = $image;
                $model->upload();
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                $model->cover = $issetImageName;
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $issetImageName = $model->cover;
        if ($model) {
            if ($issetImageName != null) {
                $filname = Yii::getAlias('@frontend') . '/web/uploads/team/' . $issetImageName;
                if (file_exists($filname)){
                    unlink($filname);
                }
            }
            $model->delete();
        }

        return $this->redirect(['index']);
    }
    public function actionDeleteImage()
    {
        if (Yii::$app->request->isAjax && $id = Yii::$app->request->get('id')) {
            $tour = Team::find()->where(['id' => $id])->one();
            $path = Yii::getAlias('@frontend') . '/web/uploads/team/' . $tour->cover;
            unlink($path);
            $tour->cover = '';
            $tour->save(false);
            echo 'success';
        } else {
            throwException(404);
        }
    }


    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
