<?php

namespace backend\controllers;

use Yii;
use common\models\Media;
use common\models\MediaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use common\models\User;

/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::can('Category', $action);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();

        if ($model->load(Yii::$app->request->post())) {
            $model->images = UploadedFile::getInstances($model, 'images');
            if ($model->upload()) {
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $issetFileName = $model->uniq_name;
        $time = time();

        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'uniq_name');

            if($file) {
                if (Yii::$app->request->post('hidden_type')=='video-mobile') {
                    $format = $file->getExtension();
                    if ($format != 'gif') {
                        Yii::$app->session->setFlash('errorFormat', 'upload file format must be in GIF format.');
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                    if ($issetFileName != null) {
                        $filname = Yii::getAlias('@frontend') . '/web/uploads/media/' . $issetFileName;
                        if (file_exists($filname)) {
                            unlink($filname);
                        }
                    }
                    $imageName = $time . $file->baseName . '.' . $format;
                    $file->saveAs(Yii::getAlias('@frontend') . '/web/uploads/media/' . $imageName);
                    $model->uniq_name = $imageName;
                }elseif (Yii::$app->request->post('hidden_type')=='video-desktop'){
                    $format = $file->getExtension();
                    if ($format == 'webm' || $format == 'mp4' || $format == 'ogv') {
                        if ($issetFileName != null) {
                            $filname = Yii::getAlias('@frontend') . '/web/video/' . $issetFileName;
                            if (file_exists($filname)) {
                                unlink($filname);
                            }
                        }
                        $imageName = $time . $file->baseName . '.' . $format;
                        $file->saveAs(Yii::getAlias('@frontend') . '/web/video/' . $imageName);
                        $model->uniq_name = $imageName;
                    }else{
                        Yii::$app->session->setFlash('errorFormat', 'upload file format must be in webg,mp4,ogv format.');
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
            }else{
                $model->uniq_name = $issetFileName;
            }
            if($model->save()){
            return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->type == 'video-mobile' || $model->type == 'video-desktop'){
            throw new NotFoundHttpException(Yii::t('app', 'You cannot delete this item.'));
        }
        else {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
