<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'value_en')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'value_de')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'value_fr')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'value_es')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'value_it')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'value_ru')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
    <?php if ($model->key == 'Video'): ?>
        <div class="">
            <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
                <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="en">
                    <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[en]')->widget(CKEditor::className(), [

                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_en'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="de">
                    <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[de]')->widget(CKEditor::className(), [

                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_de'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="fr">
                    <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[fr]')->widget(CKEditor::className(), [

                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_fr'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="es">
                    <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[es]')->widget(CKEditor::className(), [

                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_es'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="it">
                    <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[it]')->widget(CKEditor::className(), [

                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_it'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="ru">
                    <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'description[ru]')->widget(CKEditor::className(), [
                        'options' => [ 'allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationCaption.text_ru'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>

            </div>
        </div>

        <?php if ($model->banner != null) : ?>
            <div id="update_image">
                <img width="300" src="/uploads/video/<?= $model->banner ?>"/>
                <div class="clearfix"></div>
                <br/>
                <button type="button" data-type="banner" class="btn btn-danger delete"
                ><?= Yii::t('app', 'Delete'); ?></button>
            </div>
        <?php else : ?>
            <?= $form->field($model, 'banner')->fileInput() ?>
        <?php endif; ?>


    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php
$mess = Yii::t('app', 'Do you really want to delete this item');
$js = <<<EOT
$('.delete').on('click', function(){
        var _this = this;
        var deletebutton = confirm('$mess')
        if(deletebutton){
            $.ajax({
            url: '/admin_v/settings/delete-image',
            data: {id: '$model->id'},
            success: function(){
            $(_this).parent().remove();
            }
            })
        }
    })
EOT;


$this->registerJs($js, yii\web\View::POS_END);
?>
