<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VisitorssSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'VISITOR STATISTICS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitors-index">

    <h1><?= Html::encode($this->title) ?></h1>
   <div class="row">
       <div class="col-md-3 market-update-gd">
           <div class="market-update-block clr-block-2" style="padding: 1em 1em;background: #fc3158;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);transition: 0.5s all;-webkit-transition: 0.5s all;">
               <div class="col-md-4 market-update-right">
                   <i class="fa fa-eye" style="font-size: 3em;color: #fff;text-align: left;"> </i>
               </div>
               <div class="col-md-8 market-update-left">
                   <h4 style="font-size: 1.2em;color: #fff;margin: 0.3em 0em;">Today</h4>
                   <h3 style="color: #fff;font-size: 2em;"><?= $todayCount ?></h3>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
       <div class="col-md-3 market-update-gd">
           <div class="market-update-block clr-block-2" style="padding: 1em 1em;background: #53d769;;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);transition: 0.5s all;-webkit-transition: 0.5s all;">
               <div class="col-md-4 market-update-right">
                   <i class="fa fa-eye" style="font-size: 3em;color: #fff;text-align: left;"></i>
               </div>
               <div class="col-md-8 market-update-left">
                   <h4 style="font-size: 1.2em;color: #fff;margin: 0.3em 0em;">Week</h4>
                   <h3 style="color: #fff;font-size: 2em;"><?= $weekCount ?></h3>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
       <div class="col-md-3 market-update-gd">
           <div class="market-update-block clr-block-2" style="padding: 1em 1em;background: #147efb;;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);transition: 0.5s all;-webkit-transition: 0.5s all;">
               <div class="col-md-4 market-update-right">
                   <i class="fa fa-eye" style="font-size: 3em;color: #fff;text-align: left;"></i>
               </div>
               <div class="col-md-8 market-update-left">
                   <h4 style="font-size: 1.2em;color: #fff;margin: 0.3em 0em;">Month</h4>
                   <h3 style="color: #fff;font-size: 2em;"><?= $monthCount ?></h3>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
       <div class="col-md-3 market-update-gd">
           <div class="market-update-block clr-block-2" style="padding: 1em 1em;background: #005d8d;;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);transition: 0.5s all;-webkit-transition: 0.5s all;">
               <div class="col-md-4 market-update-right">
                   <i class="fa fa-eye" style="font-size: 3em;color: #fff;text-align: left;"></i>
               </div>
               <div class="col-md-8 market-update-left">
                   <h4 style="font-size: 1.2em;color: #fff;margin: 0.3em 0em;">All Time</h4>
                   <h3 style="color: #fff;font-size: 2em;"><?= count($visitors) ?></h3>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
   </div>
    <hr>
    <div class="row" style="padding-top: 20px">
        <?php $form = ActiveForm::begin(
                ['method' => 'post']
              ) ?>
        <div class="col-md-9">
            <?php
            echo DatePicker::widget([
                'name' => 'check_issue_date',
                'value' => date('Y-m-d 00:00:00'),
                'options' => [
                        'placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-d 00:00:00',
                    'todayHighlight' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('filter',['class' => 'btn btn-success']) ?>
            <a href="<?= \yii\helpers\Url::to('') ?>" ><?= Html::Button('Clear',['class' => 'btn btn-danger']) ?></a>
        </div>
        <?php $form = ActiveForm::end() ?>
    </div>
    Show Links By Date <?= $filterDate ?>
    <hr>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'link',
            //'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url,$model) use ($filterDate) {
                        $url.= '?check_issue_date='.$filterDate;
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            $url);

                    }
                ],
            ],
        ],
    ]); ?>
    

</div>
