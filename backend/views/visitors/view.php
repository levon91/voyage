<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Visitors */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Visitors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="visitors-view" >
    <h4>Show Statictics By Date <?= $filterDate ?></h4>
    <div style="background: #fff;padding: 10px;margin-bottom: 20px">
        <i class="fa fa-eye" style="font-size: 24px;color: #005d8d;text-align: left;" aria-hidden="true"></i>
        <span style="font-size: 16px"><?= count($visitors) ?> </span>
    </div>
    <div style="background: #fff;padding: 10px;margin-bottom: 20px">
        <i class="fa fa-external-link" style="font-size: 24px;color: #005d8d;text-align: left;" aria-hidden="true"></i>
        <span><?= $model->link ?></span>
    </div>

    <ul class="list-group">
        <?php foreach ($countriesVisitors as $k => $country):?>
        <li class="list-group-item" >
            <img src="/img/countries/<?=strtolower($country['country_code'])?>.png" width="30px" height="15px">
            <span><?= $country['country'] ?> (<?= $country['count'] ?>) </span>
            <?php foreach ($country['filterVisitors'] as $k => $item): ?>
                <br>
                <?= ($k+1).')'?>
                <?php if ($item['link_from'] == null):?>
                    <?= 'Site link'?>(<?= $item['count']?>)
                <?php else:?>
                    <?= $item['link_from']?>(<?= $item['count']?>)
                <?php endif;?>
            <?php endforeach?>
        <?php endforeach;?>
        </li>
    </ul>
    <a href="<?= Yii::$app->request->referrer ?>">
        <?= Html::Button('Back',['class' => 'btn btn-danger']) ?>
    </a>

</div>
