<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model common\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="posts-form">
        <div class="rooms-form tab-content">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="">
                <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
                    <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
                    </li>
                    <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
                    </li>
                    <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
                    </li>
                    <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
                    </li>
                    <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
                    </li>
                    <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                                      data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="en">
                        <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[en]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_en'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[en]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_en'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]); ?>

                    </div>
                    <div class="tab-pane fade" id="de">
                        <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[de]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_de'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[de]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_de'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="fr">
                        <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[fr]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_fr'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[fr]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_fr'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="es">
                        <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[es]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_es'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[es]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_es'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="it">
                        <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[it]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_it'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[it]')->widget(CKEditor::className(), [

                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_it'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="ru">
                        <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'), 'maxlength' => true]) ?>
                        <?= $form->field($model, 'content[ru]')->widget(CKEditor::className(), [
                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationContent.text_ru'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'extraPlugins' => 'accordionList',
                                'filebrowserUploadUrl' => '/admin_v/page/upload'
                            ],
                        ]) ?>
                        <?= $form->field($model, 'caption[ru]')->widget(CKEditor::className(), [
                            'options' => ['allowedContent' => true,
                                'value' => ArrayHelper::getValue($model, 'translationCaption.text_ru'),
                            ],
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                            ],
                        ]) ?>
                    </div>
                </div>
                <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title')) ?>

                <?php // $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

                <?php if ($model->cover != null) : ?>
                    <div id="update_image">
                        <img width="300" src="/uploads/posts/<?= $model->cover ?>"/>
                        <div class="clearfix"></div>
                        <br/>
                        <button type="button" class="btn btn-danger delete" data-type="cover"
                                id="delete"><?= Yii::t('app', 'Delete'); ?></button>
                    </div>
                <?php else : ?>
                    <?= $form->field($model, 'cover')->fileInput() ?>
                <?php endif; ?>

                <?php if ($model->banner != null) : ?>
                    <div id="update_image">
                        <img width="300" src="/uploads/posts/<?= $model->banner ?>"/>
                        <div class="clearfix"></div>
                        <br/>
                        <button type="button" class="btn btn-danger delete" data-type="banner"
                                id="delete"><?= Yii::t('app', 'Delete'); ?></button>
                    </div>
                <?php else : ?>
                    <?= $form->field($model, 'banner')->fileInput(); ?>
                <?php endif; ?>

                <?= $form->field($model, 'have_token')->checkbox(['checked' => 'checked']); ?>
                
                <?= $form->field($model, 'url')->textinput(['value' => $model->isNewRecord ? '' : $model->route->url]); ?>
                

                <?= $form->field($model, 'meta')->widget(MultipleInput::className(), [
                    'max' => 4,
                    'columns' => [
                        [
                            'name' => 'key',
                            'title' => 'Key',
                            'options' => ['class' => 'form-control']
                        ],
                        [
                            'name' => 'value_en',
                            'title' => 'Value En',
                            'options' => ['class' => 'form-control'],
                            'value' => function ($data) {
                                return $data['translationValue']['text_en'];

                            }
                        ],
                        [
                            'name' => 'value_fr',
                            'title' => 'Value FR',
                            'options' => ['class' => 'form-control'],
                            'value' => function ($data) {
                                return $data['translationValue']['text_fr'];
                            }
                        ],
                        [
                            'name' => 'value_es',
                            'title' => 'Value ES',
                            'options' => ['class' => 'form-control'],
                            'value' => function ($data) {
                                return $data['translationValue']['text_es'];
                            }
                        ],
                        [
                            'name' => 'value_it',
                            'title' => 'Value It',
                            'options' => ['class' => 'form-control'],
                            'value' => function ($data) {
                                return $data['translationValue']['text_it'];
                            }
                        ],
                        [
                            'name' => 'value_ru',
                            'title' => 'Value Ru',
                            'options' => ['class' => 'form-control'],
                            'value' => function ($data) {
                                return $data['translationValue']['text_ru'];
                            }
                        ],
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
        <?php $this->registerJsFile("/admin_v/widget/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php $this->registerJsFile("/admin_v/accordionList/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php $this->registerJsFile("/admin_v/collapsibleItem/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php ActiveForm::end(); ?>
    </div>
    </div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php
$mess = Yii::t('app', 'Do you really want to delete this item');
$js = <<<EOT
$('.delete').on('click', function(){
        var _this = this;
        var deletebutton = confirm('$mess')
        if(deletebutton){
            $.ajax({
            url: '/admin_v/posts/delete-image',
           data: {type:$(this).data('type'),id: '$model->id'},
            success: function(){
            $(_this).parent().remove();
            }
            })
        }
    })
EOT;


$this->registerJs($js, yii\web\View::POS_END);

