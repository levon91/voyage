<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="posts-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Create Posts'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                'caption:ntext',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {view} {delete} {copy}',
                    'buttons' => [
                        'copy' => function ($url, $model, $key) {
                            $token = '';
                            if ($model->token != '') {
                                $token = '?token=' . $model->token;
                            }
                            return '<input type="hidden" id="cop'.$model->id.'" " value="http://voyage-armenia.loc/posts/' . $model->id . $token . '"><div id = "'.$model->id.'" class="copy_token">Copy Url</div>';

                        },
                    ]
                ],
            ],
        ]); ?>
    </div>


<?php

$this->registerJs('
$(".copy_token").on("click",function(){
        var id = "#cop"+$(this).attr("id")
         var dummy = document.createElement("input");

  // Add it to the document

var dummy = document.createElement("input");
  document.body.appendChild(dummy);
  // Set its ID
  dummy.setAttribute("id", "dummy_id");

  // Output the array into it
  document.getElementById("dummy_id").value=$(id).val();

  // Select it
  dummy.select();

  // Copy its contents
  document.execCommand("copy");

  // Remove it as its not needed anymore
  document.body.removeChild(dummy);
})

',yii\web\View::POS_READY)
?>