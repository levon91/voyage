<?php
/**
 * Created by PhpStorm.
 * User: Comp
 * Date: 23/01/2018
 * Time: 21:13
 * @param $images object
 */
use yii\widgets\ListView;

// $images gallery images passed by controller
?>
<div class="col-md-12 attachments">
    <?= ListView::widget([
        'dataProvider' => $images,
        'itemView' => 'gallery'
    ]) ?>
</div>