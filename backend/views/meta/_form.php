<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Pages;
use common\models\Posts;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\Metas */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="metas-form">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="active"><a href="#en" data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class=""><a href="#de" data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class=""><a href="#fr" data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class=""><a href="#es" data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class=""><a href="#it" data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class=""><a href="#ru" data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <?php $form = ActiveForm::begin(); ?>
        <div class="tab-content">
            <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'value[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_en'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'value[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_de'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'value[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_fr'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'value[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_es'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'value[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_it'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'value[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationValue.text_ru'), 'maxlength' => true]) ?>
            </div>
            <div class="type">
                <?= $form->field($model, 'type')->dropDownList($model->pageTypes, ['prompt' => Yii::t('app', 'Choose Type')]); ?>
            </div>
            <?php
            $data = [];
            if (!$model->isNewRecord) : ?>
                <?php
                if ($model->type == 'post') {
                    $data = ArrayHelper::map(Posts::find()->all(), 'id', 'title');
                } elseif ($model->type == 'page') {
                    $data = ArrayHelper::map(Pages::find()->all(), 'id', 'title');
                } else {
                    $data = ArrayHelper::map(Categories::find()->all(), 'id', 'title');
                }
                ?>
            <?php endif; ?>
            <?= $form->field($model, 'type_id')->dropDownList($data); ?>

            <?php //$form->field($model, 'is_home')->checkbox() ?>

            <?= $form->field($model, 'global')->checkbox() ?>

            <?= $form->field($model, 'home')->checkbox() ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$this->registerJs("
    $('#metas-type').on('change', function (e) {
        $.ajax({
            url: '/admin_v/meta/types',
            type: \"POST\",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=\"csrf-token\"]').attr('content'))
            },
            data: {'type': $(this).val()},
            success: function (data) {
                $('#metas-type_id').html(data);
            }
        })
    })", yii\web\View::POS_END);
?>

<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
