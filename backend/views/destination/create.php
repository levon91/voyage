<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Destinations */

$this->title = Yii::t('app', 'Create Destinations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
