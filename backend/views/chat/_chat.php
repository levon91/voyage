<?php foreach ($messages as $message): ?>
    <div class="col-md-12 no-padd"><?= $message->is_admin ? 'Admin' : 'User' ?></div>
    <div class="col-md-12 alert alert-success no-margin"><?= $message->message ?><p><?= $message->created_at; ?></p>
    </div>
<?php endforeach; ?>
