<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Chat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-form">
    <div class="chata">
        <?php $this->render('_chat', ['messages' => $messages]) ?>
    </div>
    <div class="clearfix"></div>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'session_id')->hiddenInput(['value' => $session])->label(false); ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]); ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
