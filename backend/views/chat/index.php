<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'message:ntext',
            [
                'attribute' => 'cnt',
                'value' => function($model) use ($news){
                    return isset($news[$model->session_id]) ? $news[$model->session_id] :0;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
               'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/admin_v/chat/create?session=' . $model->session_id, [
                            'title' => Yii::t('yii', 'Update'),
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
