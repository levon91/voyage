<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="services-form">
    <div class="tour-form tab-content">
        <?php $form = ActiveForm::begin(); ?>
        <div class="">
            <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
                <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="en">
                    <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[en]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_en'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="de">
                    <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[de]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_de'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="fr">
                    <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[fr]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_fr'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="es">
                    <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[es]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_es'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="it">
                    <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[it]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_it'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="ru">
                    <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'), 'maxlength' => true]) ?>
                    <?= $form->field($model, 'content[ru]')->widget(CKEditor::className(), [

                        'options' => ['allowedContent' => true,
                            'value' => ArrayHelper::getValue($model, 'translationContent.text_ru'),
                        ],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>