<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="team-form">
    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <?php $form = ActiveForm::begin(); ?>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'name[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_en'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_en'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_en'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[en]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_en'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'name[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_de'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_de'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_de'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[de]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_de'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'name[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_fr'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_fr'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_fr'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[fr]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_fr'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'name[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_es'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_es'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_es'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[es]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_es'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'name[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_it'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_it'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_it'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[it]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_it'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'name[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationName.text_ru'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'surname[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationSurname.text_ru'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'position[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationPosition.text_ru'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'info[ru]')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'translationInfo.text_ru'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <?php if ($model->cover != null) : ?>
                <div id="update_image">
                    <img width="300" src="/uploads/team/<?= $model->cover ?>"/>
                    <div class="clearfix"></div>
                    <br/>
                    <?= $form->field($model, 'cover')->fileInput() ?>
                </div>
            <?php else : ?>
                <?= $form->field($model, 'cover')->fileInput() ?>
            <?php endif; ?>
            <?= $form->field($model, 'order')->textInput(['type' => 'number','value' => ArrayHelper::getValue($model, 'order'), 'maxlength' => true]) ?>

        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php
$mess = Yii::t('app', 'Do you really want to delete this item');
$js = <<<EOT
$('.delete').on('click', function(){
        var _this = this;
        var deletebutton = confirm('$mess')
        if(deletebutton){
            $.ajax({
            url: '/admin_v/team/delete-image',
           data: {id: '$model->id'},
            success: function(){
            $(_this).parent().remove();
            }
            })
        }
    })
EOT;


$this->registerJs($js, yii\web\View::POS_END);
