<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Media'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
            if($model->type == 'video-mobile'){
                return ['class' => 'video-type'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'uniq_name',
                'format' => 'html',
                'label' => Yii::t('app', 'Image'),
                'value' => function ($model) {
                    if ($model->uniq_name != null && $model->type != 'video-desktop' ) {
                        return '<img width="200" src="/uploads/media/' . $model->uniq_name . '">';
                    }
                    else if ($model->type == 'video-desktop') {
                        return 'Video';
                    }
                    else{
                        return 'no set';
                    }
                }

            ],
            'name',
            'type',
            'extension',
            //'type_id',
            //'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',

                'buttons' => [
                'update' => function ($url,$model) {
                    return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    $url);
                },'view' => function ($url,$model) {
                    return Html::a(
                    '<span class="glyphicon glyphicon-eye-open"></span>',
                    $url);
                },'delete' => function ($url,$model) {
                    if ($model->type == 'video-mobile' || $model->type == 'video-desktop'){
                        return '';
                    }
                    return Html::a(
                    '<span class="glyphicon glyphicon-trash"></span>',
                    $url,[
                    'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                    ]);
                },

            ],
            ],
        ],
    ]); ?>
</div>

