<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Media */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="media-form">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="tab-content">
            <?php if ($model->uniq_name): ?>
            <?php if($model->type != 'video-desktop'):?>
                <img width="300px" src="<?= '/uploads/media/' . $model->uniq_name ?>"/>
                <p>
                    <code><?= $model->uniq_name ?></code>
                </p>
            <?php else:?>
                    <video width="320" height="240" controls>
                        <source src="/video/<?=$model->uniq_name?>" type="video/mp4">
                    </video>
            <?php endif;?>
            <?php elseif ($model->type != 'video-mobile' && $model->type != 'video-desktop'): ?>
                <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
            <?php endif; ?>
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'), 'maxlength' => true]) ?>
            </div>
            <?php if($model['type'] != 'video-mobile' && $model['type'] != 'video-desktop'): ?>
            <?= $form->field($model, 'type')->dropDownList($model->typeList, ['id' => 'type']) ?>
            <?php else:?>
            <?= $form->field($model, 'active')->checkbox([ 'checked' => ($model['active'] == true)? true : false]) ?>
            <?= $form->field($model, 'uniq_name')->fileInput([ 'accept' => 'image/*']) ?>
            <input type="hidden" name="hidden_type" value="<?= $model->type ?>">

            <?php endif;?>

            <?= $form->field($model, 'gallery_type')->dropDownList($model->galleryTypes, ['prompt' => 'Choose gallery image size', 'id' => 'gallery_type', 'class' => 'hidden form-control'])->label(false) ?>
            <?= $form->field($model, 'gallery_category')->dropDownList(ArrayHelper::map($model->categories, 'id', 'title'), ['prompt' => 'Choose Category', 'id' => 'gallery_cat', 'class' => 'hidden form-control'])->label(false)?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

<?php
$this->registerJs("
if($('select[name=\"Media[type]\"]').val() == 'home_gallery'){
    $('#gallery_type').removeClass('hidden')
}
    $('#type').on('change',function(){
        if($(this).val() == 'home_gallery' || $(this).val() == 'gallery'){
             $('#gallery_type').removeClass('hidden')
             $('#gallery_cat').removeClass('hidden')
        }else{
         $('#gallery_type').addClass('hidden')
         $('#gallery_cat').addClass('hidden')
        }
     
        
    })

   
", yii\web\View::POS_END);


?>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]);