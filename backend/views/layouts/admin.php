<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
$this_user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js sidebar-large">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="font-size: 13px;">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php if (!Yii::$app->user->isGuest): ?>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="z-index: 222;">
            <div class="container-fluid">
                <div class="navbar-header">

                    <a id="menu-medium" class="sidebar-toggle tooltips">

                    </a>
                    <a class="domain" href="<?php \yii\helpers\Url::base(); ?>">VOYAGE ARMENIA</a>
                </div>
                <div class="navbar-center">Dashboard
                    <a href="/admin_v/site/change-language?lang=am">AM</a>
                    <a href="/admin_v/site/change-language?lang=en">EN</a>
                </div>
                <div class="navbar-center">
                </div>
                <div class="navbar-collapse collapse">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <ul class="nav navbar-nav pull-right header-menu">


                        <!-- BEGIN USER DROPDOWN -->
                        <li class="dropdown" id="user-header">
                            <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                <img src="/img/avatar2.jpg" alt="user avatar"
                                     width="30" class="p-r-5">
                                <span class="username"><?= Yii::$app->user->identity['username'] ?></span>
                                <i class="fa fa-angle-down p-r-10"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a>
                                        <?php
                                        echo Html::beginForm(['/site/logout'], 'post')
                                            . Html::submitButton(
                                                '<i class="fa fa-power-off"></i>' . Yii::t('app', 'logout'),
                                                ['class' => 'btn btn-link']
                                            )
                                            . Html::endForm();
                                        ?></a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER DROPDOWN -->
                    </ul>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
        </nav>
        <!-- BEGIN MAIN SIDEBAR -->
        <div id="wrapper">
            <nav id="sidebar">
                <div id="main-menu">
                    <ul class="sidebar-nav">

                        <?php if (Yii::$app->user->identity->can('Category', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "categories" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['categories/index']) ?>"><i class="fa fa-list-ul"></i><span
                                        class="sidebar-text"><?= Yii::t('app', 'Categories') ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Page', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "page" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['page/index']) ?>"><i class="fa fa-pagelines"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Page') ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Post', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "posts" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['posts/index']) ?>"><i class="fa fa-book "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Posts'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Events', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "events" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['events/index']) ?>"><i class="fa fa-book"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Events'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Project', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "project" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['project/index']) ?>"><i class="fa fa-book "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Projects'); ?></span></a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->identity->can('Menus', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "menus" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['menus/index']) ?>"><i class="fa fa-bars "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Menus'); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('FooterMenu', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "footer-menu" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['footer-menu/index']) ?>"><i class="fa fa-bars "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'FooterMenu'); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Services', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "services" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['services/index']) ?>"><i class="fa fa-list "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Services'); ?></span>

                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Reviews', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "reviews" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['reviews/index']) ?>"><i class="fa fa-list"></i><span
                                        class="sidebar-text"><?= Yii::t('app', 'Reviews') ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('chat', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "chat" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['chat/index']) ?>"><i class="fa fa-bars "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Chat'); ?></span>
                                    <span class="glyphicon glyphicon-envelope chat_icon"></span>
                                </a>

                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Media', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "media" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['media/index']) ?>"><i class="fa fa-image"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Media'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('FunnyBanner', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "FunnyBanner" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['funny-banner/index']) ?>"><i class="fa fa-heart"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Funny Banner'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Tours', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "tours" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['tours/index']) ?>"><i class="fa fa-bars "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Tours'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Schedule', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "schedule" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['schedule/index']) ?>"><i class="fa fa-bars "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Schedule'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Settings', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "settings" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['settings/index']) ?>"><i class="fa fa-cog"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Settings'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Team', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "team" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['team/index']) ?>"><i class="fa fa-cog"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Team'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Destination', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "destination" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['destination/index']) ?>"><i class="fa fa-map-marker "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Destinations'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Meta', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "meta" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['meta/index']) ?>"><i class="fa fa-book"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Meta'); ?></span></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->can('Platforms', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "platforms" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['platforms/index']) ?>"><i class="fa fa-book "></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Platforms'); ?></span></a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->identity->can('Visitors', 'read')): ?>
                            <li class="<?php echo Yii::$app->controller->id == "Visitors" ? "current" : ""; ?>">
                                <a href="<?= Url::to(['visitors/index']) ?>"><i class="fa fa-book"></i>
                                    <span class="sidebar-text"><?= Yii::t('app', 'Visitors'); ?></span></a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
            </nav>
        </div>
        <!-- END MAIN SIDEBAR -->
    <?php endif; ?>
    <div class="<?= !Yii::$app->user->isGuest ? "col-md-offset-3 container-admin" : 'container' ?>">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<audio id="audio" src="/uploads/definite.ogg"></audio>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
