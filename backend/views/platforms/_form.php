<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Platforms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="platforms-form">
    <div class="rooms-form tab-content">
    <?php $form = ActiveForm::begin(); ?>
        <div class="">
            <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
                <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
                </li>
                <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                                  data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">

                <?= $form->field($model, 'description_en')->textarea(['rows' => 10]) ?>

            </div>
            <div class="tab-pane fade" id="de">

                <?= $form->field($model, 'description_de')->textarea(['rows' => 10]) ?>

            </div>
            <div class="tab-pane fade" id="fr">

                <?= $form->field($model, 'description_fr')->textarea(['rows' => 10]) ?>

            </div>
            <div class="tab-pane fade" id="es">

                <?= $form->field($model, 'description_es')->textarea(['rows' => 10]) ?>

            </div>
            <div class="tab-pane fade" id="it">

                <?= $form->field($model, 'description_it')->textarea(['rows' => 10]) ?>

            </div>
            <div class="tab-pane fade" id="ru">

                <?= $form->field($model, 'description_ru')->textarea(['rows' => 10]) ?>

            </div>
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'order')->textInput() ?>
        </div>

    <?php if ($model->logo): ?>
        <img width="300px" src="<?= '/uploads/platforms/' . $model->logo ?>"/>
    <?php endif; ?>
    <?= $form->field($model, 'logo')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

        <?php $this->registerJsFile("/admin_v/widget/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php $this->registerJsFile("/admin_v/accordionList/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php $this->registerJsFile("/admin_v/collapsibleItem/plugin.js", ['depends' => [\yii\web\JqueryAsset::className()]])?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php
$mess = Yii::t('app', 'Do you really want to delete this item');
$js = <<<EOT
$('.delete').on('click', function(){
        var _this = this;
        var deletebutton = confirm('$mess')
        if(deletebutton){
            $.ajax({
            url: '/admin_v/posts/delete-image',
           data: {type:$(this).data('type'),id: '$model->id'},
            success: function(){
            $(_this).parent().remove();
            }
            })
        }
    })
EOT;


$this->registerJs($js, yii\web\View::POS_END);
