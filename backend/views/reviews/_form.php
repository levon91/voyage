<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?php //= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_id')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'approved')->checkbox(); ?>

    <?= $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
