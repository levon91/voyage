<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FunnyBanner */

$this->title = Yii::t('app', 'Create Funny Banner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Funny Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funny-banner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
