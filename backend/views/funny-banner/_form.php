<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\FunnyBanner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="funny-banner-form">
    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'number')->textInput() ?>

        <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'description[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_en'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'description[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_de'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'description[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_fr'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'description[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_es'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'description[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_it'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'description[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_ru'), 'maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
