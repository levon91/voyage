<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FunnyBannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Funny Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funny-banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($dataProvider->totalCount < 4): ?>

        <p>
            <?= Html::a(Yii::t('app', 'Create Funny Banner'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'number',
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
