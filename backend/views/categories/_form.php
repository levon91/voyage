<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="categories-form">

    <div class="rooms-form tab-content">

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_en'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_de'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_fr'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_es'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_it'), 'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'), 'maxlength' => true]) ?>
                <?= $form->field($model, 'description[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationDescription.text_ru'), 'maxlength' => true]) ?>
            </div>
            <?php if ($model->cover != null) : ?>
                <div id="update_image">
                    <img width="300" src="/uploads/categories/<?= $model->cover ?>"/>
                    <div class="clearfix"></div>
                    <br/>
                    <button type="button" class="btn btn-danger delete"
                            id="delete"><?= Yii::t('app', 'Delete'); ?></button>
                </div>
            <?php else : ?>
                <?= $form->field($model, 'cover')->fileInput() ?>
            <?php endif; ?>
        </div>
        <?= $form->field($model, 'for_tour')->checkbox(); ?>
        <?= $form->field($model, 'for_gallery')->checkbox(); ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>

<?php
$mess = Yii::t('app', 'Do you really want to delete this item');
$js = <<<EOT
$('.delete').on('click', function(){
        var _this = this;
        var deletebutton = confirm('$mess')
        if(deletebutton){
            $.ajax({
            url: '/admin_v/categories/delete-image',
           data: {id: '$model->id'},
            success: function(){
            $(_this).parent().remove();
            }
            })
        }
    })
EOT;


$this->registerJs($js, yii\web\View::POS_END);
