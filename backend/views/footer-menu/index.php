<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FooterMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Footer Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'template' => '{view}{update}',
            ],
            ]
    ]); ?>


</div>
