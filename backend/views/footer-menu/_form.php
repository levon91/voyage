<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FooterMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-menu-form">
    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>
        <?php $form = ActiveForm::begin(); ?>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="en">

                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_en')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_en'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_de')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_de'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>

            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_fr')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_fr'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_es')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_es'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_it')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_it'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'key')->textInput(['maxlength' => true,'disabled' => true]) ?>

                <?= $form->field($model, 'value_ru')->widget(CKEditor::className(), [

                    'options' => ['allowedContent' => true,
                        'value' => ArrayHelper::getValue($model, 'value_ru'),
                    ],
                    'editorOptions' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]); ?>
<?php $this->registerJs('CKEDITOR.editorConfig = function (config)
{
    config.enterMode = CKEDITOR.ENTER_BR;

};', yii\web\View::POS_END); ?>