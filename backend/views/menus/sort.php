<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\Menus;

use backend\components\BuildMenus;


/* @var $this yii\web\View */
/* @var $model common\models\Menus */
$this->title = Yii::t('app', 'Sort Menus');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this_user = Yii::$app->user->identity;
$title = 'title';

?>



<div class="menus-view">

    <h1><?= Html::encode($this->title) ?></h1>


</div>


<?php $form = ActiveForm::begin(); ?>
<input type="hidden" id="sort" name="sort">
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

<?= BuildMenus::widget() ?>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
