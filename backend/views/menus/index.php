<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
$this_user = Yii::$app->user->identity;

?>
<div class="menus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if($this_user->can('menus','create')) echo Html::a(Yii::t('app', 'Create Menus'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php if($this_user->can('menus','update')) echo Html::a(Yii::t('app', 'Sort Menus'), ['sort'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
       //    'parent_id',
            [
                'attribute'=> 'title',
                'label' => Yii::t('app','Title')
            ],
             'type',
            /*[
                'attribute' => 'typeName.title_'.Yii::$app->language,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'sorting'],
                'label' => Yii::t('app', 'Type name')
            ],
            [
                'attribute' => 'parent.title_'.Yii::$app->language,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'sorting'],
                'label' => Yii::t('app', 'Parent')
            ],*/
            // 'type_id',
             'location',
             'created_at',
             'updated_at',


            [
                'class' => 'yii\grid\ActionColumn' ,
                'template' => '{update} {delete}',
                'visibleButtons' =>[
                    'update' => function ($model, $key, $index) {
                        $user =  Yii::$app->user->identity;
                        return $user->can('menus','update');
                    },
                    'delete' => function ($model, $key, $index) {
                        $user =  Yii::$app->user->identity;
                        return $user->can('menus','delete') ;
                    }
                ],
                'buttons'=>[
                    'view' => function ($url) {
                        return Html::a(
                            '<span class="label label-success w-300">'.Yii::t("app","View").'</span><br />',
                            $url,
                            [
                                'title' => 'Download',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="label label-info w-300">'.Yii::t("app","Update").'</span>',
                            $url,
                            [
                                'title' => 'Update',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'delete' => function ($url) {
                        return Html::a(
                            '<span class="label label-danger w-300">'.Yii::t("app","Delete").'</span><br />',
                            $url,
                            [
                                'title' => 'Delete',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
            ]

        ],
    ]); ?>
</div>
