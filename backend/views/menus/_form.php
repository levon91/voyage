<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

//use kartik\file\FileInput;
//USE yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Menus */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="">
        <ul id="myTab" class="nav nav-tabs" style="margin-left: -1px;">
            <li class="<?= Yii::$app->language == 'en' ? 'active' : '' ?>"><a href="#en"
                                                                              data-toggle="tab"><?= Yii::t('app', 'English'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'de' ? 'active' : '' ?>"><a href="#de"
                                                                              data-toggle="tab"><?= Yii::t('app', 'German'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'fr' ? 'active' : '' ?>"><a href="#fr"
                                                                              data-toggle="tab"><?= Yii::t('app', 'French'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'es' ? 'active' : '' ?>"><a href="#es"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Spain'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'it' ? 'active' : '' ?>"><a href="#it"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Italian'); ?></a>
            </li>
            <li class="<?= Yii::$app->language == 'ru' ? 'active' : '' ?>"><a href="#ru"
                                                                              data-toggle="tab"><?= Yii::t('app', 'Russian'); ?></a>
            </li>
        </ul>

        <?php $form = ActiveForm::begin(['id' => 'createMenu']); ?>
        <div class="tab-content">
            <?= $form->field($model, 'url')->textInput() ?>
            <div class="tab-pane fade active in" id="en">
                <?= $form->field($model, 'title[en]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_en'),'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="de">
                <?= $form->field($model, 'title[de]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_de'),'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="fr">
                <?= $form->field($model, 'title[fr]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_fr'),'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="es">
                <?= $form->field($model, 'title[es]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_es'),'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="it">
                <?= $form->field($model, 'title[it]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_it'),'maxlength' => true]) ?>
            </div>
            <div class="tab-pane fade" id="ru">
                <?= $form->field($model, 'title[ru]')->textInput(['value' => ArrayHelper::getValue($model, 'translationTitle.text_ru'),'maxlength' => true]) ?>
            </div>

            <?= $form->field($model, 'location')->dropDownList($model->locations, ['options' => [$model->location => ['Selected' => 'selected']], 'prompt' => Yii::t('app', 'Choose location')]); ?>
            <?= $form->field($model, 'top')->dropDownList($model->top, ['options' => [$model->parent_id => ['Selected' => 'selected']], 'class' => 'form-control location top', 'prompt' => Yii::t('app', 'Choose Parent Menu')]); ?>
            <?= $form->field($model, 'top_2')->dropDownList($model->top_2, ['options' => [$model->parent_id => ['Selected' => 'selected']], 'class' => 'form-control location top_2', 'prompt' => Yii::t('app', 'Choose Parent Menu')]); ?>
            <?= $form->field($model, 'footer1')->dropDownList($model->footer1, ['options' => [$model->parent_id => ['Selected' => 'selected']], 'class' => 'form-control location footer_1', 'prompt' => Yii::t('app', 'Choose Parent Menu')]); ?>
            <?= $form->field($model, 'footer2')->dropDownList($model->footer2, ['options' => [$model->parent_id => ['Selected' => 'selected']], 'class' => 'form-control location footer_2', 'prompt' => Yii::t('app', 'Choose Parent Menu')]); ?>
            <?= $form->field($model, 'footer3')->dropDownList($model->footer3, ['options' => [$model->parent_id => ['Selected' => 'selected']], 'class' => 'form-control location footer_3', 'prompt' => Yii::t('app', 'Choose Parent Menu')]); ?>
            <?= $form->field($model, 'types')->dropDownList($model->types, ['options' => [$model->type => ['Selected' => 'selected']], 'prompt' => Yii::t('app', 'Choose type')]); ?>
            <?= $form->field($model, 'post')->dropDownList($model->post, ['options' => [$model->type_id => ['Selected' => 'selected']], 'class' => 'form-control block post', 'prompt' => Yii::t('app', 'Choose post')]); ?>
            <?= $form->field($model, 'categories')->dropDownList($model->categories, ['options' => [$model->type_id => ['Selected' => 'selected']], 'class' => 'form-control block categories', 'prompt' => Yii::t('app', 'Choose category')]); ?>
            <?= $form->field($model, 'pages')->dropDownList($model->pages, ['options' => [$model->type_id => ['Selected' => 'selected']], 'class' => 'form-control block pages', 'prompt' => Yii::t('app', 'Choose Page')]); ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary check']) ?>
            </div>
        </div>
        <?php if ($model->id): ?>
            <input type="hidden" name="old-parent" value="<?= $model->parent_id ?>">
        <?php endif; ?>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$script = <<<EOD
    $(document).ready(function(){
        $('.block').parent('.form-group').addClass('hidden');
        $('.location').parent('.form-group').addClass('hidden');

        $('#menus-types').on('change' ,function(){
            $('.block').parent('.form-group').addClass('hidden');
            $('.'+$(this).val()).parent('.form-group').removeClass('hidden');
        });

        $('#menus-location').on('change' ,function(){
            $('.location').parent('.form-group').addClass('hidden');
                $('.'+$(this).val()).parent('.form-group').removeClass('hidden');
        });
         });
EOD;
$this->registerJs($script, yii\web\View::POS_END);

/*
. $model->id .  "
            $('." . $model->type . "').parent('.form-group').removeClass('hidden');
            $('." . $model->location . "').parent('.form-group').removeClass('hidden');
    });" : " });*/

?>
<?php $this->registerJsFile('/admin_v/plugins/bootstrap/bootstrap.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
