<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tours */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tours'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tours-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php /*= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_am',
            'title_ru',
            'title_en',
            'content_am:ntext',
            'content_en',
            'content_ru',
            'caption_am:ntext',
            'caption_en',
            'caption_ru',
            'category_id',
            'price',
            'days',
            'age_min',
            'type',
            'cover',
            'banner',
            'attachments:ntext',
            'destination_id',
            'coordinates',
            'created_at',
            'updated_at',
        ],
    ]) */?>

</div>
