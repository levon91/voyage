<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'time_from')->input('date') ?>

    <?= $form->field($model, 'time_to')->input('date') ?>

    <?php //   $form->field($model, 'week_day')->dropDownList($model->daysList); ?>


    <?= $form->field($model, 'tour_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Tours::find()->all(),'id','title')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
