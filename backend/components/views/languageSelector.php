<div id="language-select">
    <?php
    if(sizeof($languages) < 4) {
        // Render options as links
        $lastElement = end($languages);
        foreach($languages as $key=>$lang) {
            $controller = $this->context->callingcontroller;
            if($key != $currentLang) {
                $url = $controller->createMultilanguageReturnUrl($key, $controller->actionParams);
                echo yii\helpers\Html::a($lang, $url);
//                echo yii\helpers\Url::to(['users/index']);
//                echo $url;
            }
            else{
                echo '<b style="color: #159077;">' . $lang . '</b>';
            }
            if($lang != $lastElement){
                echo ' | ';
            }
        }
    }
    else {
        // Render options as dropDownList
        echo yii\helpers\Html::form();
        foreach($languages as $key=>$lang) {
            echo yii\helpers\Html::hiddenInput(
                $key,
                $this->getOwner()->createMultilanguageReturnUrl($key));
        }
        echo yii\helpers\Html::dropDownList('language', $currentLang, $languages,
            array(
                'submit'=>'',
            )
        );
        echo yii\helpers\Html::endForm();
    }
    ?>
</div>