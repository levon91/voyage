<?php

use backend\components\BuildMenus;

?>


<link href="<?= Yii::$app->request->baseUrl; ?>/css/menu/vendor.css" rel="stylesheet">

<link href="<?= Yii::$app->request->baseUrl; ?>/css/menu/application.css" rel='stylesheet'>
<div class="col-md-6">
    <ol data-name="top" class="serialization1 vertical">
        <?= Yii::t('app', 'Top') ?>
        <?php echo BuildMenus::buildSort(BuildMenus::topParent(), 'top') ?>
    </ol>
</div>
<!--<div class="col-md-6">-->
<!--    <ol data-name="top_2" class="serialization1 vertical">-->
<!--        --><? //= Yii::t('app','Footer')?>
<!--        --><?php // echo BuildMenus::buildSort(BuildMenus::leftParent(), 'footer')?>
<!--    </ol>-->
<!--</div>-->
<!--        <pre id="serialize_output21"></pre>-->

<?php
Yii::$app->view->registerJsFile(Yii::$app->request->baseUrl . '/js/menu/application.js', ['depends' => \yii\web\JqueryAsset::className()]);

Yii::$app->view->registerJs('
    var group = $("ol.serialization1").sortable({
        group: \'serialization\',
        delay: 500,
        onDrop: function ($item, container, _super) {
            var data = group.sortable("serialize").get();
            var jsonString = JSON.stringify(data, null, \' \');
            $(\'#sort\').val(jsonString);
            _super($item, container);
        }
    });', yii\web\View::POS_READY);
?>

