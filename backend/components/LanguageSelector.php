<?php

namespace backend\components;

use yii\base\BootstrapInterface;
use yii\base\BaseObject;

class LanguageSelector extends BaseObject implements BootstrapInterface
{
    public static $supportedLanguages = ['en', 'ru','am'];

    public function bootstrap($app)
    {
        $preferredLanguage = isset($app->request->cookies['language']) ? (string)$app->request->cookies['language'] : null;

        if (empty($preferredLanguage)) {
            $preferredLanguage = $app->request->getPreferredLanguage(self::$supportedLanguages);
        }

        $app->language = $preferredLanguage;
    }
}

?>