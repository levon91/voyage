<?php
namespace backend\components;

use Yii;
use yii\base\Widget;
use common\models\Menus;
use yii\helpers\Html;


class BuildMenus extends Widget{

    public function run(){

        return $this->render('buildMenus');
    }

    public static function topParent()
    {

        return Menus::find()
            ->with('translationTitle')
            ->where(['location' => 'top', 'parent_id'=>0])
            ->orderBy('sort')
            ->all();
    }

//    public static function leftParent()
//    {
//        return Menus::find()
//            ->select("id, title_". Yii::$app->language ." as title,  parent_id")
//            ->where(['location' => 'footer', 'parent_id'=>0])
//            ->orderBy('sort')
//            ->all();
//    }
    public static function buildSort($menus,$location)
    {
        $sort = '';
        foreach ($menus as $menu) {
            $sort .= "<li data-id=\"{$menu->id}\" data-name=\"{$menu->title}\">{$menu->title}";
                $sort .=    '<ol>';
                $sort .=        static::buildSort($menu->children, $location);
                $sort .=    '</ol>';
            $sort .= '</li>';
        }
        return $sort;
    }
}
?>