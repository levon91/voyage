<?php
return [
    'adminEmail' => 'admin@example.com',
    'status-10' => ['Category'],
    'default_language' => 'am',
    'languages' => ['am','en-US','en']
];
